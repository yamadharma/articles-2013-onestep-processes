(TeX-add-style-hook "text"
 (lambda ()
    (LaTeX-add-bibitems
     "vol"
     "kol"
     "bas"
     "gr"
     "gar"
     "ga")
    (LaTeX-add-labels
     "fig:1.2")))

