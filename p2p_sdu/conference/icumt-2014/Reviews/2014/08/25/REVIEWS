Reviews
3 Reviews

Review 1 (Reviewer A)
Your Overall Recommendation
Likely Accept (between top 10%-30% of the papers) (4)

Detailed Comments (Please justify your overall recommendation and
suggest improvements in technical content or presentation to the
author(s). What are the key strengths of the paper to warrant an
'accept'? What are the key weaknesses of the paper or main reasons to
reject it?)

    The article “The method of constructing models of peer to peer
    protocols” proposed by a team of authors deals with FastTrack and
    BitTorrent protocols models construction. The one of the ‘pluses’
    of the article is that the model description is based on ordinary
    differentiation equation and partial differential equation
    technique. This approach is useful on both initial and boundary
    value problems. Another ‘plus’ of the article is a detailed
    description of one-step stochastic process construction as for
    FastTrack protocol as well as for BitTorrent protocol. For
    FastTrack protocol a deterministic approach for population
    dynamics of new clients and seeds description is thoroughly
    presented. The main ‘minus’ of the article is that the authors did
    not compare the investigated one-step stochastic processes method
    with other methods used for p2p protocols models construction and
    analysis. Also a misprint in Markov process description (the first
    paragraph of part III) should be noted. Proofread of the
    manuscript is recommended. First sentence in Section III contains
    unexpected sign ?. It is better to change word “dispersion” to
    more common “variance”. In section IV in the sentence “To upload a
    file, a node sends a request to the supernode…” probably authors
    mean “To download a file…”. First sentence of the subsection A
    missed dot at the end. Dots and commas are also missed in
    subsection B. The words in the figures are written in Russian
    (must be corrected).


Review 2 (Reviewer C)
Your Overall Recommendation
Likely Accept (between top 10%-30% of the papers) (4)

Detailed Comments (Please justify your overall recommendation and
suggest improvements in technical content or presentation to the
author(s). What are the key strengths of the paper to warrant an
'accept'? What are the key weaknesses of the paper or main reasons to
reject it?)

    The paper considers an application of the developed technique of
    construction of the stochastic one-step processes to peer to peer
    protocols. In previous studies, the authors in particular, have
    applied this approach to population dynamic models. What are the
    key strengths of the paper to warrant an 'accept'? The main
    contribution of the paper is that the authors show that this
    approach can also be applied to model p2p-networks, in particular
    the FastTrack and BitTorrent. More exactly, they show that, to
    describe the initial value problem in such networks, the ordinary
    differential equations (Fokker-Planck equations) can be applied,
    while the boundary value problem can be formulated by means of the
    partial differential equations. What are the key weaknesses of the
    paper or main reasons to reject it? The main drawback of the paper
    is that the language is not acceptable and must be significantly
    improved. An example of an unclear phrase: “After as the desired
    file is found, it is directly sent to the node bypassing the
    supernode from the node possessing the necessary file [8], [9].”
    It would be also useful to enrich the list of
    references. Moreover, it is a mystery for me how to guess “an
    appropriate formula” in the following sentence: “stochastic
    differential equation in its Langevin’s form can be derived with
    help of an appropriate formula.” The pictures must be changed to
    delete Russification. Overall recommendation: the results can be
    presented at the conference, while the paper must be seriousy
    revised along the lines above before to be accpeted for pubication
    in the Proceedings of the conference.


Review 3 (Reviewer B)
Your Overall Recommendation
Borderline (between top 30%-50% of the papers) (3)

Detailed Comments (Please justify your overall recommendation and
suggest improvements in technical content or presentation to the
author(s). What are the key strengths of the paper to warrant an
'accept'? What are the key weaknesses of the paper or main reasons to
reject it?)

    In the paper authors introduced one-step stochastic processes and
    show how to construct such processes in order to model FastTrack
    and BitTorrent protocols. The paper is well written, especially
    the parts devoted to one-step stochastic processes construction
    for both above mentioned protocols, but some minor changes are
    needed. For example, Russian-language notation in figures should
    be translated into English. Also the authors are recommended to
    extend the bibliography by English-language sources on one-step
    stochastic processes and their usage for constructing the models
    of FastTrack and BitTorrent protocols. And the last remark is
    about a misprint in the first paragraph of part III (Markov
    process description).

