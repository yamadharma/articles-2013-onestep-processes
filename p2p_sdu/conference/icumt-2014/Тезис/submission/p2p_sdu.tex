\documentclass[conference]{IEEEtran}




\usepackage{amsmath}
\usepackage{amssymb}

\usepackage{mathtools}
\mathtoolsset{
showonlyrefs,
mathic = true
}

\allowdisplaybreaks

\usepackage{setspace}


\renewcommand{\d}{\mathrm{d}}
\newcommand{\e}{\mathrm{e}}
\renewcommand{\i}{\mathrm{i}}

\newcommand{\setC}{\mathbb{C}}
\newcommand{\setN}{\mathbb{N}}
\newcommand{\setR}{\mathbb{R}}
\newcommand{\setS}{\mathbb{S}}
\newcommand{\setZ}{\mathbb{Z}}
\newcommand{\setM}{\mathbb{M}}
\newcommand{\setV}{\mathbb{V}}

\newcommand{\crd}[1]{\underline{\vphantom{j}{#1}}}



\begin{document}

\graphicspath{{en/}}

\title{The method of constructing models \\ of peer to peer protocols
}

\author{\IEEEauthorblockN{A. V. Demidova\IEEEauthorrefmark{1},
A. V. Korolkova\IEEEauthorrefmark{1},
D. S. Kulyabov\IEEEauthorrefmark{1}, and
L. A. Sevastyanov\IEEEauthorrefmark{1}}
\IEEEauthorblockA{\IEEEauthorrefmark{1}Department of Applied Informatics and Probability Theory\\
Peoples' Friendship University of Russia\\
Miklukho-Maklaya str., 6, Moscow, 117198, Russia}}

\maketitle              % typeset the title of the contribution

\begin{abstract}
  The models of peer to peer protocols are presented with the help of
  one-step processes. On the basis of this presentation and the method
  of randomization of one-step processes, it is described  method for
  constructing models of peer to peer protocols. 
The models of FastTrack and
  Bittorrent protocols are studied by means of  proposed method.\footnote{978-1-4799-5291-5/14/\$31.00 \copyright 2014 IEEE}
\end{abstract}



\section{Introduction}

While constructing stochastic mathematical model there is a certain
problem how to introduce the stochastic term which deals not with
external impact on the system, but has a direct relationship with the
system's structure. In to order to construct a required mathematical
model we will consider the processes occurring in the system as
one-step Markov processes.

This approach allows to obtain stochastic differential equations with
compatible stochastic and deterministic parts, since they are derived
from the same equation. The stochastic differential equations theory
allows qualitatively to analyse the solutions of these equations. The
Runge--Kutta methods are used to obtain the solutions of stochastic
differential equations and for illustration of presented results.

In previous studies, the authors developed a method of construction of
mathematical model based on one-step stochastic processes, which
describes a wide class of phenomena~\cite{L_lit13, L_lit10::en}.  This
method presented good results for population dynamic
models~\cite{L_lit14::en, L_lit12::en, L_lit11::en}. This method is
also applicable to some technical problems such as p2p-networks
simulation, in particular to the FastTrack and
BitTorrent~\cite{kulyabov:2013:conference:mephi::en}.

The paper proposes to use one-step stochastic processes method in
order to construct FastTrack and BitTorrent protocol models and to
study stochastic influence on the deterministic model.

\section{Notations and conventions}
\label{sec:2}

\begin{enumerate}
\item In this paper the notation of abstract indices is
  used~\cite{penrose-rindler-1987::en}. Under the given notation, the
  tensor is denoted by an index (eg, $x^{i}$), and the tensor's
  components are denoted by an underlined index (eg, $x^{\crd{i}}$).

\item Latin indices of the middle of the alphabet (eg, $i$, $j$, $k$)
  denote system space vectors.  Latin indices from the beginning of
  the alphabet (eg, $a$) are related to the space of Wiener's
  process. Latin indices from the end of the alphabet (eg, $p$, $q$)
  are the indices of the Runge--Kutta methods. Greek indices (eg,
  $\alpha$) denote a quantity of different interactions in the kinetic
  equations.

\item A dot over the symbol (eg, $\dot{x}$) denotes time
  differentiation.

\item A comma in the index denotes the partial derivative with respect
  to corresponding coordinate.

\end{enumerate}


\section{One-step processes modeling}
\label{sec:onestep}


Under the one-step process we understand the continuous time Markov
processes with integer state space. The transition matrix which allows
only transitions between neighboring states. Also, these processes are
known as birth-and-death processes.
The state of the system is described by a state vector $x^{i} \in
\setR^n$, where $n$~--- system dimension.

The idea of the method is as follows. For the studied system the
interaction scheme as a symbolic record of all possible interactions
between system elements is introduced. The scheme shows the number and
type of elements in certain interaction and the result of the
interaction. For this purpose the system state operators are used. The
operator $n^{i \alpha}_{j} \in \setZ^{n}_{{}\geqslant 0} \times
\setZ^{n}_{{}\geqslant 0} \times \setZ^{s}_{0}$ sets the state of the
system before the interaction, and the operator $m^{i \alpha}_{j} \in
\setZ^{n}_{{}\geqslant 0} \times \setZ^{n}_{{}\geqslant 0} \times
\setZ^{s}_{0}$~--- sets the state after the interaction. It is also
assumed that in the system $s$ kinds of different interactions may
occur, where $s\in \setZ_{+}$. As a result of the interaction , the
system switches into the $x^{i} \rightarrow x^i + r^{i
  \crd{\alpha}}_{j} x^{j}$ or $x^{i} \rightarrow x^{i} - r^{i
  \crd{\alpha}}_{j} x^{j}$ states, where $r_j^{i \alpha} = m_j^{i
  \alpha} -n_j^{i \alpha}$.


Let us introduce transition probabilities from state $x^{i}$ into
states $x^{i} + r^{i \crd{\alpha}}_{j} x^{j}$ (the state $x^{i} - r^{i
  \crd{\alpha}}_{j} x^{j}$). The transition probabilities are assumed
to be proportional to the number of possible interactions between
elements.

Based on the interaction schemas and transition probabilities, we
create the master equation, decompose it into a series, leaving only
the terms up to the second derivative. The resulting equation is the
Fokker--Planck equation, which looks like:
\begin{equation}
  \label{eq:FP}
  \frac{\partial p}{\partial t} = -
  \partial_{i} \left[ A^{i} p \right] + \frac{1}{2} \partial_{i} \partial_{j} \left[
    B^{i j}p \right],
\end{equation}
where
\begin{equation}
  \label{eq:kFP}
  \begin{gathered}
    A^{i} := A^{i}(x^{k}, t) = r^{i \crd{\alpha}} \left[ s^+_{\crd{\alpha}} - s^-_{\crd{\alpha}} \right], \\
    B^{i j} := B^{i j}(x^{k},t) = r^{i \crd{\alpha}} r^{j
      \crd{\alpha}} \left[ s^+_{\crd{\alpha}} - s^-_{\crd{\alpha}}
    \right].
  \end{gathered}
\end{equation}

Here $p := p(x^{i},t)$ is a density function of random variable
  % is a random variable
$x^{i}$ density function, $A^{i}$ --- a drift vector, $B^{i j}$ --- a
diffusion vector.

As it is evident from \eqref{eq:kFP}, the Fokker--Planck equation
coefficients can be obtained immediately from interaction scheme and
transition probabilities, i.e., for practical calculations the master
equation is not in need.

To get the more convenient form of the model the corresponding
Langevin's equation is given:
\begin{equation}
  \label{eq:langevin}
  \d x^{i} = a^{i} \d t + b^i_{a} \d W^{a},
\end{equation}
where $a^{i} := a^{i} (x^k, t)$, $b^{i}_{a} := b^{i}_{a} (x^k, t)$,
$x^i \in \setR^n $, $W^{a} \in \mathbb{R}^m$ --- $m$-dimensional
Wiener's process. It is implemented as $\d W = \varepsilon \sqrt{\d
  t}$, where $\varepsilon \sim N(0,1)$~--- normal distribution with
mean equal to$0$ and variance equal to $1$.

The relationship between the equations \eqref{eq:FP} and
\eqref{eq:langevin} is expressed by the following way:
\begin{equation}
  \label{eq:k-langevin}
  A^{i} = a^{i}, \quad B^{i j} = b^{i}_{a} b^{j a}.
\end{equation}



Thus, for our system description the stochastic differential equation
may be derived to from the general considerations.  This equation
consists of two parts, one of which describes a deterministic
behaviour of the system, and other describes the stochastic
one. Furthermore, both sides of the equations are consistent, since
they are derived from the same equation (Figure~\ref{fig:met}).

\begin{figure}[!h]
  \centering
  \includegraphics[width=\linewidth]{met}
  \caption{Method's diagram}
\label{fig:met}
\end{figure}

\section{Fast Track Protocol}

Fast Track Protocol is the p2p network protocol for Internet file
sharing.  The file can be downloaded only from peers which possess the
whole file.  FastTrack was originally implemented in the KaZaA
software. It has a decentralized topology that makes it very
reliable. All FastTrack users are divided into two classes: supernodes
and ordinary nodes.  The supernodes allocation is one of the functions
of FT protocol. Supernodes are those with fast network connection,
high-bandwidth and possibility of a fast data processing.  The users
themselves do not know that their computer has been designated as a
supernode.

To upload a file, a node sends a request to the supernode, which, in
its turn, communicates with the other nodes, etc. So, the request
extends to a certain level protocol network which is called a lifetime
request. After as the desired file is found, it is directly sent to
the node bypassing the supernode from the node possessing the
necessary file~\cite{ft1, ft2}.

\subsection{FastTrack modeling }

Assume that the file consists of one part Thus, during one
interaction, the node desiring to download it can download the entire
file at once. When the download is completed, the node becomes
supernode.

Let $N$ denote the new node, $L$ --- supernode and $\beta$ ---
interaction coefficient. The new nodes appear with the intensity of
$\lambda$, and the supernodes leave the system with the intensity of
$\mu$.  Then the scheme of interaction and vector $\mathbf{r}$ are:
\begin{equation} 
  \label{ft:1}
  \begin{cases}
    0 \xrightarrow{\lambda } N, & r^{\crd{i}1}=(1,0), \\
    N+L \xrightarrow{\beta } 2L, & r^{\crd{i}2}=(-1,1),\\
    L \xrightarrow{\mu} 0, & r^{\crd{i}3}=(0,-1).
  \end{cases}
\end{equation}

The first line in the diagram describes the a new client appearance in
the system. The second line reflects the interaction of a new client
and a seed. After this interaction a new seed appears.  And the third
line indicates the departure of the seed from the system.  Let us
introduce the transition probabilities:
% \begin{equation} 
%   \label{ft:2}
%   \begin{gathered}
%     s^{+}_1 (n,l) = \lambda, \\
%     s^{+}_2 (n,l) = \beta nl, \\
%     s^{+}_3 (n,l) = \mu l.
%   \end{gathered}
% \end{equation}
\begin{equation} 
  \label{ft:2}
    s^{+}_1 (n,l) = \lambda, \quad
    s^{+}_2 (n,l) = \beta nl, \quad
    s^{+}_3 (n,l) = \mu l.
\end{equation}

It is possible now to write out the Fokker-Planck equation for our
model:
\begin{equation}
  \label{ft:3} 
  \frac{\partial p(n,l)}{\partial t} = {\partial_i}
  (A^i(n,l) p(n,l)) + \frac{1}{2} {\partial_i \partial_j} (B^{ij}(n,l)
  p(n,l)),
\end{equation}
where the drift vector and diffusion matrix are follows:
\begin{equation}
  \begin{gathered}
    A^i: = A^i(x^k,t)= r^{i\crd{\alpha}}s^+_{\crd{\alpha}} (n,l) ,\\
    B^i:= B^{ij}(x^k,t) = r^{i\crd{\alpha}}r^{i\crd{\alpha}}
    s^+_{\crd{\alpha}} (n,l), \quad \crd{\alpha}=1,2,3.
  \end{gathered}
\end{equation}


At last we get:
\begin{equation} 
  \label{ft:4}
  \begin{gathered}
    \mathbf A =
    \begin{pmatrix}
      1\\
      0
    \end{pmatrix}
    \lambda +
    \begin{pmatrix}
      -1\\
      1
    \end{pmatrix}
    \beta n l +
    \begin{pmatrix}
      0\\
      -1
    \end{pmatrix}
    \mu l =
    \begin{pmatrix}
      \lambda - \beta n l\\
      \beta n l - \mu l
    \end{pmatrix}, \\
    \begin{multlined}
      \mathbf B =
      \begin{pmatrix}
        1\\
        0
      \end{pmatrix}
      (1,0) \lambda +
      \begin{pmatrix}
        -1\\
        1
      \end{pmatrix}
      (-1,1) \beta n l +
      \begin{pmatrix}
        0\\
        -1
      \end{pmatrix}
      (0,-1) \mu l = \\ =
      \begin{pmatrix}
        \lambda + \beta n l & - \beta n l \\
        - \beta n l & \beta n l + \mu l
      \end{pmatrix}.
    \end{multlined}
  \end{gathered}
\end{equation}

The stochastic differential equation in its Langevin's form can be
derived by using an appropriate formula.

\subsection{Deterministic behavior}

Since the drifts vector $A$ completely describes the deterministic
behavior of the system, it is possible to derive an ordinary
differential equations system, which describes the population dynamics
of new clients and seeds.
\begin{equation}
  \label{ft:5} 
  \left \{
    \begin{aligned}
      \frac{dn}{d t}&=     \lambda - \beta n l,\\
      \frac{dl}{d t}&= \beta n l - \mu l.
    \end{aligned}
  \right.
\end{equation}


\subsubsection{Steady-states}

Let us find steady-states of the system~\eqref{ft:5} from the
following system of equations:
\begin{equation} 
  \label{ft:6} 
  \left \{
    \begin{aligned}
      \lambda - \beta n l &=0,\\
      \beta n l - \mu l &=0.
    \end{aligned}
  \right.
\end{equation}

The system~\eqref{ft:5} has the only one steady state:
\begin{equation}
  (\bar{n},\bar{l})= \left ( \frac{\mu }{\beta }, \frac{\lambda }{\mu }
  \right ).
\end{equation}


To linearize the system~\eqref{ft:5}, let 
\[
n=\bar{n} + \xi ,\quad
l=\bar{l} + l\eta,
\]
 where $\bar{n}$ and $\bar{l}$ are coordinates of
stability points, $\xi$ and $\eta $ are small parameters
\begin{equation}
  \label{ft:7} 
  \left\{
    \begin{aligned}
      \frac{d\xi }{d t}&=-\beta \bar{n} \eta- \beta \bar{l} \xi, \\
      \frac{d\eta }{d t}&=\beta \bar{n} \eta + \beta \bar{l} \xi - \mu
      \eta.
    \end{aligned}
  \right.
\end{equation}

In the neighborhood of the equilibrium point, the linearized system is
presented as following:
\begin{equation}
  \label{ft:8} 
  \left\{
    \begin{aligned}
      \frac{d\xi }{d t}&= - \mu \eta \frac{\beta \lambda }{\mu}\xi,  \\
      \frac{d\eta }{d t}&= \frac{\beta \lambda }{\mu}\xi.
    \end{aligned}
  \right.
\end{equation}

Now we may find the eigenvalues of the characteristic equation:
\begin{equation}
  \label{ft:9} 
  s^2+\frac{\beta \lambda }{\mu} s + \beta
  \lambda =0.
\end{equation}


  The roots of this characteristic equation:
\begin{equation}
  \label{ft:10} 
  s_{1,2}= \frac{1}{2} \left(
    -\frac{\beta \lambda }{\mu} \pm \sqrt{ \left( \frac{\beta \lambda
        }{\mu} \right)^2 - 4 \beta \lambda} \right).
\end{equation}

Thus, depending on the choice of parameters, the critical point has
different types. In the case when $\beta\lambda < 4\mu^2$, the
critical point represents a stable focus, while in the reverse case
--- a steady node.  In both the cases, the singular point is a stable
one because the real part of the roots of the equation is
negative. Thus, depending on the choice of coefficient, the change of
values of the variables can occur in one of two trajectories.

In the case when the critical point represents a focus, the damped
oscillations of the nodes and supernodes quantity
occur~Figure~\ref{fig:ft1}. And if the critical point is a node, there are no
oscillations in the trajectories~in Figure~\ref{fig:ft2}. 
% And if the critical point is node, there are
% no oscillations in the trajectories~on Fig.~\ref{fig:ft2}.
Phase portraits of the system for each of the two cases are plotted,
respectively, in Figures~\ref{fig:ft3} and~\ref{fig:ft4}.

\begin{figure}[!h]
  \centering
  \includegraphics[width=0.96\linewidth]{1}
    \caption{The time dependence of the nodes and seeds quantity 
      in the Fast Track network for the deterministic case 
      $\beta \lambda < 4\mu^2$.}
  \label{fig:ft1}
\end{figure}

\begin{figure}[!h]
  \centering
  \includegraphics[width=0.96\linewidth]{2}
    \caption{The time dependence of the nodes and seeds quantity  
      in Fast Track network for the deterministic case $\beta \lambda >
      4\mu^2$.}
  \label{fig:ft2}
\end{figure}

\begin{figure}[!h]
  \centering
  \includegraphics[width=\linewidth]{3}
  \caption{Phase portraits of a deterministic Fast Track system with
    various deviations $(\Delta x, \Delta y)$ from stationary point if
    $\beta \lambda < 4\mu^2$.}
  \label{fig:ft3}
\end{figure}

\begin{figure}[!h]
  \centering
  \includegraphics[width=\linewidth]{4}
  \caption{Phase portraits of a deterministic Fast Track system with
    various deviations $(\Delta x, \Delta y)$ from stationary point if
    $\beta \lambda > 4\mu^2$.}
  \label{fig:ft4}
\end{figure}

\subsubsection{Numerical simulation of the stochastic model}

To illustrate the obtained results the numerical modelling of
stochastic differential equation in the Langevin's form was
performed. The extension of Runge-Kutta methods for stochastic
differential equations was applied~\cite{L_lit04, L_lit01}, and a
Fortran program for this extension was written. The results are
presented on Figures~\ref{fig:ft5} and~\ref{fig:all_sft}.

\begin{figure}[!h]
  \centering
  \includegraphics[width=\linewidth]{sft_graph}
  \caption{The time dependence of new nodes and seeds quantity 
     in the FastTrack network for the stochastic case.}
\label{fig:sft_graph}
\end{figure}

\begin{figure}[!h]
  \centering
  \includegraphics[width=\linewidth]{5}
  \caption{Phase trajectories of the stochastic FastTrack model with
    various deviations $(\Delta x, \Delta y)$ from the stationary point
    $\beta \lambda > 4\mu^2$.}
\label{fig:ft5}
\end{figure}

\begin{figure}[!h]
  \centering
  \includegraphics[width=\linewidth]{all_sft}
  \caption{Phase trajectories of the stochastic FastTrack model with
    various deviations $(\Delta x, \Delta y)$ from the stationary point
    $\beta \lambda > 4\mu^2$.}
\label{fig:all_sft}
\end{figure}

Figures~\ref{fig:ft5} and~\ref{fig:all_sft} clearly indicate that
small stochastic terms do not substantially affect the behaviour of
the system in the stationary point neighbourhood.  The stochastic term
influence exists only on the early evolution of the system.  After a
relatively short period of time, the system enters the steady-state
regime and differs little from the deterministic case.



\subsubsection{Conclusions}
The obtained results indicate that the stochastic introduction in the
steady-state regime has little effect on the behaviour of the
system. So, the deterministic model provides the appropriate results.

Furthermore, the proposed method allows to extend the framework of the
tools used for the analysis, so is becomes possible to use ordinary
stochastic differential equation (Langevin) and partial differential
equation (Fokker-Planck) simultaneously.  Furthermore, as the above
example indicates, in some cases a deterministic approach defined by
the diffusion matrix is sufficient.





\section{BitTorrent protocol}

BitTorrent is the p2p-network protocol for file sharing over the
Internet.  Files are transferred by chunks. Each torrent-client
simultaneously downloads the needed chunks from one node and uploads
available chunks to another node. It makes the BitTorrent protocol
more flexible then the FastTrack one.

\subsection{Modeling}

First, we consider a simplified model of a closed system, where the
numbers of leechers and seeders are constant.  Furthermore, we assume
that the file consists of one chunk.  Thus, the leecher downloads the
file during only one time step and then becomes a seeder.

Let $N$ denote a new leecher, $C$ ---seeder, and $\beta$ ---
interaction coefficient. Then the interaction scheme will be:
\begin{equation} 
\label{bt:1}
N+C \xrightarrow{\beta } 2C, \quad r^{\crd{i}2}=(-1,1).
\end{equation}

The scheme reflects that after the leecher interaction with the
seeder, the leecher disappears and another seeder appears.

Let $n$ be the number of new nodes, and $c$ --- the number of seeders
in the system.

The transition probabilities:
\begin{equation} 
\label{bt:2}
s^{+} (n,c) = \beta nc.
\end{equation}

The Fokker--Planck's equation for this model:
\begin{equation} 
\label{bt:3}
\frac{\partial p(n,c)}{\partial t} = 
{\partial_i} (A^i(n,c) p(n,c))
+\frac{1}{2} {\partial_i \partial_j} (B^{ij}(n,c) p(n,c)),
\end{equation}
where the drift vector and the diffusion matrix are presented as
following:
\begin{equation}
  \begin{gathered}
     A^i(n,c)= r^{i\crd{\alpha}}s^+_{\crd{\alpha}} (n,l) ,\\
     B^i(n,c) = r^{i\crd{\alpha}}r^{i\crd{\alpha}} s^+_{\crd{\alpha}} (n,l).
  \end{gathered}
\end{equation}

Thus, we obtain:
\begin{equation} 
\label{bt:4}
  \begin{gathered}
    \mathbf A =
    \begin{pmatrix}
      -1\\
      1
    \end{pmatrix}
    \beta n c +
     =
    \begin{pmatrix}
      - \beta n l\\
      \beta n l 
    \end{pmatrix}, \\
    \begin{multlined}
      \mathbf B =
       \begin{pmatrix}
        -1\\
        1
      \end{pmatrix}
      (-1,1) \beta n c  = 
      \begin{pmatrix}
         \beta n c & - \beta n c \\
        - \beta n c& \beta n c
      \end{pmatrix}.
    \end{multlined}
  \end{gathered}
\end{equation}


The stochastic differential equation in the Langevin's form can be
obtained with the help of the appropriate formula.

It is also possible to write out differential equations which describe
the deterministic behaviour of the system:
\begin{equation} 
\label{bt:5}
\left \{
\begin{aligned}
 \frac{dn}{d t}&=      - \beta n c,\\
 \frac{dc}{d t}&=     \beta n c. 
\end{aligned}
\right.
\end{equation}

Next, we consider the open system in which new clients appear with the
intensity $\lambda $, and seeders leave it with the intensity $\mu
$. Now, the scheme of interaction has the form of:
\begin{equation} 
\label{bt:6}
\begin{gathered}
0 \xrightarrow{\lambda } N, \quad r^{\crd{i}1}=(1,0),\\
N+C \xrightarrow{\beta } 2C, \quad r^{\crd{i}2}=(-1,1),\\
C  \xrightarrow{\mu } 0, \quad r^{\crd{i}3}=(0,-1).
\end{gathered}
\end{equation}


The first line in the scheme describes the appearance of the new peer in 
the system, the second line describes the interactions between the new 
peer and the seeder, after which a new seeder appears. And the third 
line meaning is that the seeder leaves the system. 

Let $n$ denote the number of new clients and $c$ --- the number of
seeders in the system.

This system is equivalent to the Fast Track model up to notation. 

Now consider a system in which downloaded files consist of $m$ chunks.
The system consists of:
\begin{itemize}
\item Peers ($N$) are the clients without any chunk of the file.
\item Leechers ($L$) are the clients who have already downloaded a
  number of chunks of the file and can share them with new peers or
  other leechers.
\item Seeders ($C$) are the clients who have the whole file and they
  only can share the file.
\end{itemize}

In addition, $n$ is the number of new peers, and $c$ --- number of
seeders in the system, $l_i$ --- number of leechers with exactly $i$
chunks of the file, where $i = \overline{i, n-1}$. Also, let $\bar
{L}_i$ be the number of leechers with any chunk of the file of
interest for leecher $ L_i $ and the $ \bar{l}_i $ is their amount.

For this scheme it is possible to write out the following types of
relations:
\begin{equation}
\label{bt:7}
\begin{aligned}
0  \xrightarrow{\lambda } & N, \\
N+C  \xrightarrow{\beta } & L_1+C, \\
N+L_i  \xrightarrow{\beta_i } & L_1+L_i, \\
L_i + \bar{L}_i  \xrightarrow{\delta_i } & L_{i+1}+\bar{L}_i, \\
L_i + C  \xrightarrow{\gamma_i } & L_{i+1}+C, \\
L_{m-1} + \bar{L}_{m-1}  \xrightarrow{\gamma_{m-1} } & C+\bar{L}_{m-1}, \\
L_{m-1} + C  \xrightarrow{\gamma } & 2C, \\
C  \xrightarrow{\mu } & 0. 
\end{aligned}
\end{equation}

On every interaction step one chunk of file is transferred from one
peer to another. The first relation describes the appearance of a new
peer in a system with the intensity $\lambda$.

The second and third relations describe the interaction of a new peer
with a seeder or a leecher with the interaction coefficients $\beta$
and $\beta i$, $(i=\overline{i, m-1})$. As the result of interaction,
the peer transforms into a leecher from the $L_1$ class.  The fourth
and fifth relations describe the leecher $L_i$ interaction with the
seeder and other leechers with the coefficients $\delta_i$ and
$\gamma_i$ $(i=\overline{i, m-2})$.  As the result of this
interaction, the leecher gets one chunk of a file and becomes the
$L_{i+1}$-class leacher.  The sixth and seventh relations describe the
transformation of leecher into seeders with the coefficients
$\gamma_{m-1} $ and $\gamma$ (the leecher downloads the last file
chunk). The last relation describes the seeder departure from the
system with the intensity $\mu$.

The vectors $r^{i\crd{\alpha}}=(n,l_1,l_2,...,l_{m-1},c)$ and
transition probabilities $s^+_{\crd{\alpha}}$:
\begin{equation} 
\label{bt:8}
\begin{gathered}
r^{1}  =(1,0,0,...,0), \\
r^{2}  =r_i^3=(-1,1,0,...,0), i=\overline{i, m-1}, \\
r_i^4  =r_i^5=(0,...,-1,1,...,0), i=\overline{i, m-2}, \\
r^{6}  =r^7=(0,0,...,-1,1), \\
r^{8}  =(0,0,...,-1);
\end{gathered}
\end{equation}
\begin{equation} 
\label{bt:9}
\begin{gathered}
s^{+}_1  =\lambda, \\
s^{+}_2  =\beta n c, \\
s^{+}_{3i}  =\beta_i n l_i, \\
s^{+}_{4i}  =\delta_i l_i \bar{l}_i, i=\overline{i, m-1}\\
s^{+}_{5i}  =\gamma_i l_i c, i=\overline{i, m-2}\\
s^{+}_{6}  =\gamma_{m-1} l_{m-1} \bar{l}_{m-1}, \\
s^{+}_{7}  =\gamma l_{m-1} c, \\
s^{+}_{8}  =\mu c. \\
\end{gathered}
\end{equation}

For this model, which is similar to the previous one, we can write out
the Fokker--Planck's equation. But for deterministic behaviour
description, it's enough to write out the matrix $A$:

\begin{equation}
\label{bt:10}
\mathbf A =
\begin{pmatrix}
  \lambda - \beta n c - \sum_{i=1}^{m-1} \beta_i n l_i \\
  \beta n c + \sum_{i=1}^{m-1} \beta_i n l_i -\delta_1 l_1 \bar{l}_1 - \gamma_1 l_1 c \\
  \delta_1 l_1 \bar{l}_1 + \gamma_1 l_1 c -  \delta_2 l_2 \bar{l}_2 - \gamma_2 l_2 c  \\
  \ldots \\
     \begin{multlined}
       \delta_{m-2} l_{m-2} \bar{l}_{m-2} + \gamma_{m-2} l_{m-2} c -
       {} \\ {} -
       \delta_{m-1} l_{m-1} \bar{l}_{m-1} - \gamma_{m-1} l_{m-1} c 
     \end{multlined}
     \\ 
      \delta_{m-1} l_{m-1} \bar{l}_{m-1} + \gamma_{m-1} l_{m-1} c  - \mu c 
    \end{pmatrix}.
\end{equation}


As a result, we obtain a system of differential equations describing
the dynamics of new peers, leechers and seeders:
\begin{equation} \label{bt:11}
\left \{
\begin{aligned}
&  \frac{d n}{d t} =      \lambda - \beta n c - \sum\limits_{i=1}^{m-1} \beta_i n l_i, \\
&  \frac{d l_1}{d t}=     \beta n c + \sum\limits_{i=1}^{m-1} \beta_i n l_i -\delta_1 l_1 \bar{l}_1 - \gamma_1 l_1 c, \\
&  \frac{d l_2}{d t}=  \delta_1 l_1 \bar{l}_1 + \gamma_1 l_1 c -  \delta_2 l_2 \bar{l}_2 - \gamma_2 l_2 c,  \\
&  \ldots \\
&\begin{multlined}
 \frac{d l_{m-1}}{d t}=  \delta_{m-2} l_{m-2} \bar{l}_{m-2} + 
 \gamma_{m-2} l_{m-2} c - {} \\ {} - 
 \delta_{m-1} l_{m-1} \bar{l}_{m-1} -
 \gamma_{m-1} l_{m-1} c,
\end{multlined}
\\
&\frac{d c}{d t}= \delta_{m-1} l_{m-1} \bar{l}_{m-1} + \gamma_{m-1}
l_{m-1} c - \mu c.
\end{aligned}
\right.
\end{equation}

Let us suppose that
$\delta=\delta_{1}=\delta_{2}=...=\delta_{m-1}=const$, then let's sum
the equations in our system from the second one to the $m+1$-th. If we
denote leechers and seeders as $l = l_1 + l_2 + ... + l_{m-1} + c$ the
system of the equations may be simplified as follows:
\begin{equation} 
\label{bt:12}
\left \{
\begin{aligned}
& \frac{d n}{d t}=      \lambda - \beta n (l+c), \\
& \frac{d (l+c)}{d t}=     \beta n (l+c)  - \mu c. 
\end{aligned}
\right.
\end{equation}

\section{Conclusion}

\begin{enumerate}
\item In this paper the method of stochastic models construction by
  use of one-step stochastic processes is described.  The proposed
  method provides an universal algorithm of deriving stochastic
  differential equations for such systems. It is also shown that there
  are two way of stochastic system's description: with the help of
  partial differential equation (Fokker-Plank) and ordinary
  differential equations (Langevin).

\item In order to study influence of the stochastic term of an
  equation the Fast Track and Bittorrent protocol models were
  discussed. The results of this study indicate, that near the
  stationary points the stochastic influence is minimal, and that is
  why the deterministic model gives very good results. In addition, as
  it was shown by the above example, in some cases, it is enough to
  study its deterministic approximation, which is described by the
  drift matrix in order to examine the system.
\end{enumerate}


\bibliographystyle{IEEEtran}

\bibliography{p2p_sdu}




\end{document}


