\RequirePackage{fixltx2e}
\RequirePackage{fix-cm}

\documentclass[%
floatfix,
showkeys,
twocolumn, %
nofootinbib, %
superscriptaddress, %
]{revtex4-1}


\usepackage{cmap}
\usepackage{mathtext}

\usepackage{ucs}
\usepackage[utf8x]{inputenc}
\usepackage[T1,T2A]{fontenc}
\usepackage[english,german,russian]{babel}


\makeatletter
\@ifpackageloaded{ucs}{%
  \PrerenderUnicode{АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЫЪЭЮЯабвгдеёжзийклмнопрстуфхцчшщьыъэюя–}
}
\makeatother
\usepackage{amsmath}
\usepackage{amssymb}

\usepackage{mathtools}
\mathtoolsset{
showonlyrefs,
mathic = true
}

\allowdisplaybreaks

\usepackage{hyperref}
\hypersetup{backref,
 colorlinks=false}
\hypersetup{pdfborder=0 0 0}

\usepackage{breakurl}

\usepackage{microtype}
\UseMicrotypeSet[protrusion]{alltext}

\usepackage{fixltx2e}

\usepackage{nicefrac}

\usepackage{physymb}

\makeatletter
\def\ps@pprintTitle{%
     \let\@oddhead\@empty
     \let\@evenhead\@empty
     \let\@oddfoot\@empty
     \let\@evenfoot\@oddfoot}
\makeatother
\usepackage{setspace}


\renewcommand{\d}{\mathrm{d}}
\newcommand{\e}{\mathrm{e}}
\renewcommand{\i}{\mathrm{i}}

\newcommand{\setC}{\mathbb{C}}
\newcommand{\setN}{\mathbb{N}}
\newcommand{\setR}{\mathbb{R}}
\newcommand{\setS}{\mathbb{S}}
\newcommand{\setZ}{\mathbb{Z}}
\newcommand{\setM}{\mathbb{M}}
\newcommand{\setV}{\mathbb{V}}

\newcommand{\crd}[1]{\underline{\vphantom{j}{#1}}}
\begin{document}
\graphicspath{{image/p2p_sdu/ru/}}


\title{Методика построения моделей пиринговых протоколов}

\author{А. В. Демидова}
\email{avdemidova@sci.pfu.edu.ru}
\affiliation{Кафедра прикладной информатики и теории вероятностей,\\
  Российский университет дружбы народов,\\
  ул. Миклухо-Маклая, д.6, Москва, Россия, 117198}

\author{А. В. Королькова}
\email{avkorolkova@gmail.com}
\affiliation{Кафедра прикладной информатики и теории вероятностей,\\
  Российский университет дружбы народов,\\
  ул. Миклухо-Маклая, д.6, Москва, Россия, 117198}

\author{Д. С. Кулябов}
\email{yamadharma@gmail.com}
\affiliation{Кафедра прикладной информатики и теории вероятностей,\\
  Российский университет дружбы народов,\\
  ул. Миклухо-Маклая, д.6, Москва, Россия, 117198}
\affiliation{Лаборатория информационных технологий,\\
Объединённый институт ядерных исследований,\\
ул. Жолио-Кюри 6, Дубна, Московская область, Россия, 141980}

\author{Л. А. Севастьянов}
\email{leonid.sevast@gmail.com}
\affiliation{Кафедра прикладной информатики и теории вероятностей,\\
  Российский университет дружбы народов,\\
  ул. Миклухо-Маклая, д.6, Москва, Россия, 117198}
\affiliation{Лаборатория теоретической физики,\\
  Объединённый институт ядерных исследований,\\
  ул. Жолио-Кюри 6, Дубна, Московская область, Россия, 141980}






  \thanks{Опубликовано в: 
    \emph{Demidova~A.~V., Korolkova~A.~V., Kulyabov~D.~S.,
      Sevastianov~L.~A.}  The method of constructing models of peer to
    peer protocols~// Applied Problems in Theory of Probabilities and
    Mathematical Statistics Related to Modeling of Information
    Systems. The 6th International Congress on Ultra Modern
    Telecommunications and Control Systems. Saint-Petersburg, Russia.
    October 6-8, 2014.~--- IEEE, 2014.~--- P.~657--662.  }

\thanks{Исходные тексты:
  \url{https://bitbucket.org/yamadharma/articles-2013-onestep-processes}}



\begin{abstract}
  Пиринговые протоколы представлены как одношаговые процессы. На
  основе этого представления и метода стохастизации одношаговых
  процессов описывается методика построения моделей пиринговых
  протоколов. В качестве конкретных реализаций приводятся модели
  протоколов Fasttrack и базового Bittorrent-подобного протокола.

\end{abstract}


  \keywords{стохастические дифференциальные уравнения; основное
    кинетическое уравнения; уравнение Фоккера--Планка; FastTrack;
    BitTorrent}

\maketitle


\section{Введение}

  При стохастизации математических моделей возникает проблема, как
  ввести стохастический член, который интерпретируется не как внешнее
  случайное воздействие на систему, а имеет непосредственную связь с
  ее структурой. Для получения стохастических моделей предлагается
  рассматривать процессы, происходящие в системе, как одношаговые
  марковские процессы. Такой подход позволяет получать стохастические
  дифференциальные уравнения с согласованными стохастической и
  детерминистической частями, так как они выводятся из одно и того же
  уравнения. Привлечение теории стохастических дифференциальных
  уравнений позволяет провести качественный и численный анализ
  поведения решений уравнений для полученной стохастической модели.
  Для иллюстрации результатов предлагается использовать численные
  Рунге--Кутты разных порядков построения решений стохастических
  дифференциальных уравнений.

  В предыдущих работах авторов разработан метод построения одношаговых
  стохастических моделей, который позволяет моделировать широкий класс
  явлений~\cite{L_lit13, L_lit10}. Данный метод показал хорошие
  результаты для популяционной динамики~\cite{L_lit14, L_lit12,
    L_lit11} . Его также можно применить к техническим задачам таким
  как peer-to-peer сети, в частности к моделированию протокола
  FastTrack и BitTorrent~\cite{kulyabov:2013:conference:mephi}.

  В работе предлагается применение данного метода для построения
  моделей протоколов FastTrack и BitTorrent и изучение влияния
  введения стохастики в детерминистическую модель.


\section{Обозначения и соглашения}
\label{sec:2}

  \begin{enumerate}
  \item В работе используется нотация абстрактных
    индексов~\cite{penrose-rindler-1987}. В данной нотации тензор как
    целостный объект обозначается просто индексом (например, $x^{i}$),
    компоненты обозначаются подчёркнутым индексом (например,
    $x^{\crd{i}}$).

  \item Будем придерживаться следующих соглашений. Латинские индексы
    из середины алфавита ($i$, $j$, $k$) будут относиться к
    пространству векторов состояний системы. Латинские индексы из
    начала алфавита ($a$) будут относиться к пространству винеровского
    процесса. Латинские индексы из конца алфавита ($p$, $q$) будут
    относиться к индексам метода Рунге--Кутты. Греческие индексы
    ($\alpha$) будут задавать количество разных взаимодействий в
    кинетических уравнениях.

  \item Точкой над символом обозначается дифференцирование по времени.

  \item Запятой в индексе обозначается частная производная по
    соответствующей координате.

  \end{enumerate}

\section{Моделирование одношаговых процессов}
\label{sec:onestep}

  Под одношаговыми процессами мы будем понимать марковские процессы с
  непрерывным временем, принимающие значения в области целых чисел,
  матрица перехода которых допускает только переходы между соседними
  участками. Также эти процессы известны под названиями процессов
  рождения--гибели.

  Состояние системы будем описывать вектором состояния $x^{i} \in
  \setR^n$, где $n$~--- размерность системы

  Идея метода состоит в следующем. Для исследуемой системы, состояние
  которой будем описывать вектором состояния $x^{i} \in \setR^n$, где
  $n$~--- размерность системы, можно записать схему
  взаимодействия. Т.е. символическую запись всех возможных
  взаимодействий между элементами системы, которая показывает сколько
  и каких элементов во взаимодействие какого типа вступают и что
  получилось в результате. Для этого используются операторы состояния
  системы. Оператор $n^{i \alpha}_{j} \in \setZ^{n}_{{}\geqslant 0}
  \times \setZ^{n}_{{}\geqslant 0} \times \setZ^{s}_{0}$ задаёт
  состояние системы до взаимодействия, оператор $m^{i \alpha}_{j} \in
  \setZ^{n}_{{}\geqslant 0} \times \setZ^{n}_{{}\geqslant 0} \times
  \setZ^{s}_{0}$~--- после. Также считается, что в системе может
  происходить $s$ видов различных взаимодействий, где $s\in
  \setZ_{+}$. И в результате взаимодействия система переходит в
  состояние $x^{i} \rightarrow x^i + r^{i \crd{\alpha}}_{j} x^{j}$ или
  $x^{i} \rightarrow x^{i} - r^{i \crd{\alpha}}_{j} x^{j}$, где
  $r_j^{i \alpha} = m_j^{i \alpha} -n_j^{i \alpha}$~---оператор
  изменения состояния системы.


  Далее предлагается записать вероятности переходов из состояния
  $x^{i}$ в состояние $x^{i} + r^{i \crd{\alpha}}_{j} x^{j}$ (в
  состояние $x^{i} - r^{i \crd{\alpha}}_{j} x^{j}$), которые
  предполагаются пропорциональными числу возможных взаимодействий
  между элементами.

  % и определяются выражениями:
  % \begin{equation}
  %   \label{eq:s-pm}
  %   \begin{gathered}
  %     s^+_{\alpha} = k^{+}_{\alpha} \prod_{\crd{i}}
  %     \frac{x^{\crd{i}}!}{(x^{\crd{i}} - n^{\crd{i} \alpha})!},       \\
  %     s^-_{\alpha} = k^{-}_{\alpha} \prod_{\crd{i}}
  %     \frac{x^{\crd{i}}!}{(x^{\crd{i}}-m^{\crd{i} \alpha})!}.
  %   \end{gathered}
  % \end{equation}


  На основании схем взаимодействия и вероятностей переходов мы строим
  основное кинетическое уравнение, раскладываем его в ряд, оставляя
  только члены до второй производной включительно. Получившееся
  уравнение будет уравнением Фоккера--Планка, которое будет иметь вид:


\begin{equation}
  \label{eq:FP}
  \frac{\partial p}{\partial t} = -
  \partial_{i} \left[ A^{i} p \right] + \frac{1}{2} \partial_{i} \partial_{j} \left[
    B^{i j}p \right],
\end{equation}
  где
\begin{equation}
  \label{eq:kFP}
  \begin{gathered}
    A^{i} := A^{i}(x^{k}, t) = r^{i \crd{\alpha}} \left[ s^+_{\crd{\alpha}} - s^-_{\crd{\alpha}} \right], \\
    B^{i j} := B^{i j}(x^{k},t) = r^{i \crd{\alpha}} r^{j
      \crd{\alpha}} \left[ s^+_{\crd{\alpha}} - s^-_{\crd{\alpha}}
    \right].
  \end{gathered}
\end{equation}

  Здесь $p := p(x^{i},t)$ и имеет смысл плотности распределения
  случайной величины $x^{i}$, $A^{i}$ --- вектор сноса, $B^{i j}$ ---
  вектор диффузии.

  Как видно из \eqref{eq:kFP}, коэффициенты уравнения Фоккера--Планка
  можно получить сразу после записи схемы взаимодействия и
  вероятностей перехода, то есть в практических расчётах записывать
  основное кинетическое уравнение нет необходимости.

  Для получения более привычного вида модели записываем
  соответствующее ему уравнение Ланжевена:
\begin{equation}
  \label{eq:langevin}
  \d x^{i} = a^{i} \d t + b^i_{a} \d W^{a},
\end{equation}
  где $a^{i} := a^{i} (x^k, t)$, $b^{i}_{a} := b^{i}_{a} (x^k, t)$,
  $x^i \in \setR^n $ --- вектор состояния системы, $W^{a} \in
  \mathbb{R}^m$ --- $m$-мерный винеровский процесс. Винеровский
  процесс реализуется как $\d W = \varepsilon \sqrt{\d t}$, где
  $\varepsilon \sim N(0,1)$~--- нормальное распределение со средним
  $0$ и дисперсией $1$. Латинскими индексами из середины алфавита
  обозначаются величины, относящиеся к векторам состояний (размерность
  пространства $n$), а латинскими индексами из начала алфавита
  обозначаются величины, относящиеся к вектору винеровского процесса
  (размерность пространства $m \leqslant n$).

  При этом связь между уравнениями \eqref{eq:FP} и \eqref{eq:langevin}
  выражается следующими соотношениями:
\begin{equation}
  \label{eq:k-langevin}
  A^{i} = a^{i}, \qquad B^{i j} = b^{i}_{a} b^{j a}.
\end{equation}



  Таким образом для описания системы из общих соображений можно
  получить стохастическое дифференциальное уравнение. Это уравнение
  состоит из двух частей, один из которых описывает детерминистическое
  поведение системы, а другой стохастическое. Кроме того, обе части
  уравнения являются согласованными, т. к. получены из одного и того
  же уравнения (схема на рис.~\ref{fig:met}).

\begin{figure}%[h]
  \centering
  \includegraphics[width=\linewidth]{met}
  \caption{Схема метода}
\label{fig:met}
\end{figure}

\section{Протокол Fast Track}

  Fast Track --- одноранговый (P2P) сетевой протокол для
  кооперативного обмена файлами через Интернет. Закачка данных
  осуществляется только из источников, содержащих полные
  файлы. FastTrack первоначально был реализован в программе
  KaZaA. Сеть, основанная на работе прокола FastTrack, имеет
  децентрализованную топологию, что делает ее работу очень надежной. В
  сети пользователи разделены на два класса: суперузлы и простые узлы
  (supernodes и ordinary nodes). Выделение суперузлов является одной
  из функций протокола и на эту роль выбираются узлы с быстрым
  подключением к сети, высокой пропускной способностью и возможностью
  быстрой обработки данных. При этом владельцы компьютеров не знают,
  что их компьютер был назначен в качестве суперузла.

  Для того, чтобы загрузить файл, узел посылает запрос суперузлу,
  который в свою очередь взаимодействует с другими узлами и т.д. Таким
  образом запрос распространяется до определенного протоколом уровня
  сети и называется временем жизни запроса (Time to live). После того,
  как нужный файл будет найден, он передается непосредственно узлу,
  его запросившему, от узла, который имеет этот файл, минуя
  суперузел~\cite{ft1, ft2}.

  \subsection{Моделирование}

  Сделаем предположение, что файл состоит из одной части. Таким
  образом за один шаг взаимодействия нового узла, желающего скачать
  файл, и узла, раздающего файл, новый узел скачивает весь файл и
  становится раздающим узлом.

  Пусть $N$ --- это обозначение нового узла, $L$ --- это раздающий
  узел, а $\beta $ --- коэффициент взаимодействия. Новые узлы могут
  приходить в систему с интенсивностью $\lambda $, а раздающие узлы
  уходить из нее с интенсивностью $\mu$. Тогда схема взаимодействия и
  вектор $\mathbf r$ будет иметь вид:
\begin{equation} 
  \label{ft:1}
  \begin{cases}
    0 \xrightarrow{\lambda } N, & r^{\crd{i}1}=(1,0) \\
    N+L \xrightarrow{\beta } 2L, & r^{\crd{i}2}=(-1,1)\\
    L \xrightarrow{\mu} 0, & r^{\crd{i}3}=(0,-1).
  \end{cases}
\end{equation}

  Первая строка в схеме описывает появление нового клиента в
  системе. Вторая строка отражает взаимодействие нового клиента и
  сида, в результате которого появляется новый сид. А третья – это
  уход сида из системы.
  Запишем вероятности переходов:

\begin{equation} 
  \label{ft:2}
  \begin{gathered}
    s^{+}_1 (n,l) = \lambda \\
    s^{+}_2 (n,l) = \beta nl \\
    s^{+}_3 (n,l) = \mu l.
  \end{gathered}
\end{equation}

  Далее можно записать уравнение Фоккера-Планка для данной модели:
\begin{equation}
  \label{ft:3} 
  \frac{\partial p(n,l)}{\partial t} = {\partial_i}
  (A^i(n,l) p(n,l)) + \frac{1}{2} {\partial_i \partial_j} (B^{ij}(n,l)
  p(n,l)),
\end{equation}
  где вектор сносов и матрица диффузии имеют следующий вид:
\begin{equation}
  \begin{gathered}
    A^i: = A^i(x^k,t)= r^{i\crd{\alpha}}s^+_{\crd{\alpha}} (n,l) ,\\
    B^i:= B^{ij}(x^k,t) = r^{i\crd{\alpha}}r^{i\crd{\alpha}}
    s^+_{\crd{\alpha}} (n,l), \crd{\alpha}=1,2,3.
  \end{gathered}
\end{equation}


  Таким образом получаем:
\begin{equation} 
  \label{ft:4}
  \begin{gathered}
    \mathbf A =
    \begin{pmatrix}
      1\\
      0
    \end{pmatrix}
    \lambda +
    \begin{pmatrix}
      -1\\
      1
    \end{pmatrix}
    \beta n l +
    \begin{pmatrix}
      0\\
      -1
    \end{pmatrix}
    \mu l =
    \begin{pmatrix}
      \lambda - \beta n l\\
      \beta n l - \mu l
    \end{pmatrix}, \\
    \begin{multlined}
      \mathbf B =
      \begin{pmatrix}
        1\\
        0
      \end{pmatrix}
      (1,0) \lambda +
      \begin{pmatrix}
        -1\\
        1
      \end{pmatrix}
      (-1,1) \beta n l +
      \begin{pmatrix}
        0\\
        -1
      \end{pmatrix}
      (0,-1) \mu l = \\ =
      \begin{pmatrix}
        \lambda + \beta n l & - \beta n l \\
        - \beta n l & \beta n l + \mu l
      \end{pmatrix}.
    \end{multlined}
  \end{gathered}
\end{equation}

  Стохастическое дифференциальное уравнение в форме Ланжевена можно
  получить воспользовавшись соответствующей формулой.

  \subsection{Детерминистическое поведение}

  Так как вектор сносов $A$ полностью описывает детерминистическое
  поведение системы можно получить систему обыкновенных
  дифференциальных уравнений, описывающих динамику численности новых
  клиентов и сидов:
\begin{equation}
  \label{ft:5} 
  \left \{
    \begin{aligned}
      \frac{dn}{d t}&=     \lambda - \beta n l\\
      \frac{dl}{d t}&= \beta n l - \mu l
    \end{aligned}
  \right.
\end{equation}


  \subsubsection{Стационарные состояния}

  Найдём стационарные состояния системы~\eqref{ft:5}, которые
  являются решением системы уравнений:

\begin{equation} 
  \label{ft:6} 
  \left \{
    \begin{aligned}
      \lambda - \beta n l &=0\\
      \beta n l - \mu l &=0
    \end{aligned}
  \right.
\end{equation}

  Система~\eqref{ft:5} имеет одностационарное состояние:
\begin{equation}
(\bar{n},\bar{l})= \left ( \frac{\mu }{\beta }, \frac{\lambda }{\mu }
\right )
\end{equation}.

  \subsubsection{Исследование линеаризованной устойчивости}
  \subsubsection{Study of linearized stability}

  Линеаризуем систему~\eqref{ft:5}. Пусть $n=\bar{n} + \xi $,
  $l=\bar{l} + l\eta$, где $\bar{n}$ и $\bar{l}$ --- координаты точки
  равновесия, а $\xi $ и $\eta $ --- малые возмущения:
\begin{equation}
  \label{ft:7} 
  \left\{
    \begin{aligned}
      \frac{d\xi }{d t}&=-\beta \bar{n} \eta- \beta \bar{l} \xi \\
      \frac{d\eta }{d t}&=\beta \bar{n} \eta + \beta \bar{l} \xi - \mu
      \eta
    \end{aligned}
  \right.
\end{equation}

  Запишем линеаризованную систему в окрестности точки равновесия:
\begin{equation}
  \label{ft:8} 
  \left\{
    \begin{aligned}
      \frac{d\xi }{d t}&= - \mu \eta \frac{\beta \lambda }{\mu}\xi  \\
      \frac{d\eta }{d t}&= \frac{\beta \lambda }{\mu}\xi
    \end{aligned}
  \right.
\end{equation}

  Найдём собственные значения характеристического уравнения, которое
  имеет вид:
\begin{equation}
  \label{ft:9} 
  s^2+\frac{\beta \lambda }{\mu} s + \beta
  \lambda =0.
\end{equation}


  Далее запишем корни этого характеристического уравнения:
\begin{equation}
  \label{ft:10} 
  s_{1,2}= \frac{1}{2} \left(
    -\frac{\beta \lambda }{\mu} \pm \sqrt{ \left( \frac{\beta \lambda
        }{\mu} \right)^2 - 4 \beta \lambda} \right).
\end{equation}

  Таким образом, в зависимости от выбора параметров особая точка может
  иметь разный характер. Так при $\beta \lambda < 4\mu^2$ особая точка
  является устойчивым фокусом, а при обратном соотношении ---
  устойчивый узел. В обоих случаях особая точка является устойчивой,
  так как действительная часть корней уравнения отрицательная. Таким
  образом, в зависимости от выбора значений коэффициентов, изменения
  переменных системы может происходить по одной из двух
  траекторий. Если особая точка является фокусом, то в системе
  происходят затухающие колебания численностей новых и раздающих
  узлов~\ref{fig:ft1}. А в узловом случае приближение численностей к
  стационарным значениям происходит в бесколебательном
  режиме~\ref{fig:ft2}. Фазовые портреты системы для каждого из двух
  случаев изображены, соответственно, на графиках ~\ref{fig:ft3}
  и~\ref{fig:ft4}.

\begin{figure}%[h]
  \centering
  \includegraphics[width=\linewidth]{1}
    \caption{Зависимость числа новых и раздающих узлов от времени в
      сети Fast Track для детерминистического случая при 
      $\beta \lambda < 4\mu^2$.}
  \label{fig:ft1}
\end{figure}

\begin{figure}%[h]
  \centering
  \includegraphics[width=\linewidth]{2}
    \caption{Зависимость числа новых и раздающих узлов от времени в
      сети Fast Track для детерминистического случая при $\beta
      \lambda > 4\mu^2$.}
  \label{fig:ft2}
\end{figure}

\begin{figure}%[h]
  \centering
  \includegraphics[width=\linewidth]{3}
    \caption{Фазовые портреты детерминистической системы Fast Track с
      различными отклонениями $(\Delta x, \Delta y)$ от стационарной
      точки при $\beta \lambda < 4\mu^2$.}
  \label{fig:ft3}
\end{figure}

\begin{figure}%[h]
  \centering
  \includegraphics[width=\linewidth]{4}
    \caption{ Фазовые портреты детерминистической системы Fast Track с
      различными отклонениями $(\Delta x, \Delta y)$ от стационарной
      точки при $\beta \lambda > 4\mu^2$.}
  \label{fig:ft4}
\end{figure}

  \subsubsection{Численное моделирование стохастической модели}

  Для иллюстрации полученных результатов было проведено численное
  моделирование стохастического дифференциального уравнения в форме
  Ланжевена. Для численного решения стохастических дифференциальных
  уравнений использован метод, заключающийся в распространении методов
  Рунге-Кутты на случай стохастических дифференциальных
  уравнений~\cite{L_lit04, L_lit01}, реализованный на языке
  Фортран. Результаты численного моделирования приведены на
  графиках~\ref{fig:ft5} и~\ref{fig:all_sft}.

  На рисунках~\ref{fig:ft5} и~\ref{fig:all_sft} наглядно видно, что
  введение малых стохастических членов существенно не влияет на
  поведение системы в близи узловой точки при большом числе раздающих
  узлов. Последствия введения стохастики ощущаются лишь в начале
  эволюции системы. По прошествии сравнительно небольшого отрезка
  времени система входит в стационарный режим и мало отличается от
  детерминированного случая.

\subsubsection{Выводы}

  Полученные результаты показывают, что введение стохастики в
  стационарном режиме слабо влияет на поведение системы, поэтому при
  ее изучении можно рассматривать детерминистическую модель. Кроме
  того, предложенный метод позволяет расширить аппарат инструментов,
  используемых для анализа модели, так как одновременно при применении
  данного подхода для описания системы можно получить обыкновенное
  стохастическое дифференциальное уравнение и уравнение в частных
  производных в форме уравнения Фоккера-Планка. Кроме того, как
  показал рассмотренный пример в некоторых случаях для изучения
  системы можно рассматривать ее детерминистическое приближение,
  которое определяется матрицей сносов.


\begin{figure}%[h]
  \centering
  \includegraphics[width=\linewidth]{sft_graph}
  \caption{Зависимость числа новых и раздающих узлов от времени в
    сети FastTrack для стохастического случая.}
\label{fig:sft_graph}
\end{figure}


\begin{figure}%[h]
  \centering
  \includegraphics[width=\linewidth]{5}
  \caption{Фазовые портреты стохастической системы Fast Track с
    различными отклонениями $(\Delta x, \Delta y)$ от стационарной
    точки при $\beta \lambda > 4\mu^2$.}
\label{fig:ft5}
\end{figure}



\begin{figure}%[]
  \centering
  \includegraphics[width=\linewidth]{all_sft}
  \caption{Фазовые портреты стохастической системы Fast Track с
    различными отклонениями $(\Delta x, \Delta y)$ от стационарной
    точки при $\beta \lambda > 4\mu^2$.}
\label{fig:all_sft}
\end{figure}

\section{Протокол BitTorrent}

BitTorrent --- пиринговый (P2P) сетевой протокол для кооперативного
обмена файлами через Интернет. Файлы передаются частями, каждый
torrent-клиент, получая (скачивая) эти части, в тоже время отдает
(закачивает) их другим клиентам, что снижает нагрузку и зависимость от
каждого клиента-источника и обеспечивает избыточность данных.

\subsection{Моделирование}

Сначала рассмотрим упрощенную модель закрытой системы, т.е. такую, в
которой не приходят новые клиенты и не уходят раздающие. Кроме того,
сделаем предположение, что файл состоит из одной части. Таким образом
за один шаг взаимодействия нового клиента (личера), желающего скачать
файл, и клиента, раздающего файл (сида), новый клиент скачивает весь
файл и становиться сидом.


Пусть $N$ --- это обозначение нового клиента (личера), $С$ --- это
раздающий клиент (сид), а $\beta$ --- коэффициент
взаимодействия. Тогда схема взаимодействия будет иметь вид:


\begin{equation} 
\label{bt:1}
N+C \xrightarrow{\beta } 2C, \qquad r^{\crd{i}2}=(-1,1).
\end{equation}

Схема отражает, что после взаимодействия личера и сида, в системе
пропадает личер и появляется еще один сид.

Далее, пусть $n$ --- это численность новых клиентов, а $c$ ---
количество сидов в системе.

Запишем вероятности переходов:
\begin{equation} 
\label{bt:2}
s^{+} (n,c) = \beta nc.
\end{equation}

Далее можно записать уравнение Фоккера--Планка для данной модели:
\begin{equation} 
\label{bt:3}
\frac{\partial p(n,c)}{\partial t} = 
{\partial_i} (A^i(n,c) p(n,c))
+\frac{1}{2} {\partial_i \partial_j} (B^{ij}(n,c) p(n,c)),
\end{equation}
где вектор сносов и матрица диффузии имеют следующий вид:
\begin{equation}
  \begin{gathered}
     A^i(n,c)= r^{i\crd{\alpha}}s^+_{\crd{\alpha}} (n,l) ,\\
     B^i(n,c) = r^{i\crd{\alpha}}r^{i\crd{\alpha}} s^+_{\crd{\alpha}} (n,l).
  \end{gathered}
\end{equation}

Таким образом получаем:
\begin{equation} 
\label{bt:4}
  \begin{gathered}
    \mathbf A =
    \begin{pmatrix}
      -1\\
      1
    \end{pmatrix}
    \beta n c +
     =
    \begin{pmatrix}
      - \beta n l\\
      \beta n l 
    \end{pmatrix}, \\
    \begin{multlined}
      \mathbf B =
       \begin{pmatrix}
        -1\\
        1
      \end{pmatrix}
      (-1,1) \beta n c  = 
      \begin{pmatrix}
         \beta n c & - \beta n c \\
        - \beta n c& \beta n c
      \end{pmatrix}.
    \end{multlined}
  \end{gathered}
\end{equation}


Стохастическое дифференциальное уравнение в форме Ланжевена можно
получить воспользовавшись соответствующей формулой.

Также можно записать систему дифференциальных уравнений, описывающую
детерминистическое поведение системы:
\begin{equation} 
\label{bt:5}
\left \{
\begin{aligned}
 \frac{dn}{d t}&=      - \beta n c\\
 \frac{dc}{d t}&=     \beta n c 
\end{aligned}
\right.
\end{equation}

Далее рассмотрим открытую систему, в которой новые клиенты могут
приходить в систему с интенсивностью $\lambda$, а сиды уходить из нее
с интенсивностью $\mu$. Схема взаимодействия имеет вид:

\begin{equation} 
\label{bt:6}
\begin{aligned}
0 \xrightarrow{\lambda } N, & r^{\crd{i}1}=(1,0),\\
N+C \xrightarrow{\beta } 2C, & r^{\crd{i}2}=(-1,1),\\
C  \xrightarrow{\mu } 0, & r^{\crd{i}3}=(0,-1).
\end{aligned}
\end{equation}


Первая строка в схеме описывает появление нового клиента в системе,
вторая строка --- взаимодействие нового клиента и сида, в результате
которого появляется новый сид. А третья --- это уход сида из системы.

Далее, пусть $n$ --- это численность новых клиентов, а $c$ ---
количество сидов в системе.

Эта система с точностью до обозначений совпадает с моделью Fasttrack.

Теперь рассмотрим систему, в которой передаются файлы, состоящие из
$m$ частей. В системе присутствуют следующие участники:

\begin{itemize}
\item Новые клиенты ($N$) --- это клиенты, у которых нет ни одной
  части файла.
\item Личеры ($L$) --- это клиенты, которые уже скачали какое-то
  количество частей файла и могут их раздавать новым клиентам или
  другим личерам.
\item Сиды ($C$) --- это клиенты, у которых есть весь файл, т.е. они
  только раздают.
\end{itemize}

Кроме того $n$ --- это численность новых клиентов, а $c$ ---
количество сидов в системе, $l_i$ --- количество личеров, у которых
есть ровно $i$ частей файла, где $i=\overline{i, n-1}$. Также пусть $
\bar{L}_i$ --- это личеры , у которых есть какие-либо части файла
интересующие личера $L_i$ и соответственно $ \bar{l}_i$i их
количество.

Для данной системы в схеме взаимодействия будут иметь место следующие
типы соотношений:
\begin{equation} 
\label{bt:7}
\begin{aligned}
0  \xrightarrow{\lambda } & N, \\
N+C  \xrightarrow{\beta } & L_1+C, \\
N+L_i  \xrightarrow{\beta_i } & L_1+L_i, \\
L_i + \bar{L}_i  \xrightarrow{\delta_i } & L_{i+1}+\bar{L}_i, \\
L_i + C  \xrightarrow{\gamma_i } & L_{i+1}+C, \\
L_{m-1} + \bar{L}_{m-1}  \xrightarrow{\gamma_{m-1} } & C+\bar{L}_{m-1}, \\
L_{m-1} + C  \xrightarrow{\gamma } & 2C, \\
C  \xrightarrow{\mu } & 0. 
\end{aligned}
\end{equation}

Один шаг взаимодействия --- это передача одной части файла от одного
клиента другому. Первое соотношение описывает появление нового клиента
в системе с интенсивностью $\lambda$. Второе и третье соотношения
описывают взаимодействие нового клиента с сидом или личером с
коэффициентами $\beta$ и $\beta_i$, $(i=\overline{i, m-1})$, в
результате которого новый клиент становиться личером из класса $L_1$
. Четвертое и пятое соотношения --- это взаимодействие личера $L_i$ с
сидом или другим личером с коэффициентами $\delta_i$ и $\gamma_i$
$(i=\overline{i, m-2})$, что приводит к получению личером одной части
файла и переходу его в класс $L_{i+1}$.  Шестое и седьмое описывает
процесс перехода личера в класс сидов с коэффициентами $\gamma_{m-1}$
и $\gamma$, т.е. личер скачивает последнюю часть файл. Последнее
соотношение – это уход сида из системы с интенсивность $\mu$.

Запишем векторы $r^{i\crd{\alpha}}=(n,l_1,l_2,...,l_{m-1},c)$ и 
вероятности перехода $s^+_{\crd{\alpha}}$:

\begin{equation} 
\label{bt:8}
\begin{gathered}
r^{1}  =(1,0,0,...,0), \\
r^{2}  =r_i^3=(-1,1,0,...,0), i=\overline{i, m-1} \\
r_i^4  =r_i^5=(0,...,-1,1,...,0), i=\overline{i, m-2} \\
r^{6}  =r^7=(0,0,...,-1,1), \\
r^{8}  =(0,0,...,-1).
\end{gathered}
\end{equation}
\begin{equation} 
\label{bt:9}
\begin{gathered}
s^{+}_1  =\lambda, \\
s^{+}_2  =\beta n c, \\
s^{+}_{3i}  =\beta_i n l_i, \\
s^{+}_{4i}  =\delta_i l_i \bar{l}_i, i=\overline{i, m-1}\\
s^{+}_{5i}  =\gamma_i l_i c, i=\overline{i, m-2}\\
s^{+}_{6}  =\gamma_{m-1} l_{m-1} \bar{l}_{m-1}, \\
s^{+}_{7}  =\gamma l_{m-1} c, \\
s^{+}_{8}  =\mu c. \\
\end{gathered}
\end{equation}

Для данной модели, аналогично предыдущей, можно записать уравнение
Фоккера-Планка. Но так как детерминистическое поведение полностью
описывается матрицей $A$, запишем только ее.

Таким образом получаем:
\begin{equation}
\label{bt:10}
    \mathbf A =
    \begin{pmatrix}
    \lambda - \beta n c - \sum_{i=1}^{m-1} \beta_i n l_i \\
    \beta n c + \sum_{i=1}^{m-1} \beta_i n l_i -\delta_1 l_1 \bar{l}_1 - \gamma_1 l_1 c \\
      \delta_1 l_1 \bar{l}_1 + \gamma_1 l_1 c -  \delta_2 l_2 \bar{l}_2 - \gamma_2 l_2 c  \\
     \ldots \\
     \begin{multlined}
       \delta_{m-2} l_{m-2} \bar{l}_{m-2} + \gamma_{m-2} l_{m-2} c -
       {} \\ {} -
       \delta_{m-1} l_{m-1} \bar{l}_{m-1} - \gamma_{m-1} l_{m-1} c 
     \end{multlined}
     \\ 
      \delta_{m-1} l_{m-1} \bar{l}_{m-1} + \gamma_{m-1} l_{m-1} c  - \mu c 
    \end{pmatrix}.
\end{equation}


Как следствие можно получить систему дифференциальных уравнений
описывающих динамику численности новых клиентов, личеров и сидов :



\begin{equation} \label{bt:11}
\left \{
\begin{gathered}
  \frac{d n}{d t} =      \lambda - \beta n c - \sum_{i=1}^{m-1} \beta_i n l_i, \\
 \frac{d l_1}{d t}=     \beta n c + \sum_{i=1}^{m-1} \beta_i n l_i -\delta_1 l_1 \bar{l}_1 - \gamma_1 l_1 c, \\ 
 \frac{d l_2}{d t}=  \delta_1 l_1 \bar{l}_1 + \gamma_1 l_1 c -  \delta_2 l_2 \bar{l}_2 - \gamma_2 l_2 c,  \\
    \ldots \\
\begin{multlined}
 \frac{d l_{m-1}}{d t}=  \delta_{m-2} l_{m-2} \bar{l}_{m-2} + 
 \gamma_{m-2} l_{m-2} c - {} \\ {} - 
 \delta_{m-1} l_{m-1} \bar{l}_{m-1} -
 \gamma_{m-1} l_{m-1} c,
\end{multlined}
\\ 
\frac{d c}{d t}= \delta_{m-1} l_{m-1} \bar{l}_{m-1} + \gamma_{m-1} l_{m-1} c  - \mu c. 
\end{gathered}
\right.
\end{equation}

Сделаем предположение, что
$\delta=\delta_{1}=\delta_{2}=...=\delta_{m-1}=const$. Сложим в
системе уравнения со второго по $m+1$ и при обозначении всех личеров и
сидов через $l = l_1 + l_2 + ... + l_{m-1} + c$ получим упрощённую
систему следующего вида:


\begin{equation} 
\label{bt:12}
\left \{
\begin{aligned}
 \frac{d n}{d t}&=      \lambda - \beta n (l+c), \\
 \frac{d (l+c)}{d t}&=     \beta n (l+c)  - \mu c. 
\end{aligned}
\right.
\end{equation}

\section{Заключение}

\begin{enumerate}
\item В работе описан метод получения стохастических моделей для
  систем, которые возможно описывать одношаговыми
  процессами. Предложенный метод позволяет получить универсальные
  правила записи стохастических дифференциальных уравнений для систем,
  процессы в которых представимы как одношаговые процессы. А также
  расширить аппарат инструментов, используемых для анализа модели, так
  как одновременно при применении данного подхода для описания системы
  можно получить обыкновенное стохастическое дифференциальное
  уравнение и уравнение в частных производных в форме уравнения
  Фоккера--Планка.
\item Изучено влияния введения стохастики в детерминистические модели,
  на примере модели протокола FastTrack и Bittorrent. Полученные
  результаты показывают, что введение стохастики в стационарном режиме
  слабо влияет на поведение системы, поэтому при ее изучении можно
  рассматривать детерминистическую модель. Кроме того, как показал
  рассмотренный пример в некоторых случаях для изучения системы можно
  рассматривать ее детерминистическое приближение, которое
  определяется матрицей сносов.
\end{enumerate}


\bibliographystyle{gost2008l}

\bibliography{bib/p2p_sdu/p2p_sdu}




\end{document}
