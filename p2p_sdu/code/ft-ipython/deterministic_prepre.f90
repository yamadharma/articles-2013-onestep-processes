!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!	Программа численно решает системы ОДУ, заданные в модуле
! deterministic_prepre_models
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
program deterministic_prepre
	use ERKs
	use deterministic_prepre_models
	implicit none
!-------------------------------------------------------------------------------
	procedure(f), pointer				:: f_ode => null()
	integer                     :: EQN
	double precision, parameter :: h = 0.01
	double precision, parameter :: eps = 1.0d-8
	double precision, parameter :: t_start = 0.0d0
	double precision, parameter :: t_stop  = 20.0d0
	double precision            :: t
	! Сеточные функции
	double precision, allocatable, dimension(:) :: x
	double precision, allocatable, dimension(:) :: x_o
	! Массив для параметров функций различных моделей
	double precision, allocatable, dimension(:) :: p
	integer :: i
	
	character(len=32), dimension(1:7) :: arg
	double precision, dimension(1:3)  :: init_value
	double precision, dimension(1:3)  :: parameter_value
!-------------------------------------------------------------------------------	
	
	! Считываем все аргументы командной строки
	do i = 1,command_argument_count(),1
		call get_command_argument(i, arg(i))
	end do
	
	select case( trim(arg(1)) )
	! {---
		case('PredatorPrey')
			f_ode => PredatorPrey
			EQN = 2
			allocate(x(1:EQN), x_o(1:EQN), p(1:2))
			! Здесь безразмерные коэффициенты и переменные
			! Начальные значения и параметр r
			read(arg(2),*) init_value(1)
			read(arg(3),*) init_value(2)
			read(arg(4),*) parameter_value(1)
			! так как массив не может состоять из одного элемента, то
			! мы передаем два параметра, но второй не используется
			p = [ parameter_value(1), 0.0d0 ] ! r = k_3/ak_1
			x_o(1) = init_value(1)
			x_o(2) = init_value(2)
!-------------------------------------------------------------------------------
		case('PredatorPreyPrey') ! Хищник-жертва с конкуренцией среди жертв
			f_ode => PredatorPreyPrey
			EQN = 2
			allocate(x(1:EQN), x_o(1:EQN), p(1:2))
			! Здесь безразмерные коэффициенты и переменные
			! Начальные значения и параметры r1, r2
			read(arg(2),*) init_value(1)
			read(arg(3),*) init_value(2)
			read(arg(4),*) parameter_value(1)
			read(arg(5),*) parameter_value(2)
			p = [ parameter_value(1), parameter_value(2) ] ! r1 r2
			x_o(1) = init_value(1)
			x_o(2) = init_value(2)
!-------------------------------------------------------------------------------			
		case('Concurs')
			f_ode => Concurs
			EQN = 2
			allocate(x(1:EQN), x_o(1:EQN), p(1:3))
			! Здесь безразмерные коэффициенты и переменные
			! Начальные значения и параметры a12 и a21
			do i = 1,2,1
				read(arg(i+1),*) init_value(i)
				read(arg(i+3),*) parameter_value(i)
			end do
			
			p = [ 1.0d0, parameter_value(1), parameter_value(2) ] ! r, a12,a21
			x_o(1) = init_value(1) ! (1.0d0 - p(2))/(1.0d0 - p(2)*p(3))
			x_o(2) = init_value(2)
!-------------------------------------------------------------------------------
		case('Symbiosis')
			f_ode => Symbiosis
			EQN = 2
			allocate(x(1:EQN), x_o(1:EQN), p(1:4))
			
			p(1:4) = [ 1.0d0, 0.5d0, 2.0d0, 1.0d0 ]  ! b1, b2, k1, k2
			x_o(1) = p(1)/p(3) + 4.1d0
			x_o(2) = p(2)/p(4) + 2.1d0
!-------------------------------------------------------------------------------
		case('PredatorPrey3D')
			f_ode => PredatorPrey3D
			EQN = 3
			allocate(x(1:EQN), x_o(1:EQN), p(1:5))
			
			p(1:5) = [ 5.0d0, 6.0d0, 4.0d0, 2.0d0, 3.0d0 ] ! a1,a2,c,k1,k2
			! Единственная стационарная точка это (0,0,0)
			x_o(1) = 10.2d0
			x_o(2) = 3.0d0
			x_o(3) = 1.0d0
!-------------------------------------------------------------------------------
	!---}
	end select
!----------------  Вычисления --------------------
	x = x_o
	t = t_start
	! call RK_print(f_ode, EQN, x, t, p, [t_start, t_stop], h, 'RKp5n2')
	call ERK(f_ode, EQN, x, t, p, [t_start, t_stop], eps, eps, .true., 'DVERK_ERK6(5)')
	deallocate(x_o, x, p)
!----------------             --------------------	
end program deterministic_prepre