module matrix
contains
!-----------------------------------------------------------------------
!			! Вычисление определителя матрицы с помощью LUP-разложения
!-----------------------------------------------------------------------
	function det(Mat)
		implicit none
		! Размерность матрицы
		integer :: N
		! Вычисляемое значение определителя
		double precision :: det
		! матрица, детерминант которой следует вычислить
		double precision, intent(in), dimension(1:,1:) :: Mat
		double precision, allocatable :: A(:,:)
		! info --- индикатор успешности выполнения процедуры LAPACK
		integer :: i, j, info
		! Вектор, задающий матрицу перестановок
		integer, allocatable :: ipiv(:)
		! знак (понадобится для вычисления определителя)
		real :: sgn
		!------------------------------------
		if(size(Mat,1) .ne. size(Mat,2)) then
			print *, "Матрица не квадратная"
			return
		else
			N = size(Mat,1)
		end if
		!------------------------------------
		allocate(A(1:N,1:N))
		allocate(ipiv(1:N))
		ipiv = 0.0d0
		det = 1.0d0
		sgn = 1.0d0
		A = Mat
		!------------------------------------
		call dgetrf(N, N, A, N, ipiv, info)
		!------------------------------------
		! Считаем определитель
		do i = 1,N
			det = det*A(i, i)
		end do
		! Считаем число переставленных строк
		do i = 1,N
			if(ipiv(i) .ne. i) then
				sgn = -sgn
			end if
		end do
		! Получаем окончательный результат
		det = sgn*det
	end function det
!-----------------------------------------------------------------------
!		! Нахождение обратной матрицы с помощью LUP разложения
!-----------------------------------------------------------------------
	subroutine Invert(A)
		implicit none
		! Передаваемая процедуре матрица затирается
		double precision, intent(inout), dimension(1:,1:) :: A
		! Вектор, задающий матрицу перестановок
		integer, allocatable, dimension(:) :: ipiv
		! Служебный массив
		double precision, allocatable, dimension(:) :: work
		! Размерности матрицы и служебные переменные
		integer :: m, n, lda, lwork, info
		! Если размерности матрицы не совпадают, то
		!	происходит выход из процедуры
		m = size(A,1)
		n = size(A,2)
		if (m /= n) then
			return
		end if
		allocate(ipiv(1:m))
		allocate(work(1:m))
		lda = max(1,m)
		lwork = m
		! Вычисляем LUP разложение
		call dgetrf(m, n, A, lda, ipiv, info)
		! Получившуюся матрицу передаем следующей процедуре
		call dgetri(n, A, lda, ipiv, work, lwork, info )
	end subroutine Invert
!-----------------------------------------------------------------------
!		! Нахождение обратной матрицы с помощью SVD разложения
!		! по сравнению с методом через LUP разложение очень медленно
!-----------------------------------------------------------------------
	subroutine InvertSVD(A)
		implicit none
		double precision, intent(inout), dimension(1:,1:) :: A
		character(len=1) :: jobu, jobvt
		! Для LAPACK процедуры
		double precision, allocatable :: U(:,:), Vt(:,:), s(:), work(:)
		double precision, allocatable :: D(:,:) ! Диагональная матрица (или D^{-1})
		double precision, allocatable :: invA(:,:) ! Обратная к A
		double precision, allocatable :: B(:,:) ! A = B
		integer :: m, n, lda, ldu, ldvt, lwork, info, i, j
!---------------------------------------------------------------------------
		m = size(A,1)
		n = size(A,2)
		lda = max(1,m)
		ldu = m
		ldvt = m
		jobu = 'A'
		jobvt = 'A'
		lwork = 20*max(m,n) ! Служебная переменная, чем больше, тем лучше
		
		allocate(B(1:m,1:n))
		allocate(U(1:m,1:m))
		allocate(Vt(1:n,1:n))
		allocate(invA(1:m,1:n))
		allocate(D(1:m,1:n))
		allocate(s(1:min(n,m))) ! Диагональные элементы сингулярной матрицы
		allocate(work(1:lwork)) ! Служебный массив
		!!-----------------------
		call dgesvd(jobu, jobvt, m, n, A, lda, s, U, ldu, Vt, ldvt, work, lwork, info)
		!!----------------------
		D = 0.0d0
		forall(i=1:m, j = 1:n, i==j) D(i,j) = 1.0/s(i)
		A = matmul(transpose(Vt),transpose(D))
		A = matmul(A,transpose(U))
	end subroutine InvertSVD
	!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	!					Вычисление квадратного корня из матрицы
	!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	function matrix_sqrt(A, m, n)
		implicit none
		! Размерности получаемой матрицы
		integer, intent(in) :: m
		integer, intent(in) :: n
		double precision, dimension(1:m,1:n), intent(in) :: A
		! Возвращаемое значение функции
		double precision, dimension(1:m,1:n) :: matrix_sqrt
		! Параметры метода SVD
		integer :: lda
		integer :: ldu
		integer :: ldvt
		integer :: lwork ! Служебная переменная, чем больше, тем лучше
		integer :: info
		integer :: i, j
		!!
		character(len=1), parameter :: jobu = 'A'
		character(len=1), parameter :: jobvt = 'A'
		! Массивы для LAPACK процедуры
		double precision, dimension(1:m,1:n) :: U, Vt
		! Диагональные элементы сингулярной матрицы
		double precision, allocatable, dimension(:) :: S
		double precision, allocatable, dimension(:) :: work
		! Временный массив для того, чтобы массив A не затирался
		double precision, dimension(1:m,1:n) :: A_tmp
		! Квадратный корень из диагональной матрицы
		double precision, dimension(1:m,1:n) :: sqrt_D
		!!
		lda = max(1,m)
		ldu = m
		ldvt = m
		lwork = 20*max(m,n)
		allocate(work(1:lwork), S(1:min(m,n)))
		
		A_tmp = A
		!!-----------------------
		call dgeSVD(jobu, jobvt, m, n, A_tmp, lda, S, U, ldu, Vt, ldvt, work, lwork, info)
		!!----------------------
		sqrt_D = 0.0d0
		forall(i=1:m, j = 1:n, i==j) sqrt_D(i,j) = dsqrt(s(i))
		matrix_sqrt = matmul( matmul(U,sqrt_D), Vt)
		deallocate(work, S)
		return
	end function matrix_sqrt
end module matrix
