module FastTrack_models
implicit none
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! Модуль содержит функции, задающие правые часть систем ОДУ, описывающих
! стохастическую и детерминированную модели FastTrack
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	abstract interface
		function A(t, x, p)
			implicit none
			double precision, intent(in) :: t
			double precision, dimension(:), intent(in) :: x
			double precision, dimension(:), intent(in) :: p ! Параметры
			double precision, dimension(1:ubound(x,1)) :: A
		end function A
		function B(t, x, p)
			implicit none
			double precision, intent(in) :: t
			double precision, dimension(:), intent(in) :: x
			double precision, dimension(:), intent(in) :: p ! Параметры
			double precision, dimension(lbound(x,1):ubound(x,1),lbound(x,1):ubound(x,1)) ::  B
		end function B
	end interface
	
	contains
!------------------------------------------------------------------
!	проверка результатов вычисления на осмысленность
!------------------------------------------------------------------
	pure function check(x)
		implicit none
		double precision, dimension(:), intent(in) :: x
		logical :: check
		if( minval(x) > 0.0d0 ) then
			check = .True.
		else
			check = .False.
		end if
	end function check
!-------------------------------------------------------------------------------
!            Детерминированная модель FastTrack (в безразмерном виде)
!-------------------------------------------------------------------------------
	function ode_FastTrack(t,x,p)
		implicit none
		double precision, intent(in) :: t
		double precision, dimension(:), intent(in) :: x
		double precision, dimension(:), intent(in) :: p ! Параметры
		double precision, dimension(1:ubound(x,1)) :: ode_FastTrack
		double precision :: r
		
		r = p(1)
		
		ode_FastTrack(1) = 1.0d0 - x(1)*x(2)
		ode_FastTrack(2) = r*x(2)*(x(1)-1.0d0)
	end function ode_FastTrack
!-------------------------------------------------------------------------------
!			Детерминированная часть для стохастического случая модели FastTrack
!-------------------------------------------------------------------------------
	function Afasttrack(t,x,p)
		implicit none
		double precision, intent(in) :: t
		double precision, dimension(:), intent(in) :: x
		double precision, dimension(:), intent(in) :: p ! Параметры
		double precision, dimension(1:ubound(x,1)) :: Afasttrack
		double precision :: lambda, beta, mu
		
		lambda = p(1)
		beta = p(2)
		mu = p(3)
		
		Afasttrack(1) = lambda - beta*x(1)*x(2)
		Afasttrack(2) = -x(2)*(mu - beta*x(1))
	end function Afasttrack
!-------------------------------------------------------------------------------
!			Стохастическая часть для стохастического случая модели FastTrack
!-------------------------------------------------------------------------------
	function Bfasttrack(t,x,p)
		use matrix
		implicit none
		double precision, intent(in) :: t
		double precision, dimension(:), intent(in) :: x
		double precision, dimension(:), intent(in) :: p ! Параметры
		double precision, dimension(lbound(x,1):ubound(x,1),lbound(x,1):ubound(x,1)) ::  Bfasttrack
		double precision :: lambda, beta, mu
		double precision, dimension(1:2,1:2) :: B

		lambda = p(1)
		beta = p(2)
		mu = p(3)
		
		B(1,1:2) = [ lambda + beta*x(1)*x(2), -beta*x(1)*x(2) ]
		B(2,1:2) = [ -beta*x(1)*x(2), beta*x(1)*x(2) + mu*x(2) ]
		
		Bfasttrack = matrix_sqrt(B,2,2)
	end function Bfasttrack
end module FastTrack_models
