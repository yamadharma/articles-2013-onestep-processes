!-------------------------------------------------------------------------------
!                   Классические методы Рунге-Кутты
!-------------------------------------------------------------------------------
module RKs
implicit none
private
!-------------------------------------------------------------------------------
!       Набор глобальных переменных, задающих метод Рунге--Кутты
!-------------------------------------------------------------------------------
integer                                       :: method_stage
integer                                       :: method_order
integer                                       :: equations_number
double precision, allocatable, dimension(:,:) :: a
double precision, allocatable, dimension(:)   :: b
double precision, allocatable, dimension(:)   :: c
double precision, allocatable, dimension(:,:) :: k
! Флаг включения отладочных сообщений
logical, parameter                            :: debug = .false.
!-------------------------------------------------------------------------------
!                  Доступные вовне подпрограммы и функции
!-------------------------------------------------------------------------------
public :: RK, RKsteps, RK_print
public :: euclid_norm
public :: print_matrix
!-------------------------------------------------------------------------------
!  Абстрактный интерфейс для функции, представляющей правую часть системы ОДУ
!-------------------------------------------------------------------------------
abstract interface 
	function ode_func(t,x,p)
		double precision, intent(in)               :: t ! Время
		double precision, dimension(:), intent(in) :: x ! Переменные
		double precision, dimension(:), intent(in) :: p ! Параметры
		double precision, dimension(1:ubound(x,1)) :: ode_func
	end function ode_func
end interface	
contains
!-------------------------------------------------------------------------------
!	                      Инициализация всех коэффициентов
!-------------------------------------------------------------------------------
	subroutine CoefficientsInit(method_code_name)
		character(len = *), intent(in) :: method_code_name
		select case (trim(method_code_name))
	!-----------------------------------------------------------------------------
			case('RKp2n1')
				method_stage = 2
				method_order = 2
				allocate(a(1:method_stage,1:method_stage), b(1:method_stage))
				allocate(c(1:method_stage), k(1:equations_number,1:method_stage))
				a(1,1:2) = [ 0.0d0, 0.0d0 ]
				a(2,1:2) = [ 1.0d0, 0.0d0 ]
				b(1:2) = [ 0.5d0, 0.5d0 ]
				c(1:2) = [ 0.0d0, 1.0d0 ]
	!-----------------------------------------------------------------------------
			case('RKp2n2')
				method_stage = 2
				method_order = 2
				allocate(a(1:method_stage,1:method_stage), b(1:method_stage))
				allocate(c(1:method_stage), k(1:equations_number,1:method_stage))
				a(1,1:2) = [ 0.0d0, 0.0d0 ]
				a(2,1:2) = [ 0.5d0, 0.0d0 ]
				b(1:2) = [ 0.0d0, 1.0d0 ]
				c(1:2) = [ 0.0d0, 0.5d0 ]
	!-----------------------------------------------------------------------------
			case('RKp3n1')
			! 'Метод Рунге--Кутты 3-го порядка I'
				method_stage = 3
				method_order = 3
				allocate(a(1:method_stage,1:method_stage), b(1:method_stage))
				allocate(c(1:method_stage), k(1:equations_number,1:method_stage))
				a(1,1:3) = [ 0.0d0, 0.0d0, 0.0d0 ]
				a(2,1:3) = [ 2.0d0/3.0d0, 0.0d0, 0.0d0 ]
				a(3,1:3) = [ 1.0d0/3.0d0, 1.0d0/3.0d0, 0.0d0 ]
				b(1:3) = [ 0.25d0, 0.0d0, 0.75d0 ]
				c(1:3) = [ 0.0d0, 2.0d0/3.0d0, 2.0d0/3.0d0 ]
	!-----------------------------------------------------------------------------
			case('RKp3n2')
			! 'Метод Рунге--Кутты 3-го порядка II'
				method_stage = 3
				method_order = 3
				allocate(a(1:method_stage,1:method_stage), b(1:method_stage))
				allocate(c(1:method_stage), k(1:equations_number,1:method_stage))
				a(1,1:3) = [  0.0d0, 0.0d0, 0.0d0 ]
				a(2,1:3) = [  0.5d0, 0.0d0, 0.0d0 ]
				a(3,1:3) = [ -1.0d0, 2.0d0, 0.0d0 ]
				b(1:3) = [ 1.0d0/6.0d0, 4.0d0/6.0d0, 1.0d0/6.0d0 ]
				c(1:3) = [ 0.0d0, 1.0d0/3.0d0, 2.0d0/3.0d0 ]
	!-----------------------------------------------------------------------------
			case('RKp3n3')
			! 'Метод Рунге--Кутты 3-го порядка III (Хойна)'
				method_stage = 3
				method_order = 3
				allocate(a(1:method_stage,1:method_stage), b(1:method_stage))
				allocate(c(1:method_stage), k(1:equations_number,1:method_stage))
				a(1,1:3) = [ 0.0d0, 0.0d0, 0.0d0 ]
				a(2,1:3) = [ 1.0d0/3.0d0, 0.0d0, 0.0d0 ]
				a(3,1:3) = [ 0.0d0, 2.0d0/3.0d0, 0.0d0 ]
				b(1:3) = [ 0.25d0, 0.0d0, 0.75d0 ]
				c(1:3) = [ 0.0d0, 1.0d0/3.0d0, 2.0/3.0d0 ]
	!-----------------------------------------------------------------------------
			case('RKp4n1')
			! 'Метод Рунге--Кутты 4-ого порядка I'
				method_stage = 4
				method_order = 4
				allocate(a(1:method_stage,1:method_stage), b(1:method_stage))
				allocate(c(1:method_stage), k(1:equations_number,1:method_stage))
				a(1,1:4) = [ 0.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(2,1:4) = [ 1.0d0/2.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(3,1:4) = [ 0.0d0, 1.0d0/2.0d0, 0.0d0, 0.0d0 ]
				a(4,1:4) = [ 0.0d0, 0.0d0, 1.0d0, 0.0d0 ]
				b(1:4) = [ 1.0d0/6.0d0, 1.0d0/3.0d0, 1.0d0/3.0d0, 1.0d0/6.0d0 ]
				c(1:4) = [ 0.0d0, 1.0d0/2.0d0, 1.0d0/2.0d0, 1.0d0 ]
	!-----------------------------------------------------------------------------
			case('RKp4n2')
			! 'Метод Рунге--Кутты 4-ого порядка II'
				method_stage = 4
				method_order = 4
				allocate(a(1:method_stage,1:method_stage), b(1:method_stage))
				allocate(c(1:method_stage), k(1:equations_number,1:method_stage))
				a(1,1:4) = [ 0.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(2,1:4) = [ 1.0/4.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(3,1:4) = [ 0.0d0, 1.0/2.0d0, 0.0d0, 0.0d0 ]
				a(4,1:4) = [ 1.0d0, -2.0d0, 2.0d0, 0.0d0 ]
				b(1:4) = [ 1.0d0/6.0d0, 0.0d0, 2.0d0/3.0d0, 1.0d0/6.0d0 ]
				c(1:4) = [ 0.0d0, 0.25d0, 0.5d0, 1.0d0 ]
	!-----------------------------------------------------------------------------
			case('RKp4n3')
			! 'Метод Рунге--Кутты 4-ого порядка III (правило 3/8)'
				method_stage = 4
				method_order = 4
				allocate(a(1:method_stage,1:method_stage), b(1:method_stage))
				allocate(c(1:method_stage), k(1:equations_number,1:method_stage))
				a(1,1:4) = [  0.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(2,1:4) = [  1.0d0/3.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(3,1:4) = [ -1.0d0/3.0d0, 1.0d0, 0.0d0, 0.0d0 ]
				a(4,1:4) = [  1.0d0, -1.0d0, 1.0d0, 0.0d0 ]
				b(1:4) = [ 1.0d0/8.0d0, 3.0/8.0d0, 3.0d0/8.0d0, 1.0d0/8.0d0 ]
				c(1:4) = [ 0.0d0, 1.0d0/3.0d0, 2.0d0/3.0d0, 1.0d0 ]
	!-----------------------------------------------------------------------------
			case('RKp5n1')
			! Метод Рунге-Кутты 5-го порядка (Кутта(1901)-Нюстрем(1925))
				method_stage = 6
				method_order = 5
				allocate(a(1:method_stage,1:method_stage), b(1:method_stage))
				allocate(c(1:method_stage), k(1:equations_number,1:method_stage))
				a(1,1:6) = [ 0.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(2,1:6) = [ 1.0d0/3.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(3,1:6) = [ 4.0d0/25.0d0, 6.0d0/25.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(4,1:6) = [ 1.0d0/4.0d0, -3.0d0, 15.0d0/4.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(5,1:6) = [ 6.0d0/81.0d0, 90.0d0/81.0d0, -50.0d0/81.0d0, 8.0d0/81.0d0, 0.0d0, 0.0d0 ]
				a(6,1:6) = [ 6.0d0/75.0d0, 36.0d0/75.0d0, 10.0d0/75.0d0, 8.0d0/75.0d0, 0.0d0, 0.0d0 ]
				b(1:6) = [ 23.0d0/192.0d0, 0.0d0, 125.0d0/192.0d0, 0.0d0, -81.0d0/192.0d0, 125.0d0/192.0d0 ]
				c(1:6) = [ 0.0d0, 1.0d0/3.0d0, 2.0d0/5.0d0, 1.0d0, 2.0d0/3.0d0, 4.0d0/5.0d0 ]
	!-----------------------------------------------------------------------------
			case('RKp5n2')
				! Метод Рунге-Кутты 5-го порядка Dormand/Prince (1980)
				method_stage = 7
				method_order = 5
				allocate(a(1:method_stage,1:method_stage), b(1:method_stage))
				allocate(c(1:method_stage), k(1:equations_number,1:method_stage))
				a(1,1:7) = [ 0.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(2,1:7) = [ 1.0d0/5.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(3,1:7) = [ 3.0d0/40.0d0, 9.0d0/40.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(4,1:7) = [ 44.0d0/45.0d0, -56.0d0/15.0d0, 32.0d0/9.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(5,1:7) = [ 19372.0d0/6561.0d0, -25360.0d0/2187.0d0, 64448.0d0/6561.0d0, -212.0d0/729.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(6,1:7) = [ 9017.0d0/3168.0d0, -355.0d0/33.0d0, 46732.0d0/5247.0d0, 49.0d0/176.0d0, -5103.0d0/18656.0d0, 0.0d0, 0.0d0 ]
				a(7,1:7) = [ 35.0d0/384.0d0, 0.0d0, 500.0d0/1113.0d0, 125.0d0/192.0d0, -2187.0d0/6784.0d0, 11.0d0/84.0d0, 0.0d0 ]
				b(1:7) = [ 35.0d0/384.0d0, 0.0d0, 500.0d0/1113.0d0, 125.0d0/192.0d0, -2187.0d0/6784.0d0, 11.0d0/84.0d0, 0.0d0 ]
				c(1:7) = [ 0.0d0, 1.0d0/5.0d0, 3.0d0/10.0d0, 4.0d0/5.0d0, 8.0d0/9.0d0, 1.0d0, 1.0d0 ]
	!-----------------------------------------------------------------------------
			case('RKp6n1')
			! 'Метод Рунге--Кутты 6-го порядка (Батчер)'
				method_stage = 7
				method_order = 6
				allocate(a(1:method_stage,1:method_stage), b(1:method_stage))
				allocate(c(1:method_stage), k(1:equations_number,1:method_stage))
				a(1,1:7) = 0.0d+00
				
				a(2,1)   = 0.5d+00
				a(2,2:7) = 0.0d+00
				
				a(3,1:2) = [ 2.0d+00/9.0d+00, 4.0/9.0d+00 ]
				a(3,3:7) = 0.0d+00
				
				a(4,1:3) = [ 7.0d+00/36.0d+00, 2.0d+00/9.0d+00, -1.0d+00/12.0d+00 ]
				a(4,4:7) = 0.0d+00
				
				a(5,1:4) = [ -35.0d+00/144.0d+00, -55.0d+00/36.0d+00, 35.0d+00/48.0d+00, 15.0d+00/8.0d+00 ]
				a(5,5:7) = 0.0d+00
				
				a(6,1:5) = [ -1.0d+00/360.0d+00, -11.0d+00/36.0d+00, -1.0/8.0d+00, 0.5d+00, 0.1d+00 ]
				a(6,6:7) = 0.0d+00
				
				a(7,1:3) = [ -41.0d+00/260.0d+00, 22.0d+00/13.0d+00, 43.0d+00/156.0d+00 ]
				a(7,4:6) = [ -118.0d+00/39.0d+00, 32.0d+00/195.0d+00, 80.0d+00/39.0d+00 ]
				a(7,7)   = 0.0+00
				
				b(1:3) = [ 13.0d+00/200.0d+00, 0.0d+00, 11.0d+00/40.0d+00 ] 
				b(4:6) = [ 11.0d+00/40.0d+00, 4.0d+00/25.0d+00, 4.0d+00/25.0d+00 ]
				b(7) = 13.0d+00/200.0d+00 
				
				c(1:4) = [ 0.0d+00, 0.5d+00, 2.0d+00/3.0d+00, 1.0d+00/3.0d+00 ]
				c(5:7) = [ 5.0d+00/6.0d+00, 1.0d+00/6.0d+00, 1.0d+00 ]
	!-----------------------------------------------------------------------------
			case default
			! 'Метод Рунге--Кутты 4-ого порядка III (правило 3/8)'
				method_stage = 4
				method_order = 4
				allocate(a(1:method_stage,1:method_stage), b(1:method_stage))
				allocate(c(1:method_stage), k(1:equations_number,1:method_stage))
				a(1,1:4) = [  0.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(2,1:4) = [  1.0d0/3.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(3,1:4) = [ -1.0d0/3.0d0, 1.0d0, 0.0d0, 0.0d0 ]
				a(4,1:4) = [  1.0d0, -1.0d0, 1.0d0, 0.0d0 ]
				b(1:4) = [ 1.0d0/8.0d0, 3.0d0/8.0d0, 3.0d0/8.0d0, 1.0d0/8.0d0 ]
				c(1:4) = [ 0.0d0, 1.0d0/3.0d0, 2.0d0/3.0d0, 1.0d0 ]
		end select
	end subroutine CoefficientsInit
!-------------------------------------------------------------------------------
!	          Один шаг метода Рунге-Кутты (для внутреннего использования)
!-------------------------------------------------------------------------------
	subroutine RKMethodStep(func, y, t, p, h)
		implicit none	
			procedure(ode_func)                                             :: func
			double precision, dimension(1:equations_number), intent(inout)  :: y
			double precision, intent(in)                                    :: t
			double precision, dimension(:), intent(in)                      :: p
			double precision, intent(in)                                    :: h
		!---------------------------------------------------------------------------
			integer                                                         :: s
			integer                                                         :: EQN
			integer                                                         :: i, alpha
			double precision, dimension(1:equations_number)                 :: g
		!---------------------------------------------------------------------------
		s = method_stage
		EQN = equations_number
		k = 0.0
		! {----------------------- Метод Рунге-Кутта -------------------------------
			do i = 1,s,1
				forall(alpha = 1:EQN)
					g(alpha) = y(alpha) + h*dot_product(k(alpha,1:s),a(i,1:s))
				end forall
				k(1:EQN,i) = func(t + h*c(i), g(1:EQN), p)
			end do
			forall(alpha = 1:EQN)
				y(alpha) = y(alpha) + h*dot_product(k(alpha,1:s), b(1:s))
			end forall
		! -------------------------------------------------------------------------}
	end subroutine RKMethodStep
!-------------------------------------------------------------------------------
! Проверка переданных аргументов на адекватность (возвращает 0 в случае успех)
!-------------------------------------------------------------------------------
 pure function ArgumentsChecker(t, time_interval)
		implicit none
		double precision, intent(in) :: t
		double precision, intent(in) :: time_interval(1:2)
		double precision             :: t_start, t_stop
		integer                      :: ArgumentsChecker

		t_start = time_interval(1)
		t_stop  = time_interval(2)

		if( t >= t_stop) then
			ArgumentsChecker = -1
		else if(t_start >= t_stop) then
			ArgumentsChecker = -2
		else if (t < t_start ) then
			ArgumentsChecker = -3
		else
			ArgumentsChecker = 0
		end if	
 end function ArgumentsChecker
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!	          Классический  метода Рунге-Кутты полный цикл
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	subroutine RK(func, EQN, y, t, p, time_interval, h, method)
		implicit none
		procedure(ode_func)                                 :: func
		integer, intent(in)                                 :: EQN
		double precision, dimension(1:EQN), intent(inout)   :: y
		double precision, intent(inout)                     :: t
		double precision, dimension(:), intent(in)          :: p
		double precision, dimension(1:2), intent(in)        :: time_interval
		double precision, intent(in)                        :: h
		character(len = *), optional, intent(in)            :: method
	!-----------------------------------------------------------------------------
		character(len = 32)                                 :: method_code_name
	!-----------------------------------------------------------------------------
	! Проверка параметров	
		if ( ArgumentsChecker(t,time_interval) /= 0 )  error stop "Arguments error!"
		
		! Инициализируем все глобальные переменные
		equations_number = EQN
		method_code_name = trim(adjustl(method))
		call CoefficientsInit(method_code_name)
		
	! {------------------------ Метод Рунге-Кутта --------------------------------
		do while(t <= time_interval(2))
			call RKMethodStep(func, y, t, p, h)
			t = t + h
		end do
	! ---------------------------------------------------------------------------}
		deallocate(a,b,c,k)
	end subroutine RK
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!    Классический  метода Рунге-Кутты полный цикл с распечаткой
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	subroutine RK_print(func, EQN, y, t, p, time_interval, h, method)
		implicit none
		procedure(ode_func)                                 :: func
		integer, intent(in)                                 :: EQN
		double precision, dimension(1:EQN), intent(inout)   :: y
		double precision, intent(inout)                     :: t
		double precision, dimension(:), intent(in)          :: p
		double precision, dimension(1:2), intent(in)        :: time_interval
		double precision, intent(in)                        :: h
		character(len = *), optional, intent(in)            :: method
	!-----------------------------------------------------------------------------
		character(len = 32)                                 :: method_code_name
		character(len = 32)                                 :: format_string
		character(len = 10)                                 :: columns_number
	!-----------------------------------------------------------------------------
	! Проверка параметров	
		if ( ArgumentsChecker(t,time_interval) /= 0 )  error stop "Arguments error!"
		
		! Инициализируем все глобальные переменные
		equations_number = EQN
		method_code_name = trim(adjustl(method))
		call CoefficientsInit(method_code_name)
		
		write(columns_number,'(i10)'), equations_number 
		format_string = '(SP,'//trim(adjustl(columns_number))//'(e24.17,","),1e24.17)'
		
	! {------------------------ Метод Рунге-Кутта --------------------------------
		do while(t <= time_interval(2))
			write(*,format_string) t, y
			call RKMethodStep(func, y, t, p, h)
			t = t + h
		end do
	! ---------------------------------------------------------------------------}
		deallocate(a,b,c,k)
	end subroutine RK_print
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!  Классический  метода Р-К полный цикл с накоплением результатов в массиве
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! y_o — начальное значение функции
! t_o — начальный момент времени
! y_out — массив, в который записываются все вычисленные значения
! t_out — массив, в который записываются все моменты времени
	subroutine RKsteps(func, EQN, y_o, t_o, p, y_out, t_out, time_interval, h, method)
		implicit none
		procedure(ode_func)                                 :: func
		integer, intent(in)                                 :: EQN
		double precision, dimension(1:EQN), intent(in)      :: y_o
		double precision, intent(in)                        :: t_o
		double precision, dimension(:), intent(in)          :: p
		double precision, allocatable, intent(inout)        :: y_out(:)
		double precision, allocatable, intent(inout)        :: t_out(:)
		double precision, dimension(1:2), intent(in)        :: time_interval
		double precision, intent(in)                        :: h
		character(len = *), optional, intent(in)            :: method
	!-----------------------------------------------------------------------------
		character(len = 32)                                 :: method_code_name
		double precision, dimension(1:EQN)                  :: y
		double precision                                    :: t
	!-----------------------------------------------------------------------------
	! Проверка параметров	
		if ( ArgumentsChecker(t,time_interval) /= 0 ) error stop "Arguments error!"
		
		! Инициализируем все глобальные переменные
		equations_number = EQN
		method_code_name = trim(adjustl(method))
		call CoefficientsInit(method_code_name)
		
		y = y_o
		t = t_o
	! {------------------------ Метод Рунге-Кутта --------------------------------
		do while(t <= time_interval(2))
			call RKMethodStep(func, y, t, p, h)
			call array_append(y, y_out)
			!call element_append(t, t_out)
			t = t + h
		end do
	! ---------------------------------------------------------------------------}
		deallocate(a,b,c,k)
	end subroutine RKsteps
	
!===============================================================================
!                     Вспомогательные процедуры и функции
!===============================================================================
	!-----------------------------------------------------------------------------
	! Подпрограмма добавляет в массив новые элементы
	! размер массива растет по мере добавления элементов
	!-----------------------------------------------------------------------------
	subroutine array_append(gest_array, host_array)
		implicit none
		double precision, dimension(:), intent(in)                 :: gest_array
		double precision, dimension(:), allocatable, intent(inout) :: host_array ! Должен быть динамическим
		double precision, dimension(:), allocatable                :: tmp_array
		integer :: g_s, h_s
		
		g_s = size(gest_array)
		h_s = size(host_array)
		allocate(tmp_array(1:(h_s + g_s)))
		
		tmp_array(1:h_s) = host_array
		deallocate(host_array)
		tmp_array((h_s+1):(h_s+g_s)) = gest_array
		
		call move_alloc(tmp_array, host_array)
	end subroutine array_append
	!-----------------------------------------------------------------------------
	! Подпрограмма добавляет в массив новый элемент
	!-----------------------------------------------------------------------------
	subroutine element_append(element, array)
		implicit none
		double precision, intent(in)                               :: element
		double precision, dimension(:), allocatable, intent(inout) :: array
		double precision, dimension(:), allocatable                :: tmp_array
		integer :: s
		
		s = size(array)
		allocate(tmp_array(1:(s+1)))
		
		tmp_array(1:s) = array
		deallocate(array)
		tmp_array(s+1) = element
		
		call move_alloc(tmp_array, array)
	end subroutine element_append
	
	!-----------------------------------------------------------------------------
	! Функция, вычисляющая Евклидову норму нужна в случае точности в 16 байт
	!-----------------------------------------------------------------------------
	pure double precision function euclid_norm(x)
		implicit none
		double precision, dimension(:), intent(in) ::x
		euclid_norm = sqrt(sum(x**2))
	end function euclid_norm
	!-----------------------------------------------------------------------------
	!	 Форматированный вывод матрицы (тип real(8))
	! m — число строк
	! n — число столбцов
	!-----------------------------------------------------------------------------
	subroutine print_matrix(matrix)
		implicit none
		integer :: i,j, m, n
		double precision, dimension(1:,1:), intent(in) :: matrix
		character(len = 16) :: columns ! переменная для задания форматирования
		character(len = 128) :: format_string
		m = size(matrix,1)
		n = size(matrix,2)
		write(columns,'(i10)'), n-1
		! SP — обязательны знаки + и - перед числом
		format_string = '(SP,'//trim(adjustl(columns))//'(e24.17,","),1e24.17)'
		write (*,format_string) ((matrix(i,j), j = 1,n), i = 1,m)
	end subroutine print_matrix
end module RKs
