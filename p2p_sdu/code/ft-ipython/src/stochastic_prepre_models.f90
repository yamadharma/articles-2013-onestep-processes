module stochastic_prepre_models
implicit none
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! Модуль содержит функции, задающие правые часть систем СДУ, описывающих
! стохастические модели типа «хищник-жертва» ("predator-prey")
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	abstract interface
		function A(t, x, p)
			implicit none
			double precision, intent(in) :: t
			double precision, dimension(:), intent(in) :: x
			double precision, dimension(:), intent(in) :: p ! Параметры
			double precision, dimension(1:ubound(x,1)) :: A
		end function A
		function B(t, x, p)
			implicit none
			double precision, intent(in) :: t
			double precision, dimension(:), intent(in) :: x
			double precision, dimension(:), intent(in) :: p ! Параметры
			double precision, dimension(lbound(x,1):ubound(x,1),lbound(x,1):ubound(x,1)) ::  B
		end function B
	end interface
	contains
!------------------------------------------------------------------
!	проверка результатов вычисления на осмысленность
!------------------------------------------------------------------
	pure function check(x)
		implicit none
		real(kind = 8), dimension(:), intent(in) :: x
		logical :: check
		if( minval(x) > 0.0d0 ) then
			check = .True.
		else
			check = .False.
		end if
	end function check
!-------------------------------------------------------------------------------
!            Классическая модель Хищник-Жертва (модель Лотки — Вольтерры)
!-------------------------------------------------------------------------------
	function PredatorPreyA(t,x,p)
		implicit none
		double precision, intent(in) :: t
		double precision, dimension(:), intent(in) :: x
		double precision, dimension(:), intent(in) :: p ! Параметры
		double precision, dimension(1:ubound(x,1)) :: PredatorPreyA
		double precision :: a, k1, k2, k3
		
		a  = p(1)
		k1 = p(2)
		k2 = p(3)
		k3 = p(4)
		
		PredatorPreyA(1) = x(1)*(a*k1 - k2*x(2))
		PredatorPreyA(2) = x(2)*(k2*x(1) - x(2))
	end function PredatorPreyA
	
!			Стохастическая часть

	function PredatorPreyB(t,x,p)
		use matrix
		implicit none
		double precision, intent(in) :: t
		double precision, dimension(:), intent(in) :: x
		double precision, dimension(:), intent(in) :: p ! Параметры
		double precision, dimension(lbound(x,1):ubound(x,1),lbound(x,1):ubound(x,1)) ::  PredatorPreyB
		double precision :: a, k1, k2, k3
		double precision, dimension(1:2,1:2) :: B

		a  = p(1)
		k1 = p(2)
		k2 = p(3)
		k3 = p(4)
		
		B(1,1:2) = [ x(1)*(a*k1 + k2*x(2)), -k2*x(1)*x(2) ]
		B(2,1:2) = [ -k2*x(1)*x(2), x(2)*(k2*x(1) + k3) ]
		
		PredatorPreyB = matrix_sqrt(B,2,2)
	end function PredatorPreyB
	
!-------------------------------------------------------------------------------
!            Модель Хищник-Жертва с конкуренцией среди жертв
!-------------------------------------------------------------------------------
	function PredatorPreyPreyA(t,x,p)
		implicit none
		double precision, intent(in) :: t
		double precision, dimension(:), intent(in) :: x
		double precision, dimension(:), intent(in) :: p ! Параметры
		double precision, dimension(1:ubound(x,1)) :: PredatorPreyPreyA
		double precision :: a, k1, k2, k3
		
		a  = p(1)
		k1 = p(2)
		k2 = p(3)
		k3 = p(4)
		
		PredatorPreyPreyA(1) = x(1)*(a - k1*x(1) - k2*x(2))
		PredatorPreyPreyA(2) = x(2)*(k2*x(1) - k2)
	end function PredatorPreyPreyA
	
	!			Стохастическая часть

	function PredatorPreyPreyB(t,x,p)
		use matrix
		implicit none
		double precision, intent(in) :: t
		double precision, dimension(:), intent(in) :: x
		double precision, dimension(:), intent(in) :: p ! Параметры
		double precision, dimension(lbound(x,1):ubound(x,1),lbound(x,1):ubound(x,1)) ::  PredatorPreyPreyB
		double precision :: a, k1, k2, k3
		double precision, dimension(1:2,1:2) :: B

		a  = p(1)
		k1 = p(2)
		k2 = p(3)
		k3 = p(4)
		
		B(1,1:2) = [ x(1)*(k1*x(1) + a + k2*x(2)), -k2*x(1)*x(2) ]
		B(2,1:2) = [ -k2*x(1)*x(2), x(2)*(k2*x(1) + k3) ]
		
		PredatorPreyPreyB = matrix_sqrt(B,2,2)
	end function PredatorPreyPreyB
!-------------------------------------------------------------------------------
!            Модель конкуренции
!-------------------------------------------------------------------------------
	function ConcursA(t,x,p)
		implicit none
		double precision, intent(in) :: t
		double precision, dimension(:), intent(in) :: x
		double precision, dimension(:), intent(in) :: p ! Параметры
		double precision, dimension(1:ubound(x,1)) :: ConcursA
		double precision :: a1, a2, k11, k22, k12, k21
		
		a1  = p(1)
		a2  = p(2)
		k11 = p(3)
		k12 = p(4)
		k21 = p(5)
		k22 = p(6)
		
		ConcursA(1) = x(1)*(a1 - k11*x(1) - k12*x(2))
		ConcursA(2) = x(2)*(a2 - k22*x(2) - k21*x(1))
	end function ConcursA
	
	!			Стохастическая часть

	function ConcursB(t,x,p)
		use matrix
		implicit none
		double precision, intent(in) :: t
		double precision, dimension(:), intent(in) :: x
		double precision, dimension(:), intent(in) :: p ! Параметры
		double precision, dimension(lbound(x,1):ubound(x,1),lbound(x,1):ubound(x,1)) ::  ConcursB
		double precision :: a1, a2, k11, k22, k12, k21
		double precision, dimension(1:2,1:2) :: B

		a1  = p(1)
		a2  = p(2)
		k11 = p(3)
		k12 = p(4)
		k21 = p(5)
		k22 = p(6)
		
		B(1,1:2) = [ x(1)*(a1 + k11*x(1) + k12*x(2)), 0.0d0 ]
		B(2,1:2) = [ 0.0d0, x(2)*(a2 + k22*x(2) + k21*x(1)) ]
		
		ConcursB = matrix_sqrt(B,2,2)
	end function ConcursB
!------------------------------------------------------------------
! Стохастическая модель симбиоза
!------------------------------------------------------------------
	function SymbiosisA(t, x, p)
		implicit none
		double precision, intent(in) :: t
		double precision, dimension(:), intent(in) :: x
		double precision, dimension(:), intent(in) :: p ! Параметры
		double precision, dimension(1:ubound(x,1)) :: SymbiosisA
		double precision :: b1, b2, k1, k2
		
		b1 = p(1)
		b2 = p(2)
		k1 = p(3)
		k2 = p(4)
		
		SymbiosisA(1) = x(1)*(k1*x(2) - b1)
		SymbiosisA(2) = x(2)*(k2*x(1) - b2)
	end function SymbiosisA
	
	!			Стохастическая часть

	function SymbiosisB(t,x,p)
		use matrix
		implicit none
		double precision, intent(in) :: t
		double precision, dimension(:), intent(in) :: x
		double precision, dimension(:), intent(in) :: p ! Параметры
		double precision, dimension(lbound(x,1):ubound(x,1),lbound(x,1):ubound(x,1)) ::  SymbiosisB
		double precision :: b1, b2, k1, k2
		double precision, dimension(1:2,1:2) :: B

		b1 = p(1)
		b2 = p(2)
		k1 = p(3)
		k2 = p(4)
		
		B(1,1:2) = [ x(1)*(k1*x(2) + b1), 0.0d0 ]
		B(2,1:2) = [ 0.0d0, x(2)*(k2*x(1) + b2) ]
		
		SymbiosisB = matrix_sqrt(B,2,2)
	end function SymbiosisB
	
!------------------------------------------------------------------
! 	Стохастическая модель Хищник-Жертва в случае трех популяций
!------------------------------------------------------------------
	function PredatorPrey3DA(t, x, p)
		implicit none
		double precision, intent(in) :: t
		double precision, dimension(:), intent(in) :: x
		double precision, dimension(:), intent(in) :: p ! Параметры
		double precision, dimension(1:ubound(x,1)) :: PredatorPrey3DA
		double precision :: a1, a2, c, k1, k2
		
		a1 = p(1)
		a2 = p(2)
		c  = p(3)
		k1 = p(4)
		k2 = p(5)
		
		PredatorPrey3DA(1) = x(1)*(a1 - k1*x(3))
		PredatorPrey3DA(2) = x(2)*(a2 - k2*x(3))
		PredatorPrey3DA(3) = -c*x(3)
	end function PredatorPrey3DA
	
	!			Стохастическая часть

	function PredatorPrey3DB(t,x,p)
		use matrix
		implicit none
		double precision, intent(in) :: t
		double precision, dimension(:), intent(in) :: x
		double precision, dimension(:), intent(in) :: p ! Параметры
		double precision, dimension(lbound(x,1):ubound(x,1),lbound(x,1):ubound(x,1)) ::  PredatorPrey3DB
		double precision :: a1, a2, c, k1, k2
		double precision, dimension(1:3,1:3) :: B

		a1 = p(1)
		a2 = p(2)
		c  = p(3)
		k1 = p(4)
		k2 = p(5)
		
		B(1,1:3) = [ x(1)*(a1 + k1*x(3)), 0.0d0, -k1*x(1)*x(3) ]
		B(2,1:3) = [ 0.0d0, x(2)*(a2 + k2*x(3)), -k2*x(2)*x(3) ]
		B(3,1:3) = [ -k1*x(1)*x(3), -k2*x(2)*x(3), x(3)*(k1*x(1) + k2*x(2) + c) ]
		
		PredatorPrey3DB = matrix_sqrt(B,3,3)
	end function PredatorPrey3DB
end module stochastic_prepre_models
