module NetWorm_models
implicit none
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! Модуль содержит функции, задающие правые часть систем ОДУ, описывающих
! стохастическую и детерминированную модели распространения сетевых червей
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	abstract interface
		function A(t, x, p)
			implicit none
			double precision, intent(in) :: t
			double precision, dimension(:), intent(in) :: x
			double precision, dimension(:), intent(in) :: p ! Параметры
			double precision, dimension(1:ubound(x,1)) :: A
		end function A
		function B(t, x, p)
			implicit none
			double precision, intent(in) :: t
			double precision, dimension(:), intent(in) :: x
			double precision, dimension(:), intent(in) :: p ! Параметры
			double precision, dimension(lbound(x,1):ubound(x,1),lbound(x,1):ubound(x,1)) ::  B
		end function B
	end interface
	
	contains
!------------------------------------------------------------------
!	проверка результатов вычисления на осмысленность
!------------------------------------------------------------------
	pure function check(x)
		implicit none
		real(kind = 8), dimension(:), intent(in) :: x
		logical :: check
		if( minval(x) >= 0.0d0 ) then
			check = .True.
		else
			check = .False.
		end if
	end function check
	
!-------------------------------------------------------------------------------
!            Детерминированная модель распространения сетевых червей
!-------------------------------------------------------------------------------
	function ode_NetWorm(t,x,p)
		implicit none
		double precision, intent(in) :: t
		double precision, dimension(:), intent(in) :: x ! s, i, r
		double precision, dimension(:), intent(in) :: p ! Параметры
		double precision, dimension(1:ubound(x,1)) :: ode_NetWorm
		double precision :: alpha, lambda, mu
		
		alpha = p(1) ! интенсивность появления новых уязвимых узлов
		lambda = p(2) ! интенсивность взаимодействия зараженного узла с уязвимым
		mu = p(3) ! интенсивность иммунизации
		ode_NetWorm(1) = alpha - lambda*x(1)*x(2) - mu*x(1)
		ode_NetWorm(2) = lambda*x(1)*x(2) - mu*x(2)
		ode_NetWorm(3) = mu*(x(1) + x(2))
	end function ode_NetWorm
!-------------------------------------------------------------------------------
!			Детерминированная часть для стохастического случая модели FastTrack
!-------------------------------------------------------------------------------
	function ANetWorm(t,x,p)
		implicit none
		double precision, intent(in) :: t
		double precision, dimension(:), intent(in) :: x
		double precision, dimension(:), intent(in) :: p ! Параметры
		double precision, dimension(1:ubound(x,1)) :: ANetWorm
		double precision :: alpha, lambda, mu
		
		alpha = p(1) 
		lambda = p(2)
		mu = p(3)
		
		ANetWorm(1) = alpha - lambda*x(1)*x(2) - mu*x(1)
		ANetWorm(2) = lambda*x(1)*x(2) - mu*x(2)
		ANetWorm(3) = mu*(x(1) + x(2))
	end function ANetWorm
!-------------------------------------------------------------------------------
!			Стохастическая часть для стохастического случая модели FastTrack
!-------------------------------------------------------------------------------
	function BNetWorm(t,x,p)
		use matrix
		implicit none
		double precision, intent(in) :: t
		double precision, dimension(:), intent(in) :: x
		double precision, dimension(:), intent(in) :: p ! Параметры
		double precision, dimension(lbound(x,1):ubound(x,1),lbound(x,1):ubound(x,1)) ::  BNetWorm
		double precision :: alpha, lambda, mu
		double precision, dimension(1:3,1:3) :: B

		alpha = p(1) 
		lambda = p(2)
		mu = p(3)
		
		B(1,1:3) = [ alpha + mu*x(1) + lambda*x(1)*x(2), -lambda*x(1)*x(2), -mu*x(1) ]
		B(2,1:3) = [ -lambda*x(1)*x(2), mu*x(2) + lambda*x(1)*x(2), -mu*x(2) ]
		B(3,1:3) = [ -mu*x(1), -mu*x(2), mu*(x(1) + x(2)) ]
		BNetWorm = matrix_sqrt(B,3,3)
	end function BNetWorm
end module NetWorm_models
