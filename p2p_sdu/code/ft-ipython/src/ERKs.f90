module ERKs
implicit none
private
!-------------------------------------------------------------------------------
! Набор глобальных переменных вложенного метода Рунге--Кутты
!-------------------------------------------------------------------------------
	integer                                       :: method_stage
	integer                                       :: method_order
	integer                                       :: extrapolation_order
	integer                                       :: equations_number
	double precision, allocatable, dimension(:,:) :: a
	double precision, allocatable, dimension(:)   :: b
	double precision, allocatable, dimension(:)   :: b_hat
	double precision, allocatable, dimension(:)   :: c
	double precision, allocatable, dimension(:,:) :: k
	! Флаг включения отладочных сообщений
	logical, parameter                            :: debug = .false.
!-------------------------------------------------------------------------------
!           Набор глобальных переменных для управления шагом
!-------------------------------------------------------------------------------
	double precision                              :: factor
	double precision                              :: factor_max
	double precision                              :: factor_min
	integer                                       :: rho
	integer                                       :: q
	logical                                       :: step_rejected
!-------------------------------------------------------------------------------
!                  Доступные вовне подпрограммы и функции
!-------------------------------------------------------------------------------
public :: ERK
!-------------------------------------------------------------------------------
!  Абстрактный интерфейс для функции, представляющей правую часть системы ОДУ
!-------------------------------------------------------------------------------
	abstract interface 
		function ode_func(t,x,p)
			double precision, intent(in)               :: t ! Время
			double precision, dimension(:), intent(in) :: x ! Переменные
			double precision, dimension(:), intent(in) :: p ! Параметры
			double precision, dimension(1:ubound(x,1)) :: ode_func
		end function ode_func
	end interface	
contains
!-------------------------------------------------------------------------------
!	                      Инициализация всех коэффициентов
!-------------------------------------------------------------------------------
	subroutine CoefficientsInit(method_code_name)
		character(len = *), intent(in) :: method_code_name
		select case (trim(method_code_name))
!-------------------------------------------------------------------------------
			case('ERK2(1)') !< --- не работает !
			! Heun–Euler
				method_stage = 2
				method_order = 2
				extrapolation_order = 1
				allocate(a(1:method_stage,1:method_stage))
				allocate(b(1:method_stage), b_hat(1:method_stage))
				allocate(c(1:method_stage), k(1:equations_number,1:method_stage))
				a(1,1:2) = 0.0d0
				a(2,1:2) = [ 1.0d0, 0.0d0 ]
				
				b(1:2) = [ 0.5d0, 0.5d0 ]
				b_hat(1:2) = [ 1.0d0, 0.0d0 ]
				c(1:2) = [ 0.0d0, 1.0d0 ]
!-------------------------------------------------------------------------------
			case('ERK3(2)')
			! Bogacki–Shampine
				method_stage = 4
				method_order = 3
				extrapolation_order = 2
				allocate(a(1:method_stage,1:method_stage))
				allocate(b(1:method_stage), b_hat(1:method_stage))
				allocate(c(1:method_stage), k(1:equations_number,1:method_stage))
				
				a(1,1:4) = [ 0.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(2,1:4) = [ 0.5d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(3,1:4) = [ 0.0d0, 3.0d0/4.0d0, 0.0d0, 0.0d0 ]
				a(4,1:4) = [ 2.0d0/9.0d0, 1.0d0/3.0d0, 4.0d0/9.0d0, 0.0d0 ]
				
				b(1:4) = [ 2.0d0/9.0d0, 1.0d0/3.0d0, 4.0d0/9.0d0, 0.0d0 ]
				b_hat(1:4) = [ 7.0d0/24.0d0, 1.0d0/4.0d0, 1.0d0/3.0d0, 1.0d0/8.0d0 ]
				
				c(1:4) = [ 0.0d0, 0.5d0, 3.0d0/4.0d0, 1.0d0 ]
!-------------------------------------------------------------------------------
			case('ERK4(3)')
				method_stage = 5
				method_order = 4
				extrapolation_order = 3
				allocate(a(1:method_stage,1:method_stage))
				allocate(b(1:method_stage), b_hat(1:method_stage))
				allocate(c(1:method_stage), k(1:equations_number,1:method_stage))
				
				a(1,1:5) = [ 0.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(2,1:5) = [ 1.0d0/2.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(3,1:5) = [ 0.0d0, 1.0d0/2.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(4,1:5) = [ 0.0d0, 0.0d0, 1.0d0, 0.0d0, 0.0d0 ]
				a(5,1:5) = [ 5.0d0/32.0d0, 7.0d0/32.0d0, 13.0d0/32.0d0, -1.0d0/32.0d0, 0.0d0 ]
				c(1:5) = [ 0.0d0, 1.0d0/2.0d0, 1.0d0/2.0d0, 1.0d0, 3.0d0/4.0d0 ]
				b(1:5) = [ 1.0d0/6.0d0, 1.0d0/3.0d0, 1.0d0/3.0d0, 1.0d0/6.0d0, 0.0d0 ]
				b_hat(1:5) = [ -1.0d0/2.0d0, 7.0d0/3.0d0, 7.0d0/3.0d0, 13.0d0/6.0d0, -16.0d0/3.0d0 ]
!-------------------------------------------------------------------------------
			case('ERK4(5)') ! <-- очень долгие вычисления 
				method_stage = 5
				method_order = 4
				extrapolation_order = 5
				allocate(a(1:method_stage,1:method_stage))
				allocate(b(1:method_stage), b_hat(1:method_stage))
				allocate(c(1:method_stage), k(1:equations_number,1:method_stage))
				
				a(1,1:5) = [ 0.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(2,1:5) = [ 1.0d0/3.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(3,1:5) = [ 1.0d0/6.0d0, 1.0d0/6.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(4,1:5) = [ 1.0d0/8.0d0, 0.0d0, +3.0d0/8.0d0, 0.0d0, 0.0d0 ]
				a(5,1:5) = [ 1.0d0/2.0d0, 0.0d0, -3.0d0/8.0d0, 2.0d0, 0.0d0 ]
				c(1:5) = [ 0.0d0, 1.0d0/3.0d0, 1.0d0/3.0d0, 1.0d0/2.0d0, 1.0d0 ]
				b(1:5) = [ 1.0d0/2.0d0, 0.0d0, -3.0d0/2.0d0, 2.0d0, 0.0d0 ]
				b_hat(1:5) = [ 1.0d0/6.0d0, 0.0d0, 0.0d0, 2.0d0/3.0d0, 1.0d0/6.0d0 ]
			!-------------------------------------------------------------------------------
			case('Fehlberg_ERK4(5)')
				method_stage = 6
				method_order = 4
				extrapolation_order = 5
				allocate(a(1:method_stage,1:method_stage))
				allocate(b(1:method_stage), b_hat(1:method_stage))
				allocate(c(1:method_stage), k(1:equations_number,1:method_stage))
				
				a(1,1:6) = [ 0.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(2,1:6) = [ 1.0d0/4.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(3,1:6) = [ 3.0d0/32.0d0, 9.0d0/32.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(4,1:6) = [ 1932.0d0/2197.0d0, -7200.0d0/2197.0d0, 7296.0d0/2197.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(5,1:6) = [ 439.0d0/216.0d0, -8.0d0, 3680.0d0/513.0d0, -845.0d0/4104.0d0, 0.0d0, 0.0d0 ]
				a(6,1:6) = [ -8.0d0/27.0d0, 2.0d0, -3544.0d0/2565.0d0, 1859.0d0/4104.0d0, -11.0d0/40.0d0, 0.0d0 ]
				c(1:6) = [ 0.0d0, 1.0d0/4.0d0, 3.0d0/8.0d0, 12.0d0/13.0d0, 1.0d0, 1.0d0/2.0d0 ]
				b(1:6) = [ 25.0d0/216.0d0, 0.0d0, 1408.0d0/2565.0d0, 2197.0d0/4104.0d0, -1.0d0/5.0d0, 0.0d0 ]
				b_hat(1:6) = [ 16.0d0/135.0d0, 0.0d0, 6656.0d0/12825.0d0, 28561.0d0/56430.0d0, -9.0d0/50.0d0, 2.0d0/55.0d0 ]
!-------------------------------------------------------------------------------
			case('Cash-Karp_ERK4(5)') ! <-- глобальную погрешность почему-то дает большую, чем Fehlberg_ERK4(5)
			! Cash-Karp parameters of the Runge-Kutta-Fehlberg4(5) method
				method_stage = 6
				method_order = 4
				extrapolation_order = 5
				allocate(a(1:method_stage,1:method_stage))
				allocate(b(1:method_stage), b_hat(1:method_stage))
				allocate(c(1:method_stage), k(1:equations_number,1:method_stage))
				
				a(1,1:6) = 0.0d0
				a(2,1) = 1.0d0/5.0d0
				a(2,2:6) = 0.0d0
				a(3,1:2) = [ 3.0d0/40.0d0, 9.0d0/40.0d0 ]
				a(3,3:6) = 0.0d0
				a(4,1:3) = [ 3.0d0/10.0d0, -9.0d0/10.0d0, 6.0d0/5.0d0 ]
				a(4,4:6) = 0.0d0
				a(5,1:4) = [ -11.0d0/54.0d0, 5.0d0/2.0d0, -70.0d0/27.0d0, 35.0d0/27.0d0 ]
				a(5,5:6) = 0.0d0
				a(6,1:3) = [ 1631.0d0/55296.0d0, 175.0d0/512.0d0, 575.0d0/13824.0d0 ]
				a(6,4:6) = [ 44275.0d0/110595.0d0, 253.0d0/4096.0d0, 0.0d0 ]
				c(1:6) = [ 0.0d0, 1.0d0/5.0d0, 3.0d0/10.0d0, 3.0d0/5.0d0, 1.0d0, 7.0d0/8.0d0 ]
				b(1:6) = [ 37.0d0/378.0d0, 0.0d0, 250.0d0/621.0d0, 125.0d0/594.0d0, 0.0d0, 512.0d0/1771.0d0 ]
				b_hat(1:4) = [ 2825.0d0/27648.0d0, 0.0d0, 18575.0d0/48384.0d0, 13525.0d0/55296.0d0 ]
				b_hat(5:6) = [ 277.0d0/14336.0d0, 1.0d0/4.0d0 ]
				
!-------------------------------------------------------------------------------
			case('Fehlberg_ERK5(6)') ! <--- Доделать! Найти коэффициенты b_hat
				method_stage = 8
				method_order = 5
				extrapolation_order = 6
				allocate(a(1:method_stage,1:method_stage))
				allocate(b(1:method_stage), b_hat(1:method_stage))
				allocate(c(1:method_stage), k(1:equations_number,1:method_stage))
				
				a(1,1:8) = 0.0d0
				
				a(2,1) = 1.0d0/6.0d0
				a(2,2:8) = 0.0d0
				
				a(3,1:2) = [ 4.0d0/75.0d0, 16.0d0/75.0d0 ]
				a(3,3:8) = 0.0d0
				
				a(4,1:3) = [ 5.0d0/6.0d0, -16.0d0/6.0d0, 15.0d0/6.0d0 ]
				a(4,4:8) = 0.0d0
				
				a(5,1:4) = [ -40.0d0/25.0d0, 144.0d0/25.0d0, -100.0d0/25.0d0, 16.0d0/25.0d0 ]
				a(5,5:8) = 0.0d0
				
				a(6,1:5) = [ 722.0d0/640.0d0, -2304.0d0/640.0d0, 2035.0d0/640.0d0, -88.0d0/640.0d0, 275.0d0/640.0d0 ]
				a(6,6:8) = 0.0d0
				
				a(7,1:6) = [ -11.0d0/640.0d0, 0.0d0, 55.0d0/1280.0d0, -11.0d0/160.0d0, 55.0d0/1280.0d0, 0.0d0 ]
				a(7,7:8) = 0.0d0
				
				a(8,1:4) = [ 93.0d0/640.0d0, -18.0d0/5.0d0, 803.0d0/256.0d0, -11.0d0/160.0d0 ]
				a(8,5:8) = [ 99.0d0/256.0d0, 0.0d0, 1.0d0, 0.0d0 ]
				
!-------------------------------------------------------------------------------
			case('Dormand–Prince_ERK5(4)')
				! DOPRI5
				method_stage = 7
				method_order = 5
				extrapolation_order = 4
				allocate(a(1:method_stage,1:method_stage))
				allocate(b(1:method_stage), b_hat(1:method_stage))
				allocate(c(1:method_stage), k(1:equations_number,1:method_stage))
				
				a(1,1:7) = [ 0.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(2,1:7) = [ 1.0d0/5.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(3,1:7) = [ 3.0d0/40.0d0, 9.0d0/40.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(4,1:7) = [ 44.0d0/45.0d0, -56.0d0/15.0d0, 32.0d0/9.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(5,1:4) = [ 19372.0d0/6561.0d0, -25360.0d0/2187.0d0, 64448.0d0/6561.0d0, -212.0d0/729.0d0 ]
				a(5,5:7) = [ 0.0d0, 0.0d0, 0.0d0 ]
				a(6,1:4) = [ 9017.0d0/3168.0d0, -355.0d0/33.0d0, 46732.0d0/5247.0d0, 49.0d0/176.0d0 ]
				a(6,5:7) = [ -5103.0d0/18656.0d0, 0.0d0, 0.0d0 ]
				a(7,1:7) = [ 35.0d0/384.0d0, 0.0d0, 500.0d0/1113.0d0, 125.0d0/192.0d0, -2187.0d0/6784.0d0, 11.0d0/84.0d0, 0.0d0 ]
				c(1:7) = [ 0.0d0, 1.0d0/5.0d0, 3.0d0/10.0d0, 4.0d0/5.0d0, 8.0d0/9.0d0, 1.0d0, 1.0d0 ]
				b(1:4) = [ 35.0d0/384.0d0, 0.0d0, 500.0d0/1113.0d0, 125.0d0/192.0d0 ]
				b(5:7) = [ -2187.0d0/6784.0d0, 11.0d0/84.0d0, 0.0d0 ]
				b_hat(1:4) = [ 5179.0d0/57600.0d0, 0.0d0, 7571.0d0/16695.0d0, 393.0d0/640.0d0 ]
				b_hat(5:7) = [ -92097.0d0/339200.0d0, 187.0d0/2100.0d0, 1.0d0/40.0d0 ]
!-------------------------------------------------------------------------------
			case('DVERK_ERK6(5)')
			! Verner (1978), T.E. Hull, W.H.Enright, K.R. Jackson
				method_stage = 8
				method_order = 6
				extrapolation_order = 5
				allocate(a(1:method_stage,1:method_stage))
				allocate(b(1:method_stage), b_hat(1:method_stage))
				allocate(c(1:method_stage), k(1:equations_number,1:method_stage))
				
				a(1,1:8) = 0.0d0
				
				a(2,1) = 1.0d0/6.0d0
				a(2,2:8) = 0.0d0
				
				a(3,1:2) = [ 4.0d0/75.0d0, 16.0d0/75.0d0 ]
				a(3,3:8) = 0.0d0
				
				a(4,1:3) = [ 5.0d0/6.0d0, -8.0d0/3.0d0, 5.0d0/2.0d0 ]
				a(4,4:8) = 0.0d0
				
				a(5,1:4) = [ -165.0d0/64.0d0, 55.0d0/6.0d0, -425.0d0/64.0d0, 85.0d0/96.0d0 ]
				a(5,5:8) = 0.0d0
				
				a(6,1:5) = [ 12.0d0/5.0d0, -8.0d0, 4015.0d0/612.0d0, -11.0d0/36.0d0, 88.0d0/255.0d0 ]
				a(6,6:8) = 0.0d0
				
				a(7,1:4) = [ -8263.0d0/15000.0d0, 124.0d0/75.0d0, -643.0d0/680.0d0, -81.0d0/250.0d0 ]
				a(7,5:8) = [ 2484.0d0/10625.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				
				a(8,1:3) = [ 3501.0d0/1720.0d0, -300.0d0/43.0d0, 297275.0d0/52632.0d0 ]
				a(8,4:8) = [ -319.0d0/2322.0d0, 24068.0d0/84065.0d0, 0.0d0, 3850.0d0/26703.0d0, 0.0d0 ]
				
				c(1:5) = [ 0.0d0, 1.0d0/6.0d0, 4.0d0/15.0d0, 2.0d0/3.0d0, 5.0d0/6.0d0 ]
				c(6:8) = [ 1.0d0, 1.0d0/15.0d0, 1.0d0 ]
				
				b(1:5) = [ 3.0d0/40.0d0, 0.0d0, 875.0d0/2244.0d0, 23.0d0/72.0d0, 264.0d0/1955.0d0 ]
				b(6:8) = [ 0.0d0, 125.0d0/11592.0d0, 43.0d0/616.0d0 ]
				
				b_hat(1:4) = [ 13.0d0/160.0d0, 0.0d0, 2375.0d0/5984.0d0, 5.0d0/16.0d0 ]
				b_hat(5:8) = [ 12.0d0/85.0d0, 3.0d0/44.0d0, 0.0d0, 0.0d0 ]
				
!-------------------------------------------------------------------------------
			case('Fehlberg_ERK7(8)') ! <==== Метод дает огромную глобальную погрешность
				method_stage = 13
				method_order = 7
				extrapolation_order = 8
				allocate(a(1:method_stage,1:method_stage))
				allocate(b(1:method_stage), b_hat(1:method_stage))
				allocate(c(1:method_stage), k(1:equations_number,1:method_stage))
				a(1,1:13) = 0.0d0
				
				a(2,1) = 2.0d0/27.0d0
				a(2,2:13) = 0.0d0
				
				a(3,1:2) = [ 1.0d0/36.0d0, 1.0d0/12.0d0 ]
				a(3,3:13) = 0.0d0 
				
				a(4,1:3) = [ 1.0d0/24.0d0, 0.0d0, 1.0d0/8.0d0 ]
				a(4,4:13) = 0.0d0
				
				a(5,1:4) = [ 5.0d0/12.0d0, 0.0d0, -25.0d0/16.0d0, 25.0d0/16.0d0 ]
				a(5,5:13) = 0.0d0
				
				a(6,1:5) = [ 1.0d0/20.0d0, 0.0d0, 0.0d0, 1.0d0/4.0d0, 1.0d0/5.0d0 ]
				a(6,6:13) = 0.0d0
				
				a(7,1:4) = [ -25.0d0/108.0d0, 0.0d0, 0.0d0, 125.0d0/108.0d0 ]
				a(7,5:6) = [-65.0d0/27.0d0, 125.0d0/54.0d0 ]
				a(7,7:13) = 0.0d0
				
				a(8,1:5) = [ 31.0d0/300.0d0, 0.0d0, 0.0d0, 0.0d0, 61.0d0/225.0d0 ]
				a(8,6:7) = [ -2.0d0/9.0d0, 13.0d0/900.0d0 ]
				a(8,7:13) = 0.0d0
				
				a(9,1:5) = [ 2.0d0, 0.0d0, 0.0d0, -53.0d0/6.0d0, 704.0d0/45.0d0 ]
				a(9,6:8) = [ -107.0d0/9.0d0, 67.0d0/90.0d0, 3.0d0 ]
				a(9,9:13) = 0.0d0
				
				a(10,1:4) = [ -91.0d0/108.0d0, 0.0d0, 0.0d0, 23.0d0/108.0d0 ]
				a(10,5:7) = [ -976.0d0/135.0d0, 311.0d0/54.0d0, -19.0d0/60.0d0 ]
				a(10,8:9) = [ 17.0d0/6.0d0, -1.0d0/12.0d0 ]
				a(10,10:13) = 0.0d0
				
				a(11,1:4) = [ 2383.0d0/4100.0d0, 0.0d0, 0.0d0, -341.0d0/164.0d0 ]
				a(11,5:7) = [ 4496.0d0/1025.0d0, -301.0d0/82.0d0, 2133.0d0/4100.0d0 ]
				a(11,8:10) = [ 45.0d0/82.0d0, 45.0d0/164.0d0, 18.0d0/41.0d0 ]
				a(11,11:13) = 0.0d0
				
				a(12,1:6) = [ 3.0d0/205.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0, -6.0d0/41.0d0 ]
				a(12,7:10) = [ -3.0d0/205.0d0, -3.0d0/41.0d0, 3.0d0/41.0d0, 6.0d0/41.0d0 ]
				a(12,11:13) = 0.0d0
				
				a(13,1:4) = [ -1777.0d0/4100.0d0, 0.0d0, 0.0d0, -341.0d0/164.0d0 ]
				a(13,5:6) = [ 4496.0d0/1025.0d0, -289.0d0/82.0d0 ]
				a(13,7:9) = [ 2193.0d0/4100.0d0, 51.0d0/82.0d0, 33.0d0/164.0d0 ]
				a(13,10:13) = [ 12.0d0/41.0d0, 0.0d0, 1.0d0, 0.0d0 ]
				
				b(1:5) = [ 41.0d0/840.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0 ] 
				b(6:9) = [ 34.0d0/105.0d0, 9.0d0/35.0d0, 9.0d0/35.0d0, 9.0d0/280.0d0 ]
				b(10:13) = [ 9.0d0/280.0d0, 41.0d0/840.0d0, 0.0d0, 0.0d0 ]
				
				b_hat(1:5) = 0.0d0
				b_hat(6:8) = [ 34.0d0/105.0d0, 9.0d0/35.0d0, 9.0d0/35.0d0 ]
				b_hat(9:11) = [ 9.0d0/280.0d0, 9.0d0/280.0d0, 0.0d0 ]
				b_hat(12:13) = [ 41.0d0/840.0d0, 41.0d0/840.0d0 ]
				
				c(1:5) = [ 0.0d0, 2.0d0/27.0d0, 1.0d0/9.0d0, 1.0d0/6.0d0, 5.0d0/12.0d0 ]
				c(6:9) = [ 1.0d0/2.0d0, 5.0d0/6.0d0, 1.0d0/6.0d0, 2.0d0/3.0d0 ] 
				c(10:13) = [ 1.0d0/3.0d0, 1.0d0, 0.0d0, 1.0d0 ]
!-------------------------------------------------------------------------------
			case default
				! DOPRI5
				method_stage = 7
				method_order = 5
				extrapolation_order = 4
				allocate(a(1:method_stage,1:method_stage))
				allocate(b(1:method_stage), b_hat(1:method_stage))
				allocate(c(1:method_stage), k(1:equations_number,1:method_stage))
				
				a(1,1:7) = [ 0.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(2,1:7) = [ 1.0d0/5.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(3,1:7) = [ 3.0d0/40.0d0, 9.0d0/40.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(4,1:7) = [ 44.0d0/45.0d0, -56.0d0/15.0d0, 32.0d0/9.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0 ]
				a(5,1:4) = [ 19372.0d0/6561.0d0, -25360.0d0/2187.0d0, 64448.0d0/6561.0d0, -212.0d0/729.0d0 ]
				a(5,5:7) = [ 0.0d0, 0.0d0, 0.0d0 ]
				a(6,1:4) = [ 9017.0d0/3168.0d0, -355.0d0/33.0d0, 46732.0d0/5247.0d0, 49.0d0/176.0d0 ]
				a(6,5:7) = [ -5103.0d0/18656.0d0, 0.0d0, 0.0d0 ]
				a(7,1:7) = [ 35.0d0/384.0d0, 0.0d0, 500.0d0/1113.0d0, 125.0d0/192.0d0, -2187.0d0/6784.0d0, 11.0d0/84.0d0, 0.0d0 ]
				c(1:7) = [ 0.0d0, 1.0d0/5.0d0, 3.0d0/10.0d0, 4.0d0/5.0d0, 8.0d0/9.0d0, 1.0d0, 1.0d0 ]
				b(1:4) = [ 35.0d0/384.0d0, 0.0d0, 500.0d0/1113.0d0, 125.0d0/192.0d0 ]
				b(5:7) = [ -2187.0d0/6784.0d0, 11.0d0/84.0d0, 0.0d0 ]
				b_hat(1:4) = [ 5179.0d0/57600.0d0, 0.0d0, 7571.0d0/16695.0d0, 393.0d0/640.0d0 ]
				b_hat(5:7) = [ -92097.0d0/339200.0d0, 187.0d0/2100.0d0, 1.0d0/40.0d0 ]
!-------------------------------------------------------------------------------
		end select
	end subroutine CoefficientsInit
!-------------------------------------------------------------------------------
!                               Автоподбор шага
!-------------------------------------------------------------------------------
	function StepInit(f, t_o, y_o, p, A_tol, R_tol) result(h)
		implicit none
		procedure(ode_func)                                         :: f
		double precision, intent(in)                                :: t_o
		double precision, dimension(1:equations_number), intent(in) :: y_o
		double precision, dimension(:), intent(in)                  :: p
		double precision, intent(in)                                :: A_tol
		double precision, intent(in)                                :: R_tol
		double precision                                            :: h
	!---------------------------------------------------------------------------
		double precision                                :: d_o, d_1, d_2
		double precision                                :: h_o, h_1
		double precision, dimension(1:equations_number) :: y_1
		double precision, dimension(1:equations_number) :: scale_
		
		d_o = euclid_norm(y_o)
		d_1 = euclid_norm(f(t_o,y_o,p))
		scale_ = A_tol + abs(y_o)*R_tol
		
		if( (d_o < 1.0d-5) .or. (d_1 < 1.0d-5) ) then
			h_o = 1.0d-6
		else
			h_o = 0.01d0*(d_o/d_1)
		end if
		
		y_1 = y_o + h_o*f(t_o, y_o, p)
		d_2 = euclid_norm( f(t_o + h_o, y_1, p) - f(t_o, y_o, p) )/h_o
		
		if ( max(d_1,d_2) <= 1.0d-15 ) then
			h_1 = max(1.0d-6, h_o*1.0d-3)
		else
			h_1 = (0.01d0/max(d_1,d_2))**(1.0d0/(1.0d0 + method_order))
		end if
		
		h = min(100.0d0*h_o, h_1)
	end function StepInit
!-------------------------------------------------------------------------------
!	  Один шаг вложенного метода Рунге-Кутты (для внутреннего использования)
!-------------------------------------------------------------------------------
	subroutine ERKMethodStep(func, y_o, y, y_hat, t, p, h)
		implicit none	
			procedure(ode_func)                                          :: func
			double precision, dimension(1:equations_number), intent(in)  :: y_o
			double precision, dimension(1:equations_number), intent(out) :: y
			double precision, dimension(1:equations_number), intent(out) :: y_hat
			double precision, intent(in)                                 :: t
			double precision, dimension(:), intent(in)                   :: p
			double precision, intent(in)                                 :: h
		!---------------------------------------------------------------------------
			integer                                                      :: s
			integer                                                      :: EQN
			integer                                                      :: i, alpha
			double precision, dimension(1:equations_number)              :: g
		!---------------------------------------------------------------------------
		s = method_stage
		EQN = equations_number
		k = 0.0
		! {------------------- Вложенный метод Рунге-Кутта -------------------------
			do i = 1,s,1
				forall(alpha = 1:EQN)
					g(alpha) = y_o(alpha) + h*dot_product(a(i,1:s),k(alpha,1:s))
				end forall
				k(1:EQN,i) = func(t + h*c(i), g(1:EQN), p)
			end do
			forall(alpha = 1:EQN)
				y(alpha) = y_o(alpha) + h*dot_product(b(1:s), k(alpha,1:s))
				y_hat(alpha) = y_o(alpha) + h*dot_product(b_hat(1:s), k(alpha,1:s))
			end forall
		! -------------------------------------------------------------------------}
	end subroutine ERKMethodStep
!-------------------------------------------------------------------------------
!                   Инициализация параметров управления шагом
!-------------------------------------------------------------------------------
	subroutine StepControlInit()
		implicit none
	! Заметим, что у Хайрера ϱ = rho всегда 0
		rho = 1
	  q = min(method_order, extrapolation_order) + 1 - rho
		factor_max = 1.5d0
		factor_min = 0.3d0
		factor = (0.25d0)**(1.0d0/dble(q))
	! Другие возможные значения 0.8, 0.9, (0.38)**(1/(method_order+1))
		step_rejected = .false.
	end subroutine StepControlInit
!-------------------------------------------------------------------------------
!                            Управление шагом
!-------------------------------------------------------------------------------
	subroutine StepControl(y_o, y, y_hat, t, A_tol, R_tol, h)
		implicit none
	!-----------------------------------------------------------------------------
		double precision, dimension(1:equations_number), intent(inout) :: y_o
		double precision, dimension(1:equations_number), intent(in)    :: y
		double precision, dimension(1:equations_number), intent(in)    :: y_hat
		double precision, intent(inout)                                :: t
		double precision, intent(in)                                   :: A_tol
		double precision, intent(in)                                   :: R_tol
		double precision, intent(inout)                                :: h
	!-----------------------------------------------------------------------------
		double precision                                               :: h_new
		double precision                                               :: error_
		double precision, dimension(1:equations_number)                :: scale_
		
		scale_ = A_tol + max(abs(y_o), abs(y))*R_tol
		error_ = euclid_norm( (y - y_hat)/(scale_*(h**rho)) )
		
		!error_ = sqrt( sum( ( (y - y_hat)/scale_*(h**rho) )**2 )/EQN )
		!error_ = maxval( abs( (y - y_hat)/scale_*(h**rho) ) )
		
		h_new = h*min( factor_max, max( factor_min, factor*(1.0d0/error_)**(1.0d0/dble(q)) ) )
		
		!---------------------------------------------------------------------------
		if(error_ <= 1.0d0) then
			t = t + h
			h = h_new
			factor_max = min(factor_max + 0.1d0, 1.5d0)
			y_o = y
			step_rejected = .false.
		else if(error_ > 1.0d0) then
			t = t
			h = h_new
			factor_max = 1.0d+00
			y_o = y_o
			step_rejected = .true.
		end if
	end subroutine StepControl
!-------------------------------------------------------------------------------
! Проверка переданных аргументов на адекватность (возвращает 0 в случае успех)
!-------------------------------------------------------------------------------
 pure function ArgumentsChecker(t, time_interval)
		implicit none
		double precision, intent(in) :: t
		double precision, intent(in) :: time_interval(1:2)
		double precision             :: t_start, t_stop
		integer                      :: ArgumentsChecker

		t_start = time_interval(1)
		t_stop  = time_interval(2)

		if( t >= t_stop) then
			ArgumentsChecker = -1
		else if(t_start >= t_stop) then
			ArgumentsChecker = -2
		else if (t < t_start ) then
			ArgumentsChecker = -3
		else
			ArgumentsChecker = 0
		end if	
 end function ArgumentsChecker
!===============================================================================
!                 Вложенный метод Рунге--Кутты, полный цикл
!===============================================================================
	subroutine ERK(func, EQN, y, t, p, time_interval, A_tol, R_tol, stdout, method)
		implicit none
		procedure(ode_func)                               :: func
		integer, intent(in)                               :: EQN
		double precision, dimension(1:EQN), intent(inout) :: y
		double precision, intent(inout)                   :: t
		double precision, dimension(:), intent(in)        :: p 
		double precision, dimension(1:2), intent(in)      :: time_interval
		double precision, intent(in)                      :: A_tol
		double precision, intent(in)                      :: R_tol
		logical, intent(in)                               :: stdout
		character(len = *), optional, intent(in)          :: method
	!-----------------------------------------------------------------------------
		double precision                                  :: h
		double precision, dimension(1:EQN)                :: y_o, y_hat
	!-----------------------------------------------------------------------------
		character(len = 32)                               :: method_code_name
		character(len = 32)                               :: format_string
		character(len = 10)                               :: columns_number
	!-----------------------------------------------------------------------------
	! Проверка параметров	
		if ( ArgumentsChecker(t,time_interval) /= 0 ) error stop "Arguments error!"
		
	! Инициализируем все глобальные переменные
		equations_number = EQN
		method_code_name = trim(adjustl(method))
		
		call CoefficientsInit(method_code_name)
		call StepControlInit()
	
	! Формат распечатки результатов
		write(columns_number,'(i10)'), equations_number*2
		format_string = '(SP,'//trim(adjustl(columns_number))//'(e24.17,","),1e24.17)'
		
	! Автоподбор шага
		h = StepInit(func, time_interval(1), y, p, A_tol, R_tol)
	
	! Инициализируем начальные значения
		y_o = y
		t = time_interval(1)
		
  ! {------------------- Вложенный метод Рунге-Кутта ---------------------------
		do while(t <= time_interval(2))
			! Распечатка
			if( stdout .and. (.not. step_rejected) ) then
				write(*,format_string) t, y_o, func(t, y_o, p)
			end if
			! Вычисляем y и y_hat
			call ERKMethodStep(func, y_o, y, y_hat, t, p, h)
			! С помощью выбранной подпрограммы управления шагом обновляем
			! y_o, t, h и присваиваем нужное значение переменной step_rejected
			call StepControl(y_o, y, y_hat, t, A_tol, R_tol, h)
		end do
	! ---------------------------------------------------------------------------}
		y = y_o
		deallocate(a,b,b_hat,c,k)
	end subroutine ERK
!===============================================================================
!                           Вспомогательные функции
!===============================================================================
!-------------------------------------------------------------------------------
!    Функция, вычисляющая Евклидову норму нужна в случае точности в 16 байт
!-------------------------------------------------------------------------------
	pure double precision function euclid_norm(x)
		implicit none
		double precision, dimension(:), intent(in) ::x
		euclid_norm = sqrt(sum(x**2))
	end function euclid_norm
	
	
end module ERKs
