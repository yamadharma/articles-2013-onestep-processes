module deterministic_prepre_models
implicit none
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! Модуль содержит функции, задающие правые часть систем ОДУ, описывающих
! детерминистические модели типа «хищник-жертва» ("predator-prey")
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	abstract interface
		function f(t, x, p)
			implicit none
			double precision, intent(in) :: t
			double precision, dimension(:), intent(in) :: x
			double precision, dimension(:), intent(in) :: p ! Параметры
			double precision, dimension(1:ubound(x,1)) :: f
		end function
	end interface
	
	contains
!------------------------------------------------------------------
! Классическая модель Хищник-Жертва (модель Лотки — Вольтерры)
!------------------------------------------------------------------
	function PredatorPrey(t, x, p)
		implicit none
		double precision, intent(in) :: t
		double precision, dimension(:), intent(in) :: x
		double precision, dimension(:), intent(in) :: p ! Параметры
		double precision, dimension(1:ubound(x,1)) :: PredatorPrey
		double precision :: r
		
		! Безразмерные уравнения с одним коэффициентом
		r = p(1)
		
		PredatorPrey(1) = x(1)*(1.0d0 - x(2))
		PredatorPrey(2) = r*(x(1) - 1.0d0)*x(2)
	end function PredatorPrey
!------------------------------------------------------------------
! Модель Хищник-Жертва с конкуренцией среди жертв
!------------------------------------------------------------------
	function PredatorPreyPrey(t, x, p)
		implicit none
		double precision, intent(in) :: t
		double precision, dimension(:), intent(in) :: x
		double precision, dimension(:), intent(in) :: p ! Параметры
		double precision, dimension(1:ubound(x,1)) :: PredatorPreyPrey
		double precision :: r1, r2
		
		r1 = p(1)
		r2 = p(2)
		
		PredatorPreyPrey(1) = x(1) - r1*x(1)**2 - x(1)*x(2)
		PredatorPreyPrey(2) = x(1)*x(2) - r2*x(2)
	end function PredatorPreyPrey
!------------------------------------------------------------------
! Модель конкуренции (в безразмерном виде, с тремя коэффициентами)
!------------------------------------------------------------------
	function Concurs(t, x, p)
		implicit none
		double precision, intent(in) :: t
		double precision, dimension(:), intent(in) :: x
		double precision, dimension(:), intent(in) :: p ! Параметры
		double precision, dimension(1:ubound(x,1)) :: Concurs
		double precision :: r, a12, a21
		
		r   = p(1)
		a12 = p(2)
		a21 = p(3)
		
		Concurs(1) =   x(1)*(1.0d0 - x(2) - a12*x(1))
		Concurs(2) = r*x(2)*(1.0d0 - x(1) - a21*x(2))
	end function Concurs
!------------------------------------------------------------------
! Модель симбиоза
!------------------------------------------------------------------
	function Symbiosis(t, x, p)
		implicit none
		double precision, intent(in) :: t
		double precision, dimension(:), intent(in) :: x
		double precision, dimension(:), intent(in) :: p ! Параметры
		double precision, dimension(1:ubound(x,1)) :: Symbiosis
		double precision :: b1, b2, k1, k2
		
		b1 = p(1)
		b2 = p(2)
		k1 = p(3)
		k2 = p(4)
		
		Symbiosis(1) = k1*x(1)*x(2) - b1*x(1)
		Symbiosis(2) = k2*x(1)*x(2) - b2*x(2)
	end function Symbiosis
!------------------------------------------------------------------
! Модель Хищник-Жертва в случае трех популяций
!------------------------------------------------------------------
	function PredatorPrey3D(t, x, p)
		implicit none
		double precision, intent(in) :: t
		double precision, dimension(:), intent(in) :: x
		double precision, dimension(:), intent(in) :: p ! Параметры
		double precision, dimension(1:ubound(x,1)) :: PredatorPrey3D
		double precision :: a1, a2, c, k1, k2
		
		a1 = p(1)
		a2 = p(2)
		c  = p(3)
		k1 = p(4)
		k2 = p(5)
		
		PredatorPrey3D(1) = a1*x(1) - k1*x(1)*x(3)
		PredatorPrey3D(2) = a2*x(2) - k2*x(2)*x(3)
		PredatorPrey3D(3) = -c*x(3)
	end function PredatorPrey3D
end module deterministic_prepre_models
