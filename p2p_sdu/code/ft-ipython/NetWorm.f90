!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!	Программа численно решает системы ОДУ, заданные в модуле
! NetWorm_models
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
program NetWorm
	use ERKs
	use sde_num_methods
	use NetWorm_models
	implicit none
!-------------------------------------------------------------------------------
	procedure(A), pointer				:: f_ode => null()
	procedure(A), pointer				:: A_ode => null()
	procedure(B), pointer				:: B_ode => null()
	integer                     :: EQN
	double precision, parameter :: h = 0.01
	double precision, parameter :: eps = 1.0d-8
	double precision, parameter :: t_start = 0.0d0
	double precision, parameter :: t_stop  = 20.0d0
	double precision            :: t
	! Сеточные функции
	double precision, allocatable, dimension(:) :: x
	double precision, allocatable, dimension(:) :: x_o
	! Массив для параметров функций различных моделей
	double precision, allocatable, dimension(:) :: p
	integer :: i
	
	character(len=32), dimension(1:7) :: arg
!-------------------------------------------------------------------------------	
	
	! Считываем все аргументы командной строки
	do i = 1,command_argument_count(),1
		call get_command_argument(i, arg(i))
	end do
	
	select case( trim(arg(1)) )
	! {---
		case('deterministic')
		
			f_ode => ode_NetWorm
			EQN = 3
			
			allocate(x(1:EQN), x_o(1:EQN), p(1:3))
			
			read(arg(2:7),*) x_o(1:3), p(1), p(2), p(3) ! s, i, r, alpha, lambda, mu
			
		case('stochastic')

			A_ode => ANetWorm
			B_ode => BNetWorm
			EQN = 3
			
			allocate(x(1:EQN), x_o(1:EQN), p(1:3))
			read(arg(2:7),*) x_o(1:3), p(1:3) ! s, i, r, alpha, lambda, mu
	!---}
	end select
!----------------  Вычисления --------------------
	x = x_o
	t = t_start
	select case( trim(arg(1)) )
	! {---
		case('deterministic')
			call ERK(f_ode, EQN, x, t, p, [t_start, t_stop], eps, eps, .true., 'DVERK_ERK6(5)')
		case('stochastic')
			call StRKp3_d(A_ode, B_ode, check, EQN, x, p, t, t_start, t_stop, h)
	!---}
	end select
	deallocate(x_o, x, p)
!----------------             --------------------	
end program NetWorm