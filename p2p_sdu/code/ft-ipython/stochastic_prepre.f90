!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!	Программа численно решает системы CДУ, заданные в модуле
! stochastic_prepre_models
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
program stochastic_prepre
	use sde_num_methods
	use stochastic_prepre_models
	implicit none
!-------------------------------------------------------------------------------
	procedure(A), pointer				:: A_ode => null()
	procedure(B), pointer				:: B_ode => null()
	integer                     :: EQN
	double precision, parameter :: h = 0.01
	double precision, parameter :: eps = 1.0d-8
	double precision, parameter :: t_start = 0.0d0
	double precision, parameter :: t_stop  = 20.0d0
	double precision            :: t
	! Сеточные функции
	double precision, allocatable, dimension(:) :: x
	double precision, allocatable, dimension(:) :: x_o
	! Массив для параметров функций различных моделей
	double precision, allocatable, dimension(:) :: p
	integer :: i
	
	character(len=32), dimension(1:9) :: arg
!-------------------------------------------------------------------------------	
	
	! Считываем все аргументы командной строки
	do i = 1,command_argument_count(),1
		call get_command_argument(i, arg(i))
	end do
	
	select case( trim(arg(1)) )
	! {---
		case('PredatorPrey')
			A_ode => PredatorPreyA
			B_ode => PredatorPreyB
			EQN = 2
			
			allocate(x(1:EQN), x_o(1:EQN), p(1:4))
			
			read(arg(2:7),*) x_o(1:2), p(1:4) ! a, k1, k2, k3
!-------------------------------------------------------------------------------
		case('PredatorPreyPrey') ! Хищник-жертва с конкуренцией среди жертв
			A_ode => PredatorPreyPreyA
			B_ode => PredatorPreyPreyB
			EQN = 2
			
			allocate(x(1:EQN), x_o(1:EQN), p(1:4))
			
			read(arg(2:7),*) x_o(1:2), p(1:4) ! a, k1, k2, k3
!-------------------------------------------------------------------------------			
		case('Concurs')
			A_ode => ConcursA
			B_ode => ConcursB
			EQN = 2
			
			allocate(x(1:EQN), x_o(1:EQN), p(1:6))
			
			read(arg(2:9),*) x_o(1:2), p(1:6) ! a1, a2, k11, k12, k21, k22
!-------------------------------------------------------------------------------
		case('Symbiosis')
			A_ode => ConcursA
			B_ode => ConcursB
			EQN = 2
			
			allocate(x(1:EQN), x_o(1:EQN), p(1:4))

			read(arg(2:7),*) x_o(1:2), p(1:4) ! x1o, x2o, b1, b2, k1, k2
!-------------------------------------------------------------------------------
		case('PredatorPrey3D')
			A_ode => PredatorPrey3DA
			B_ode => PredatorPrey3DB
			EQN = 3
			
			allocate(x(1:EQN), x_o(1:EQN), p(1:5))

			read(arg(2:9),*) x_o(1:3), p(1:5) ! x1o, x2o, a1,a2,c,k1,k2
!-------------------------------------------------------------------------------
	!---}
	end select
!----------------  Вычисления --------------------
	x = x_o
	t = t_start
	
	call StRKp3_d(A_ode, B_ode, check, EQN, x, p, t, t_start, t_stop, h)
	
	deallocate(x_o, x, p)
!----------------             --------------------	
end program stochastic_prepre