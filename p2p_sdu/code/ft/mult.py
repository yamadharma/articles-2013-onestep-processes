#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import subprocess
import matplotlib.pyplot as plt
import sys
import os
import time

#change the default matplotlib font, that doesn't print the utf-8 µ !                                              
#the 'well-known' latex definition $\mu$ works perfect in any case                                                 
#ref :http://stackoverflow.com/questions/10960463/non-ascii-characters-in-matplotlib                               
plt.rc('font', **{'sans-serif' : 'Arial', 'family' : 'sans-serif'})

def drange(start, stop, step):
	'''float version of range() function'''
	r = start
	while r < stop:
		yield r
		r += step
#---------------------
# Инициализация
fig = plt.figure(1)
ax = fig.add_subplot(1,1,1)

# На вход принимается один аргумент, определяющий что именно рисовать
# на данный момент это:
# 1) prepre --- детерминированная модель хищник-жертва
# 2) sprepre --- стохастическая модель  хищник-жертва
# 3) ft --- детерминированная модель fasttrack
# 4) sft --- стохастическая модель fasttrack

# Перехватываем аргумент командной строки
if(len(sys.argv) <= 1):
	print("Ни одна модель для построения не выбрана. Выход")
	sys.exit(0)

if(len(sys.argv) <= 2):
        graphlang = 'ru'
else:
        graphlang = sys.argv[2]

if(sys.argv[1] == 'prepre'):
        name = 'prepre'
        file_name = name + '_out.dat'
        print("Построение чертежа для детерминированной модели Хищник-Жертва")
        # Координаты аннотации
        textxy = (45, 30)
        ax.set_title(u'Фазовые портреты детерминированной модели Хищник-Жертва для различных отклонений от стационарных точек', fontsize=10)
        ax.set_xlabel(u'$x(t)$ — численность жертв', fontsize=14)
        ax.set_ylabel(u'$y(t)$ — численность хищников', fontsize=14)
elif(sys.argv[1] == 'sprepre'):
        name = 'sprepre'
        file_name = name + 'out.dat'
        print("Построение чертежа для стохастической модели Хищник-Жертва")
        # Координаты аннотации
        textxy = (45, 0)
        ax.set_title(u'Фазовые портреты стохастической модели Хищник-Жертва для различных отклонений от стационарных точек', fontsize=10)
        ax.set_xlabel(u'$x(t)$ — численность жертв', fontsize=14)
        ax.set_ylabel(u'$y(t)$ — численность хищников', fontsize=14)
elif(sys.argv[1] == 'ft'):
        name = 'ft'
        file_name = name + '_out.dat'
        print("Построение чертежа для детерминированной модели FastTrack")
        # Координаты аннотации
        textxy = (-45, 30)
        if (graphlang == 'en'):
                ax.set_title(u'Phase portraits of the FastTrack deterministic model for different deviations from the stationary points', fontsize=10)
                ax.set_xlabel(u'$x(t)$ — Seeders', fontsize=14)
                ax.set_ylabel(u'$y(t)$ — New Nodes', fontsize=14)
        else:
                ax.set_title(u'Фазовые портреты детерминированной модели FastTrack для различных отклонений от стационарных точек', fontsize=10)
                ax.set_xlabel(u'$x(t)$ — число раздающих узлов', fontsize=14)
                ax.set_ylabel(u'$y(t)$ — число новых узлов', fontsize=14)
elif(sys.argv[1] == 'sft'):
        name = 'sft'
        file_name = name + '_out.dat'
        print("Построение чертежа для стохастической модели FastTrack")
        # Координаты аннотации
        textxy = (-45, 30)
        if (graphlang == 'en'):
                ax.set_title(u'Phase portraits of the FastTrack stochastic model for different deviations from the stationary points', fontsize=10)
                ax.set_xlabel(u'$x(t)$ — Seeders', fontsize=14)
                ax.set_ylabel(u'$y(t)$ — New Nodes', fontsize=14)
        else:
                ax.set_title(u'Фазовые портреты стохастической модели FastTrack для различных отклонений от стационарных точек', fontsize=10)
                ax.set_xlabel(u'$x(t)$ — число раздающих узлов', fontsize=14)
                ax.set_ylabel(u'$y(t)$ — число новых узлов', fontsize=14)
else:
        print("Недопустимый аргумент!")
        sys.exit(-1)

if (graphlang == 'en'):
        if not os.path.exists('en'):
                os.makedirs('en')
        out_dir = 'en'
else:
        if not os.path.exists('ru'):
                os.makedirs('ru')
        out_dir = 'ru'    

# Список отклонений от стационарной точки	
r1 = drange(0.0, 11.0, 1.0)
r2 = drange(0.0, 11.0, 1.0)

# r1 = [0.0, 1.0, 2.0, 3.0, 4.0, 5.0]
# r2 = [0.0, 1.0, 2.0, 3.0, 4.0, 5.0]
for x, y in zip(r1,r2):
	p = []
	q = []
	print("(dx, dy) = ","(",x,y,")")
	t1 = time.time()
	# Запуск программы на фортране для вычислений
	proc = subprocess.Popen(['./main', name, str(x), str(y)],stdout = subprocess.PIPE)
	output = proc.communicate()[0]
	# errors = proc.communicate()[1]
	# Вычисления закончены и следует записать результаты в список (массив)
	for line in output.splitlines():
		p.append(line.split()[1])
		q.append(line.split()[2])
	# Строим график
	pl = ax.plot(p, q)[0]
	pl.set_color('black')
	pl.set_linewidth(0.4)
	s = "("+str(x)+","+str(y)+")"
	# Вместо легенды используем аннотации, так как графиков много, рисунок должен быть черно-белый и поэтому легенда ничего не отобразит
	ax.annotate(s, 
				xy = (p[0],q[0]), 
				xycoords='data',
				xytext=textxy,
				textcoords='offset points',
				bbox=dict(boxstyle="round", fc="1.0"),
				arrowprops=dict(arrowstyle="->")
				)
	del q
	del p
print("Сохранение картинки")
#fig.savefig(out_dir + '/' + 'all_'+name+'.png',dpi=300, format="png", pad_inches=0.1)
plt.savefig(out_dir + '/' + 'all_'+name+'.pdf',dpi=600, format="pdf", pad_inches=0.1)
