#!/usr/bin/gnuplot -smooth
# Выбор терминала и формата файла для вывода рисунка
if(exists("png")){
	set terminal pngcairo enhanced size 1024,768 font "DejaVu Serif"
}
else {	
	set terminal pdfcairo enhanced rounded font "DejaVu Serif"
}
#Установка кодировки
set encoding utf8
# Нарисовать сетку и засечки на осях
set grid xtics ytics
# Оставить только нижнюю и левую границы
set border 3
#Убрать ось x сверху и ось y снизу
set xtics nomirror
set ytics nomirror
# Число ломанных
set samples 50000
# Стиль линий (цветной)
set style line 1 linecolor rgb '#0060ad' linetype 1 linewidth 1
set style line 2 linecolor rgb '#dd181f' linetype 1 linewidth 1
# Стиль линий (черно-белый)
set style line 3 linecolor rgb '#000000' linetype -1 linewidth 1
set style line 4 linecolor rgb '#000000' linetype 0 linewidth 1
# Входной файл с данными
input_fname = system("read filename; echo $filename")
##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#					Фазовый портрет
##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 							Детерминированная модель хищник-жертва

if(exists("en")){
	system "mkdir -p en"
	lang = "en"
}

if(exists("ru")){
	system "mkdir -p ru"
	lang = "ru"
}

if(exists("prepre")){
	# Название картинки
	set title "Детерминированная модель Хищник-Жертва (фазовый портрет)"
	# Настройка осей
	set ylabel "y(t) — численность хищников"
	set xlabel "x(t) — численность жертв"
	key_text = "Хищник-Жертва"
	if(exists("png")){
		set output "prepre_phase.png"
	}
	else {
		set output "prepre_phase.pdf"
	}
}
# 							Стохастическая модель хищник-жертва
if(exists("sprepre")){
	# Название картинки
	set title "Стохастическая модель Хищник-Жертва (фазовый портрет)"
	# Настройка осей
	set ylabel "y(t) — численность хищников"
	set xlabel "x(t) — численность жертв"
	key_text = "Хищник-Жертва"
	if(exists("png")){
		set output "sprepre_phase.png"
	}
	else {
		set output "sprepre_phase.pdf"
	}
}
#			Детерминированная модель Fasttrack
if(exists("ft")){
	if(exists("en")){
	# Название картинки
	set title "FastTrack deterministic model (phase portrait)"
	# Настройка осей
	set ylabel "Seeders"
	set xlabel "New Nodes"
	key_text = "FastTrack p2p protocol"
	}
	if(exists("ru")){
	# Название картинки
	set title "Детерминированная модель Fasttrack (фазовый портрет)"
	# Настройка осей
	set ylabel "y(t) — раздающие узлы"
	set xlabel "x(t) — новые узлы"
	key_text = "p2p протокол Fasttrack"
	}

	if(exists("png")){
		set output sprintf("%s/ft_phase.png",lang)
	}
	else {
		set output sprintf("%s/ft_phase.pdf",lang)
	}
}
#				Стохастическая модель Fasttrack
if(exists("sft")){
	if(exists("en")){
	# Название картинки
	set title "FastTrack stochastic model (phase portrait)"
	# Настройка осей
	set ylabel "Seeders"
	set xlabel "New Nodes"
	key_text = "FastTrack p2p protocol"
	}
	if(exists("ru")){
	# Название картинки
	set title "Стохастическая модель Fasttrack (фазовый портрет)"
	# Настройка осей
	set ylabel "y(t) — раздающие узлы"
	set xlabel "x(t) — новые узлы"
	key_text = "p2p протокол Fasttrack"
	}
	if(exists("png")){
		set output sprintf("%s/ft_phase.png",lang)
	}
	else {
		set output sprintf("%s/sft_phase.pdf",lang)
	}
}
#-----------------------------------------------------------------------
# Рисуем
plot input_fname using 2:3 with lines linestyle 1 title key_text

##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#					Графики решений
##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if(exists("prepre")){
	# Название картинки
	set title "Детерминированная модель Хищник-Жертва (графики решений)"
	# Настройка осей
	set ylabel "Численность"
	set xlabel "t — время"
	# Легенды для двух графиков
	key_text_1 = "Жертвы x(t)"
	key_text_2 = "Хищники y(t)"
	if(exists("png")){
		set output "prepre_graph.png"
	}
	else {
		set output "prepre_graph.pdf"
	}
}
# 							Стохастическая модель хищник-жертва
if(exists("sprepre")){
	# Название картинки
	set title "Стохастическая модель Хищник-Жертва (графики решений)"
	# Настройка осей
	# Настройка осей
	set ylabel "Численность"
	set xlabel "t — время"
	# Легенды для двух графиков
	key_text_1 = "Жертвы x(t)"
	key_text_2 = "Хищники y(t)"
	if(exists("png")){
		set output "sprepre_graph.png"
	}
	else {
		set output "sprepre_graph.pdf"
	}
}
#			Детерминированная модель Fasttrack
if(exists("ft")){
	if(exists("en")){
	# Название картинки
	set title "FastTrack deterministic model (solutions)"
	set ylabel "Number of Nodes"
	set xlabel "t (Time)"
	# Легенды для двух графиков
	key_text_1 = "Seeders"
	key_text_2 = "New Nodes"
	}
	if(exists("ru")){
	# Название картинки
	set title "Детерминированная модель Fasttrack (решения)
	# Настройка осей
	set ylabel "Число узлов"
	set xlabel "t — время"
	# Легенды для двух графиков
	key_text_1 = "Раздающие узлы"
	key_text_2 = "Вновь подключившиеся"
	}

	if(exists("png")){
		set output sprintf("%s/ft_graph.png",lang)
	}
	else {
		set output sprintf("%s/ft_graph.pdf",lang)
	}
}
#				Стохастическая модель Fasttrack
if(exists("sft")){
	if(exists("en")){
	# Название картинки
	set title "FastTrack stochastic model (solutions)"
	set ylabel "Number of Nodes"
	set xlabel "t (Time)"
	# Легенды для двух графиков
	key_text_1 = "Seeders"
	key_text_2 = "New Nodes"
	}
	if(exists("ru")){
	# Название картинки
	set title "Стохастическая модель Fasttrack (решения)
	# Настройка осей
	set ylabel "Число узлов"
	set xlabel "t — время"
	# Легенды для двух графиков
	key_text_1 = "Раздающие узлы"
	key_text_2 = "Вновь подключившиеся"
	}
	if(exists("png")){
		set output sprintf("%s/sft_graph.png",lang)
	}
	else {
		set output sprintf("%s/sft_graph.pdf",lang)
	}
}
#-----------------------------------------------------------------------
# Рисуем
plot input_fname using 1:2 with lines linestyle 1 title key_text_1,  input_fname using 1:3 with lines linestyle 2 title key_text_2

