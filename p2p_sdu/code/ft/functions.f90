﻿module functions
contains
!------------------------------------------------------------------
! Детерминированная модель Хищник-Жертва
!------------------------------------------------------------------
	subroutine func(y, yp)
		implicit none
		real(kind = 8), intent(inout) :: y(*), yp(*)
		real(kind = 8) :: alpha, beta, gamma, delta
		namelist /prepre_param/ alpha, beta, gamma, delta
		
		open (11, file = 'params.dat', delim = 'apostrophe')
		read (11, nml = prepre_param)
		close (11)
		
		yp(1) = y(1)*(alpha - beta*y(2))
		yp(2) = -y(2)*(gamma - delta*y(1))
	end subroutine func
!--------------------------------------------------------------------
!			Правая часть для стохастического случая
!			модели Хищник-Жертва
!--------------------------------------------------------------------
	subroutine Afunc(y, yp)
		implicit none
		real(kind = 8), intent(inout) :: y(*), yp(*)
		real(kind = 8) :: alpha, beta, gamma, delta
		namelist /prepre_param/ alpha, beta, gamma, delta
		
		open (11, file = 'params.dat', delim = 'apostrophe')
		read (11, nml = prepre_param)
		close (11)
		
		yp(1) = y(1)*(alpha - beta*y(2))
		yp(2) = -y(2)*(gamma - delta*y(1))
	end subroutine Afunc
!!---------------------------------------------------------------------
	subroutine Bfunc(y, ys)
		implicit none
		real(kind = 8), intent(inout) :: y(*), ys(*)
		real(kind = 8) :: s1, s2
		namelist /prepre_sparam/ s1, s2
	
		open (11, file = 'params.dat', delim = 'apostrophe')
		read (11, nml = prepre_sparam)
		close (11)
	
		ys(1) = y(1)*s1
		ys(2) = y(2)*s2
	end subroutine Bfunc
!-----------------------------------------------------------------------
!			Модель Fasttrack
!-----------------------------------------------------------------------
	subroutine fasttrack(y, yp)
		implicit none
		real(kind = 8), intent(inout) :: y(*), yp(*)
		real(kind = 8) :: ft_lambda, ft_beta, ft_mu
		namelist /fasttrack_param/ ft_lambda, ft_beta, ft_mu
		
		open (11, file = 'params.dat', delim = 'apostrophe')
		read (11, nml = fasttrack_param)
		close (11)
		
		yp(1) = ft_lambda - ft_beta*y(1)*y(2)
		yp(2) = -y(2)*(ft_mu - ft_beta*y(1))
	end subroutine fasttrack
!--------------------------------------------------------------------
!			Правая часть для стохастического случая
!			модели fasttrack
!--------------------------------------------------------------------
	subroutine Afasttrack(y, yp)
		implicit none
		real(kind = 8), intent(inout) :: y(*), yp(*)
		real(kind = 8) :: ft_lambda, ft_beta, ft_mu
		namelist /fasttrack_param/ ft_lambda, ft_beta, ft_mu
		
		open (11, file = 'params.dat', delim = 'apostrophe')
		read (11, nml = fasttrack_param)
		close (11)
		
		yp(1) = ft_lambda - ft_beta*y(1)*y(2)
		yp(2) = -y(2)*(ft_mu - ft_beta*y(1))
	end subroutine Afasttrack
!!---------------------------------------------------------------------
	subroutine Bfasttrack(y, ys)
		implicit none
		real(kind = 8), intent(inout) :: y(*), ys(*)
		real(kind = 8) :: s1, s2
		namelist /prepre_sparam/ s1, s2
	
		open (11, file = 'params.dat', delim = 'apostrophe')
		read (11, nml = prepre_sparam)
		close (11)
	
		ys(1) = y(1)*s1
		ys(2) = y(2)*s2
	end subroutine Bfasttrack
end module functions