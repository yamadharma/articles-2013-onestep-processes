module sde_num_methods
	use random
	implicit none
	contains
		subroutine EulerMaruyamaD(Afunc, Bfunc, EQN, y, t, h)
			real(kind = 8), intent(in) :: h
			real(kind = 8) :: sqrth
		! Параметры системы уравнений (задачи)
			! Размерность системы
			integer, intent(in) :: EQN
		! Подпрограмма, задающая правые части системы (функции A(y) и B(y))
			external :: Afunc
			external :: Bfunc
		! Переменные
			! Счетчики
			integer :: i
			integer :: alpha
			! Время
			real(kind = 8), intent(inout) :: t
			! Функция Y
			real(kind = 8), dimension(1:EQN), intent(inout) :: y
			real(kind = 8), dimension(1:EQN) :: yp
			real(kind = 8), dimension(1:EQN) :: ys
			
			call Afunc(y, yp)
			call Bfunc(y, ys)
			sqrth = sqrt(h)
			do 	alpha = 1,EQN,1
				y(alpha) = y(alpha) + yp(alpha)*h + ys(alpha)*sqrth*random_normal()
			end do
			t = t + h
		end subroutine EulerMaruyamaD
!!================================================================================================		
		subroutine StRK2D(Afunc, Bfunc, EQN, y, t, h)
			real(kind = 8), intent(in) :: h
			real(kind = 8) :: sqrth
		! Параметры системы уравнений (задачи)
			! Размерность системы
			integer, intent(in) :: EQN
		! Подпрограмма, задающая правые части системы (функции A(y) и B(y))
			external :: Afunc
			external :: Bfunc
		! Переменные
			! Счетчики
			integer :: i
			integer :: alpha
			! Стадийность
			integer, parameter :: s = 2
			! Время
			real(kind = 8), intent(inout) :: t
			! Функция Y
			real(kind = 8), dimension(1:EQN), intent(inout) :: y
			! правые части
			real(kind = 8), dimension(1:EQN,1:s) :: Af
			real(kind = 8), dimension(1:EQN,1:s) :: Bf
			! Вспомогательная функция YY(1:EQN,1:s) =  Y(alpha,i)
			real(kind = 8), dimension(1:EQN,1:s) :: YY
			! Коэффициенты метода
			real(kind = 8), dimension(1:s,1:s) :: a, b
			real(kind = 8), dimension(1:s) :: aa, bb
			
			a(1,1) = 0.0
			a(1,2) = 0.0
			a(2,1) = 0.5
			a(2,2) = 0.0
			b(1,1) = 0.0
			b(1,2) = 0.0
			b(2,1) = 0.5
			b(2,2) = 0.0
			aa(1) = 0.0
			aa(2) = 1.0
			bb(1) = 0.0
			bb(2) = 1.0
			sqrth = sqrt(h)
			
			YY(1:EQN,1) = y(1:EQN)
			call Afunc(YY(1:EQN,1),Af(1:EQN,1))
			call Bfunc(YY(1:EQN,1),Bf(1:EQN,1))
			do alpha = 1,EQN,1
				YY(alpha,2) = y(alpha) + h*a(2,1)*Af(alpha,1) + sqrth*b(2,1)*Bf(alpha,1)*random_normal()
			end do
			call Afunc(YY(1:EQN,2),Af(1:EQN,2))
			call Bfunc(YY(1:EQN,2),Bf(1:EQN,2))
			do alpha = 1,EQN,1
				y(alpha) = y(alpha) + h*(aa(1)*Af(alpha,1) + aa(2)*Af(alpha,2)) + sqrth*random_normal()*(bb(1)*Bf(alpha,1) + bb(2)*Bf(alpha,2))
			end do
			t = t + h
		end subroutine StRK2D
!!================================================================================================		
		subroutine StRK3D(Afunc, Bfunc, EQN, y, t, h)
			real(kind = 8), intent(in) :: h
			real(kind = 8) :: sqrth
		! Параметры системы уравнений (задачи)
			! Размерность системы
			integer, intent(in) :: EQN
		! Подпрограмма, задающая правые части системы (функции A(y) и B(y))
			external :: Afunc
			external :: Bfunc
		! Переменные
			! Счетчики
			integer :: i
			integer :: alpha
			! Стадийность
			integer, parameter :: s = 3
			! Время
			real(kind = 8), intent(inout) :: t
			! Функция Y
			real(kind = 8), dimension(1:EQN), intent(inout) :: y
			! правые части
			real(kind = 8), dimension(1:EQN,1:s) :: Af
			real(kind = 8), dimension(1:EQN,1:s) :: Bf
			! Вспомогательная функция YY(1:EQN,1:s) =  Y(alpha,i)
			real(kind = 8), dimension(1:EQN,1:s) :: YY
			! Коэффициенты метода
			real(kind = 8), dimension(1:s,1:s) :: a, b
			real(kind = 8), dimension(1:s) :: aa, bb
			
			a(1,1) = 0.0
			a(1,2) = 0.0
			a(1,3) = 0.0
			a(2,1) = 2.0/3.0
			a(2,2) = 0.0
			a(2,3) = 0.0
			a(3,1) = -1.0
			a(3,2) = -1.0
			a(3,3) = 0.0
			b(1,1) = 0.0
			b(1,2) = 0.0
			b(1,3) = 0.0
			b(2,1) = 2.0/3.0
			b(2,2) = 0.0
			b(2,3) = 0.0
			b(3,1) = 0.0
			b(3,2) = -1.0
			b(3,3) = 1.0
			aa(1) = 0.0
			aa(2) = 3.0/4.0
			aa(3) = 1.0/4.0
			bb(1) =  0.0
			bb(2) = 3.0/4.0
			bb(3) = 1.0/4.0
			sqrth = sqrt(h)
			
			YY(1:EQN,1) = y(1:EQN)
			call Afunc(YY(1:EQN,1),Af(1:EQN,1))
			call Bfunc(YY(1:EQN,1),Bf(1:EQN,1))
			do alpha = 1,EQN,1
				YY(alpha,2) = y(alpha) + h*a(2,1)*Af(alpha,1) + sqrth*b(2,1)*Bf(alpha,1)*random_normal()
			end do
			call Afunc(YY(1:EQN,2),Af(1:EQN,2))
			call Bfunc(YY(1:EQN,2),Bf(1:EQN,2))
			do alpha = 1,EQN,1
				YY(alpha,3) = y(alpha) + h*a(3,1)*Af(alpha,1) + h*a(3,2)*Af(alpha,2) + &
				& sqrth*b(2,1)*Bf(alpha,1)*random_normal() + sqrth*b(2,2)*Bf(alpha,2)*random_normal()
			end do
			call Afunc(YY(1:EQN,3),Af(1:EQN,3))
			call Bfunc(YY(1:EQN,3),Bf(1:EQN,3))
			do alpha = 1,EQN,1
				y(alpha) = y(alpha) + h*(aa(1)*Af(alpha,1) + aa(2)*Af(alpha,2) + aa(3)*Af(alpha,3)) + &
				& sqrth*random_normal()*(bb(1)*Bf(alpha,1) + bb(2)*Bf(alpha,2) + bb(3)*Bf(alpha,3))
			end do
			t = t + h
		end subroutine StRK3D
		
		subroutine StRKp3_d(Afunc, Bfunc, EQN, y, t, t_start, t_stop, h)
			implicit none
			interface
					function Afunc(x)
						implicit none
						real(kind = 8), dimension(:), intent(in) :: x
						real(kind = 8), dimension(lbound(x,1):ubound(x,1)) :: Afunc
					end function Afunc
					function Bfunc(x)
						implicit none
						real(kind = 8), dimension(:), intent(in) :: x
						real(kind = 8), dimension(lbound(x,1):ubound(x,1),lbound(x,1):ubound(x,1)) :: Bfunc
					end function Bfunc
			end interface
			integer, intent(in) :: EQN
			real(kind = 8), intent(inout), dimension(1:EQN) :: y
			real(kind = 8), intent(inout) :: t
			real(kind = 8), intent(in) :: t_start, t_stop
			real(kind = 8), intent(in) :: h
			!===
			! Стадийность
			integer, parameter :: s = 3
			real(kind = 8) :: h_sqrt
			! Коэффициенты метода
			real(kind = 8), dimension(1:s,1:s) :: a, b
			real(kind = 8), dimension(1:s) :: aa, bb
			! правые части
			real(kind = 8), dimension(1:EQN,1:s) :: Af
			real(kind = 8), dimension(1:EQN,1:EQN,1:s) :: Bf
			! Вспомогательная функция K(1:EQN,1:s) =  K(alpha,i)
			real(kind = 8), dimension(1:EQN,1:s) :: K
			! нормально распределенная случайная величина (массив)
			real(kind = 8), dimension(1:EQN) :: J
			! Счетчики
			integer :: alpha, beta, i
			
			
			h_sqrt = sqrt(h)
			a(1,1:s) = [ 0.0d0, 0.0d0, 0.0d0 ]
			a(2,1:s) = [ 2.0d0/3.0d0, 0.0d0, 0.0d0 ]
			a(3,1:s) = [ -1.0d0, -1.0d0, 0.0d0 ]
			b(1,1:s) = [ 0.0d0, 0.0d0, 0.0d0 ]
			b(2,1:s) = [ 2.0d0/3.0d0, 0.0d0, 0.0d0 ]
			b(3,1:s) = [ 0.0d0, -1.0d0, 1.0d0 ]
			aa(1:s) = [ 0.0d0, 3.0d0/4.0d0, 1.0d0/4.0d0 ]
			bb(1:s) = [ 0.0d0, 3.0d0/4.0d0, 1.0d0/4.0d0 ]

			t = t_start
			do while(t <= t_stop)
				write(*,*) t, y
				K(1:EQN,1) = y(1:EQN)
				
				Af(1:EQN,1) = Afunc(K(1:EQN,1))
				Bf(1:EQN,1:EQN,1) = Bfunc(K(1:EQN,1))
				
				do beta = 1,EQN,1 
					J(beta) = random_normal()
				end do
				
				do alpha = 1,EQN,1
					K(alpha,2) = y(alpha) + h*a(2,1)*Af(alpha,1) + h_sqrt*b(2,1)*dot_product(Bf(alpha,1:EQN,1), J(1:EQN))
				end do
				
				Af(1:EQN,2) = Afunc(K(1:EQN,2))
				Bf(1:EQN,1:EQN,2) = Bfunc(K(1:EQN,2))
				
				do beta = 1,EQN,1 
					J(beta) = random_normal()
				end do
				
				do alpha = 1,EQN,1
					K(alpha,3) = y(alpha) + h*a(3,1)*Af(alpha,1) + h*a(3,2)*Af(alpha,2) + &
					& h_sqrt*b(2,1)*dot_product(Bf(alpha,1:EQN,1), J(1:EQN)) + &
					& h_sqrt*b(2,2)*dot_product(Bf(alpha,1:EQN,2), J(1:EQN))
				end do
				
				Af(1:EQN,3) = Afunc(K(1:EQN,3))
				Bf(1:EQN,1:EQN,3) = Bfunc(K(1:EQN,3))
				
				do beta = 1,EQN,1 
					J(beta) = random_normal()
				end do
				
				do alpha = 1,EQN,1
					y(alpha) = y(alpha) + h*(aa(1)*Af(alpha,1) + aa(2)*Af(alpha,2) + aa(3)*Af(alpha,3)) + &
					& h_sqrt*bb(1)*dot_product(Bf(alpha,1:EQN,1), J(1:EQN)) + &
					& h_sqrt*bb(2)*dot_product(Bf(alpha,1:EQN,2), J(1:EQN)) + &
					& h_sqrt*bb(3)*dot_product(Bf(alpha,1:EQN,3), J(1:EQN))
				end do
				t = t + h
			end do 
		end subroutine StRKp3_d
		
end module sde_num_methods
