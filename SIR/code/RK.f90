module RK
implicit none
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!!								Методы Рунге-Кутта
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! Подпрограмма RK(func, EQN, y, t, h, order) реализует итерацию метода Рунге-Кутта
! 	func --- функция, задающая правую часть системы диф. уравнений
! 	dy/dt = func(y)
! 	integer :: EQN --- порядок системы
! 	real :: y --- сеточная функция
! 	real :: t --- текущий момент времени
! 	real :: h --- шаг сетки
! 	character(len = 32) :: method_name --- название метода (необязательный аргумент)
contains
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!!		Процедура задания коэффициентов одинарной точности
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 subroutine RK_Tab_Init_S(method_name, method_order, method_stage, a, b, c, k, EQN)
	character(len = *), intent(in) :: method_name
	integer, intent(inout) :: method_order, method_stage
	real, allocatable, dimension(:,:), intent(inout) :: a
	real, allocatable, dimension(:), intent(inout) :: b
	real, allocatable, dimension(:), intent(inout) :: c
	real, allocatable, dimension(:,:), intent(inout) :: k
	! Число уравнений
	integer :: EQN
	select case (trim(method_name))
	!---------------------------------------------------------------------------------------------------
			case("RKp2n1")
				method_stage = 2
				method_order = 2
				allocate(a(1:method_stage,1:method_stage), b(1:method_stage), c(1:method_stage), k(1:EQN,1:method_stage))
				a(1,1:2) = [ 0.0, 0.0 ]
				a(2,1:2) = [ 1.0, 0.0 ]
				b(1:2) = [ 0.5, 0.5 ]
				c(1:2) = [ 0.0, 1.0 ]
	!---------------------------------------------------------------------------------------------------
			case("RKp2n2")
				method_stage = 2
				method_order = 2
				allocate(a(1:method_stage,1:method_stage), b(1:method_stage), c(1:method_stage), k(1:EQN,1:method_stage))
				a(1,1:2) = [ 0.0, 0.0 ]
				a(2,1:2) = [ 1.0, 0.0 ]
				b(1:2) = [ 0.5, 0.5 ]
	!---------------------------------------------------------------------------------------------------
			case("RKp3n1")
			! "Метод Рунге--Кутты 3-го порядка I"
				method_stage = 3
				method_order = 3
				allocate(a(1:method_stage,1:method_stage), b(1:method_stage), c(1:method_stage), k(1:EQN,1:method_stage))
				a(1,1:3) = [ 0.0, 0.0, 0.0 ]
				a(2,1:3) = [ 2.0/3.0, 0.0, 0.0 ]
				a(3,1:3) = [ 1.0/3.0, 1.0/3.0, 0.0 ]
				b(1:3) = [ 0.25, 0.0, 0.75 ]
	!---------------------------------------------------------------------------------------------------		
			case("RKp3n2")
			! "Метод Рунге--Кутты 3-го порядка II"
				method_stage = 3
				method_order = 3
				allocate(a(1:method_stage,1:method_stage), b(1:method_stage), c(1:method_stage), k(1:EQN,1:method_stage))
				a(1,1:3) = [  0.0, 0.0, 0.0 ]
				a(2,1:3) = [  0.5, 0.0, 0.0 ]
				a(3,1:3) = [ -1.0, 2.0, 0.0 ]
				b(1:3) = [ 1.0/6.0, 4.0/6.0, 1.0/6.0 ]
				c(1:3) = [ 0.0, 1.0/3.0, 2.0/3.0 ]
	!---------------------------------------------------------------------------------------------------		
			case("RKp3n3")
			! "Метод Рунге--Кутты 3-го порядка III (Хойна)"
				method_stage = 3
				method_order = 3
				allocate(a(1:method_stage,1:method_stage), b(1:method_stage), c(1:method_stage), k(1:EQN,1:method_stage))
				a(1,1:3) = [ 0.0, 0.0, 0.0 ]
				a(2,1:3) = [ 1.0/3.0, 0.0, 0.0 ]
				a(3,1:3) = [ 0.0, 2.0/3.0, 0.0 ]
				b(1:3) = [ 0.25, 0.0, 0.75 ]
				c(1:3) = [ 0.0, 1.0/3.0, 2.0/3.0 ]
	!---------------------------------------------------------------------------------------------------
			case("RKp4n1")
			! "Метод Рунге--Кутты 4-ого порядка I"
				method_stage = 4
				method_order = 4
				allocate(a(1:method_stage,1:method_stage), b(1:method_stage), c(1:method_stage), k(1:EQN,1:method_stage))
				a(1,1:4) = [ 0.0, 0.0, 0.0, 0.0 ]
				a(2,1:4) = [ 1.0/2.0, 0.0, 0.0, 0.0 ]
				a(3,1:4) = [ 0.0, 1.0/2.0, 0.0, 0.0 ]
				a(4,1:4) = [ 0.0, 0.0, 1.0, 0.0 ]
				b(1:4) = [ 1.0/6.0, 1.0/3.0, 1.0/3.0, 1.0/6.0 ]
				c(1:4) = [ 0.0, 1.0/2.0, 1.0/2.0, 1.0 ]
	!---------------------------------------------------------------------------------------------------
			case("RKp4n2")
			! "Метод Рунге--Кутты 4-ого порядка II"
				method_stage = 4
				method_order = 4
				allocate(a(1:method_stage,1:method_stage), b(1:method_stage), c(1:method_stage), k(1:EQN,1:method_stage))
				a(1,1:4) = [ 0.0, 0.0, 0.0, 0.0 ]
				a(2,1:4) = [ 1.0/4.0, 0.0, 0.0, 0.0 ]
				a(3,1:4) = [ 0.0, 1.0/2.0, 0.0, 0.0 ]
				a(4,1:4) = [ 1.0, -2.0, 2.0, 0.0 ]
				b(1:4) = [ 1.0/6.0, 0.0, 2.0/3.0, 1.0/6.0 ]
				c(1:4) = [ 0.0, 0.25, 0.5, 1.0 ]
	!---------------------------------------------------------------------------------------------------
			case("RKp4n3")
			! "Метод Рунге--Кутты 4-ого порядка III (правило 3/8)"
				method_stage = 4
				method_order = 4
				allocate(a(1:method_stage,1:method_stage), b(1:method_stage), c(1:method_stage), k(1:EQN,1:method_stage))
				a(1,1:4) = [  0.0, 0.0, 0.0, 0.0 ]
				a(2,1:4) = [  1.0/3.0, 0.0, 0.0, 0.0 ]
				a(3,1:4) = [ -1.0/3.0, 1.0, 0.0, 0.0 ]
				a(4,1:4) = [  1.0, -1.0, 1.0, 0.0 ]
				b(1:4) = [ 1.0/8.0, 3.0/8.0, 3.0/8.0, 1.0/8.0 ]
				c(1:4) = [ 0.0, 1.0/3.0, 2.0/3.0, 1.0 ]
	!---------------------------------------------------------------------------------------------------
			case("RKp5n1")
			! Метод Рунге-Кутты 5-го порядка (Кутта(1901)-Нюстрем(1925))
				method_stage = 6
				method_order = 5
				allocate(a(1:method_stage,1:method_stage), b(1:method_stage), c(1:method_stage), k(1:EQN,1:method_stage))
				a(1,1:6) = [ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 ]
				a(2,1:6) = [ 1.0/3.0, 0.0, 0.0, 0.0, 0.0, 0.0 ]
				a(3,1:6) = [ 4.0/25.0, 6.0/25.0, 0.0, 0.0, 0.0, 0.0 ]
				a(4,1:6) = [ 1.0/4.0, -3.0, 15.0/4.0, 0.0, 0.0, 0.0 ]
				a(5,1:6) = [ 6.0/81.0, 90.0/81.0, -50.0/81.0, 8.0/81.0, 0.0, 0.0 ]
				a(6,1:6) = [ 6.0/75.0, 36.0/75.0, 10.0/75.0, 8.0/75.0, 0.0, 0.0 ]
				b(1:6) = [ 23.0/192.0, 0.0, 125.0/192.0, 0.0, -81.0/192.0, 125.0/192.0 ]
				c(1:6) = [ 0.0, 1.0/3.0, 2.0/5.0, 1.0, 2.0/3.0, 4.0/5.0 ]
	!---------------------------------------------------------------------------------------------------
			case("RKp6n1")
			! "Метод Рунге--Кутты 6-го порядка (Батчер)"
				method_stage = 7
				method_order = 6
				allocate(a(1:method_stage,1:method_stage), b(1:method_stage), c(1:method_stage), k(1:EQN,1:method_stage))
				a(1,1:7) = [ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 ]
				a(2,1:7) = [ 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 ]
				a(3,1:7) = [ 2.0/9.0, 4.0/9.0, 0.0, 0.0, 0.0, 0.0, 0.0 ]
				a(4,1:7) = [ 7.0/36.0, 2.0/9.0, -1.0/12.0, 0.0, 0.0, 0.0, 0.0 ]
				a(5,1:7) = [ -35.0/144.0, -55.0/36.0, 35.0/48.0, 15.0/8.0, 0.0, 0.0, 0.0 ]
				a(6,1:7) = [ -1.0/360.0, -11.0/36.0, -1.0/8.0, 0.5, 0.1, 0.0, 0.0 ]
				a(7,1:7) = [ -41.0/260.0, 22.0/13.0, 43.0/156.0, -118.0/39.0, 32.0/195.0, 80.0/39.0, 0.0 ]
				b(1:7) = [ 13.0/200.0, 0.0, 11.0/40.0, 11.0/40.0, 4.0/25.0, 4.0/25.0, 13.0/200.0 ]
				c(1:7) = [ 0.0, 0.5, 2.0/3.0, 1.0/3.0, 5.0/6.0, 1.0/6.0, 1.0 ]
	!---------------------------------------------------------------------------------------------------
			case default
			! "Метод Рунге--Кутты 4-ого порядка III (правило 3/8)"
				method_stage = 4
				method_order = 4
				allocate(a(1:method_stage,1:method_stage), b(1:method_stage), c(1:method_stage), k(1:EQN,1:method_stage))
				a(1,1:4) = [  0.0, 0.0, 0.0, 0.0 ]
				a(2,1:4) = [  1.0/3.0, 0.0, 0.0, 0.0 ]
				a(3,1:4) = [ -1.0/3.0, 1.0, 0.0, 0.0 ]
				a(4,1:4) = [  1.0, -1.0, 1.0, 0.0 ]
				b(1:4) = [ 1.0/8.0, 3.0/8.0, 3.0/8.0, 1.0/8.0 ]
				c(1:4) = [ 0.0, 1.0/3.0, 2.0/3.0, 1.0 ]
		end select
 end subroutine RK_Tab_Init_S
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!!		Процедура задания коэффициентов двойной точности
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 subroutine RK_Tab_Init_D(method_name, method_order, method_stage, a, b, c, k, EQN)
	character(len = *), intent(in) :: method_name
	integer, intent(inout) :: method_order, method_stage
	real(kind = 8), allocatable, dimension(:,:), intent(inout) :: a
	real(kind = 8), allocatable, dimension(:), intent(inout) :: b
	real(kind = 8), allocatable, dimension(:), intent(inout) :: c
	real(kind = 8), allocatable, dimension(:,:), intent(inout) :: k
	! Число уравнений
	integer :: EQN
	select case (trim(method_name))
	!---------------------------------------------------------------------------------------------------
			case("RKp2n1")
				method_stage = 2
				method_order = 2
				allocate(a(1:method_stage,1:method_stage), b(1:method_stage), c(1:method_stage), k(1:EQN,1:method_stage))
				a(1,1:2) = [ 0.0D0, 0.0D0 ]
				a(2,1:2) = [ 1.0D0, 0.0D0 ]
				b(1:2) = [ 0.5D0, 0.5D0 ]
				c(1:2) = [ 0.0D0, 1.0D0 ]
	!---------------------------------------------------------------------------------------------------
			case("RKp2n2")
				method_stage = 2
				method_order = 2
				allocate(a(1:method_stage,1:method_stage), b(1:method_stage), c(1:method_stage), k(1:EQN,1:method_stage))
				a(1,1:2) = [ 0.0D0, 0.0D0 ]
				a(2,1:2) = [ 1.0D0, 0.0D0 ]
				b(1:2) = [ 0.5, 0.5 ]
	!---------------------------------------------------------------------------------------------------
			case("RKp3n1")
			! "Метод Рунге--Кутты 3-го порядка I"
				method_stage = 3
				method_order = 3
				allocate(a(1:method_stage,1:method_stage), b(1:method_stage), c(1:method_stage), k(1:EQN,1:method_stage))
				a(1,1:3) = [ 0.0D0, 0.0D0, 0.D0]
				a(2,1:3) = [ 2.0D0/3.0D0, 0.0D0, 0.0D0 ]
				a(3,1:3) = [ 1.0D0/3.0D0, 1.0D0/3.0D0, 0.0D0 ]
				b(1:3) = [ 0.25D0, 0.0D0, 0.75D0 ]
	!---------------------------------------------------------------------------------------------------
			case("RKp3n2")
			! "Метод Рунге--Кутты 3-го порядка II"
				method_stage = 3
				method_order = 3
				allocate(a(1:method_stage,1:method_stage), b(1:method_stage), c(1:method_stage), k(1:EQN,1:method_stage))
				a(1,1:3) = [  0.0D0, 0.0D0, 0.0D0 ]
				a(2,1:3) = [  0.5D0, 0.0D0, 0.0D0 ]
				a(3,1:3) = [ -1.0D0, 2.0D0, 0.0D0 ]
				b(1:3) = [ 1.0D0/6.0D0, 4.0D0/6.0D0, 1.0D0/6.0D0 ]
				c(1:3) = [ 0.0D0, 1.0D0/3.0D0, 2.0D0/3.0D0 ]
	!---------------------------------------------------------------------------------------------------
			case("RKp3n3")
			! "Метод Рунге--Кутты 3-го порядка III (Хойна)"
				method_stage = 3
				method_order = 3
				allocate(a(1:method_stage,1:method_stage), b(1:method_stage), c(1:method_stage), k(1:EQN,1:method_stage))
				a(1,1:3) = [ 0.0D0, 0.0D0, 0.0D0 ]
				a(2,1:3) = [ 1.0D0/3.0D0, 0.0D0, 0.0D0 ]
				a(3,1:3) = [ 0.0D0, 2.0D0/3.0D0, 0.0D0 ]
				b(1:3) = [ 0.25D0, 0.0D0, 0.75D0 ]
				c(1:3) = [ 0.0D0, 1.0D0/3.0D0, 2.0/3.0D0 ]
	!---------------------------------------------------------------------------------------------------
			case("RKp4n1")
			! "Метод Рунге--Кутты 4-ого порядка I"
				method_stage = 4
				method_order = 4
				allocate(a(1:method_stage,1:method_stage), b(1:method_stage), c(1:method_stage), k(1:EQN,1:method_stage))
				a(1,1:4) = [ 0.0D0, 0.0D0, 0.0D0, 0.0D0 ]
				a(2,1:4) = [ 1.0D0/2.0D0, 0.0D0, 0.0D0, 0.0D0 ]
				a(3,1:4) = [ 0.0D0, 1.0D0/2.0D0, 0.0D0, 0.0D0 ]
				a(4,1:4) = [ 0.0D0, 0.0D0, 1.0D0, 0.0D0 ]
				b(1:4) = [ 1.0D0/6.0D0, 1.0D0/3.0D0, 1.0D0/3.0D0, 1.0D0/6.0D0 ]
				c(1:4) = [ 0.0D0, 1.0D0/2.0D0, 1.0D0/2.0D0, 1.0D0 ]
	!---------------------------------------------------------------------------------------------------
			case("RKp4n2")
			! "Метод Рунге--Кутты 4-ого порядка II"
				method_stage = 4
				method_order = 4
				allocate(a(1:method_stage,1:method_stage), b(1:method_stage), c(1:method_stage), k(1:EQN,1:method_stage))
				a(1,1:4) = [ 0.0D0, 0.0D0, 0.0D0, 0.0D0 ]
				a(2,1:4) = [ 1.0/4.0D0, 0.0D0, 0.0D0, 0.0D0 ]
				a(3,1:4) = [ 0.0D0, 1.0/2.0D0, 0.0D0, 0.0D0 ]
				a(4,1:4) = [ 1.0D0, -2.0D0, 2.0D0, 0.0D0 ]
				b(1:4) = [ 1.0D0/6.0D0, 0.0D0, 2.0D0/3.0D0, 1.0D0/6.0D0 ]
				c(1:4) = [ 0.0D0, 0.25D0, 0.5D0, 1.0D0 ]
	!---------------------------------------------------------------------------------------------------
			case("RKp4n3")
			! "Метод Рунге--Кутты 4-ого порядка III (правило 3/8)"
				method_stage = 4
				method_order = 4
				allocate(a(1:method_stage,1:method_stage), b(1:method_stage), c(1:method_stage), k(1:EQN,1:method_stage))
				a(1,1:4) = [  0.0D0, 0.0D0, 0.0D0, 0.0D0 ]
				a(2,1:4) = [  1.0D0/3.0D0, 0.0D0, 0.0D0, 0.0D0 ]
				a(3,1:4) = [ -1.0D0/3.0D0, 1.0D0, 0.0D0, 0.0D0 ]
				a(4,1:4) = [  1.0D0, -1.0D0, 1.0D0, 0.0D0 ]
				b(1:4) = [ 1.0D0/8.0D0, 3.0/8.0D0, 3.0D0/8.0D0, 1.0D0/8.0D0 ]
				c(1:4) = [ 0.0D0, 1.0D0/3.0D0, 2.0D0/3.0D0, 1.0D0 ]
	!---------------------------------------------------------------------------------------------------
			case("RKp5n1")
			! Метод Рунге-Кутты 5-го порядка (Кутта(1901)-Нюстрем(1925))
				method_stage = 6
				method_order = 5
				allocate(a(1:method_stage,1:method_stage), b(1:method_stage), c(1:method_stage), k(1:EQN,1:method_stage))
				a(1,1:6) = [ 0.0D0, 0.0D0, 0.0D0, 0.0D0, 0.0D0, 0.0D0 ]
				a(2,1:6) = [ 1.0D0/3.0D0, 0.0D0, 0.0D0, 0.0D0, 0.0D0, 0.0D0 ]
				a(3,1:6) = [ 4.0D0/25.0D0, 6.0D0/25.0D0, 0.0D0, 0.0D0, 0.0D0, 0.0D0 ]
				a(4,1:6) = [ 1.0D0/4.0D0, -3.0D0, 15.0D0/4.0D0, 0.0D0, 0.0D0, 0.0D0 ]
				a(5,1:6) = [ 6.0D0/81.0D0, 90.0D0/81.0D0, -50.0D0/81.0D0, 8.0D0/81.0D0, 0.0D0, 0.0D0 ]
				a(6,1:6) = [ 6.0D0/75.0D0, 36.0D0/75.0D0, 10.0D0/75.0D0, 8.0D0/75.0D0, 0.0D0, 0.0D0 ]
				b(1:6) = [ 23.0D0/192.0D0, 0.0D0, 125.0D0/192.0D0, 0.0D0, -81.0D0/192.0D0, 125.0D0/192.0D0 ]
				c(1:6) = [ 0.0D0, 1.0D0/3.0D0, 2.0D0/5.0D0, 1.0D0, 2.0D0/3.0D0, 4.0D0/5.0D0 ]
	!---------------------------------------------------------------------------------------------------
			case("RKp6n1")
			! "Метод Рунге--Кутты 6-го порядка (Батчер)"
				method_stage = 7
				method_order = 6
				allocate(a(1:method_stage,1:method_stage), b(1:method_stage), c(1:method_stage), k(1:EQN,1:method_stage))
				a(1,1:7) = [ 0.0D0, 0.0D0, 0.0D0, 0.0D0, 0.0D0, 0.0D0, 0.0D0 ]
				a(2,1:7) = [ 0.5D0, 0.0D0, 0.0D0, 0.0D0, 0.0D0, 0.0D0, 0.0D0 ]
				a(3,1:7) = [ 2.0D0/9.0D0, 4.0/9.0D0, 0.0D0, 0.0D0, 0.0D0, 0.0D0, 0.0D0 ]
				a(4,1:7) = [ 7.0D0/36.0D0, 2.0D0/9.0D0, -1.0D0/12.0D0, 0.0D0, 0.0D0, 0.0D0, 0.0D0 ]
				a(5,1:7) = [ -35.0D0/144.0D0, -55.0D0/36.0D0, 35.0D0/48.0D0, 15.0D0/8.0D0, 0.0D0, 0.0D0, 0.0D0 ]
				a(6,1:7) = [ -1.0D0/360.0D0, -11.0D0/36.0D0, -1.0/8.0D0, 0.5D0, 0.1D0, 0.0D0, 0.0D0 ]
				a(7,1:7) = [ -41.0D0/260.0D0, 22.0D0/13.0D0, 43.0D0/156.0D0, -118.0D0/39.0D0, 32.0D0/195.0D0, 80.0D0/39.0D0, 0.0D0 ]
				b(1:7) = [ 13.0D0/200.0D0, 0.0D0, 11.0D0/40.0D0, 11.0D0/40.0D0, 4.0D0/25.0D0, 4.0D0/25.0D0, 13.0D0/200.0D0 ]
				c(1:7) = [ 0.0D0, 0.5D0, 2.0D0/3.0D0, 1.0D0/3.0D0, 5.0D0/6.0D0, 1.0D0/6.0D0, 1.0D0 ]
	!---------------------------------------------------------------------------------------------------
			case default
			! "Метод Рунге--Кутты 4-ого порядка III (правило 3/8)"
				method_stage = 4
				method_order = 4
				allocate(a(1:method_stage,1:method_stage), b(1:method_stage), c(1:method_stage), k(1:EQN,1:method_stage))
				a(1,1:4) = [  0.0D0, 0.0D0, 0.0D0, 0.0D0 ]
				a(2,1:4) = [  1.0D0/3.0D0, 0.0D0, 0.0D0, 0.0D0 ]
				a(3,1:4) = [ -1.0D0/3.0D0, 1.0D0, 0.0D0, 0.0D0 ]
				a(4,1:4) = [  1.0D0, -1.0D0, 1.0D0, 0.0D0 ]
				b(1:4) = [ 1.0D0/8.0D0, 3.0D0/8.0D0, 3.0D0/8.0D0, 1.0D0/8.0D0 ]
				c(1:4) = [ 0.0D0, 1.0D0/3.0D0, 2.0D0/3.0D0, 1.0D0 ]
		end select
 end subroutine RK_Tab_Init_D
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!!								Метод одинарной точности (одна итерация)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	subroutine RK_It_S(func, EQN, y, t, h, method_name)
	implicit none
	! Аргументы процедуры	
		! Функция, задающая правую часть системы ОДУ
		interface
			function func(x)
				real, dimension(:), intent(in) :: x
				real, dimension(1:ubound(x,1)) :: func
			end function func
		end interface
		! Размерность системы
		integer, intent(in) :: EQN	
		! Функция Y (сеточная функция)
		real, dimension(1:EQN), intent(inout) :: y
		! Время
		real, intent(inout) :: t
		! Шаг метода
		real, intent(in) :: h
		! Название метода (необязательный аргумент)
		character(len = *), optional, intent(in) :: method_name
	! Параметры метода
		! Стадийность метода
		integer :: s
		! Порядок метода
		integer :: order
		! Название метода (для внутреннего использования)
		character(len = 32) :: m_name
	! Переменные
		! Счетчики
		integer :: i, alpha
		! Вспомогательная функция
		real, dimension(1:EQN) :: g
	! Коэффициенты метода Рунге-Кутта
		real, allocatable, dimension(:,:) :: a
		real, allocatable, dimension(:) :: b
		real, allocatable, dimension(:) :: c
		real, allocatable, dimension(:,:) :: k
	! ------------------------------------
	! Задание коэффициентов в зависимости от метода
	! По умолчанию используется метод 4-ого порядка
	if (present(method_name)) then
		m_name = trim(method_name)
	else
		m_name = "RKp4n3"
	end if
	call RK_Tab_Init_S(m_name, order, s, a, b, c, k, EQN)
	k = 0.0
	! ------------------------------------
	! ======= Метод Рунге-Кутта =======
		do i = 1,s,1
			do 	alpha = 1,EQN,1
				g(alpha) = y(alpha) + h*dot_product(k(alpha,1:s),a(i,1:s))
			end do
			!call func(g,k(1:EQN,i))
			k(1:EQN,i) = func(g)
		end do
		do 	alpha = 1,EQN,1
			y(alpha) = y(alpha) + h*dot_product(b,k(alpha,:))
		end do
		t = t + h
	! ==== ------------------ =======
	deallocate(a,b,c,k)
	end subroutine RK_It_S
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!!											Метод двойной точности (одна итерация)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	subroutine RK_It_D(func, EQN, y, t, h, method_name)
	implicit none
	! Аргументы процедуры	
		! Функция, задающая правую часть системы ОДУ
		interface
			function func(x)
				real(kind = 8), dimension(:), intent(in) :: x
				real(kind = 8), dimension(1:ubound(x,1)) :: func
			end function func
		end interface
		! Размерность системы
		integer, intent(in) :: EQN	
		! Функция Y (сеточная функция)
		real(kind = 8), dimension(1:EQN), intent(inout) :: y
		! Время
		real(kind = 8), intent(inout) :: t
		! Шаг метода
		real(kind = 8), intent(in) :: h
		! Название метода (необязательный аргумент)
		character(len = *), optional, intent(in) :: method_name
	! Параметры метода
		! Стадийность метода
		integer :: s
		! Порядок метода
		integer :: order
		! Название метода (для внутреннего использования)
		character(len = 32) :: m_name
	! Переменные
		! Счетчики
		integer :: i, alpha
		! Вспомогательная функция
		real(kind = 8), dimension(1:EQN) :: g
	! Коэффициенты метода Рунге-Кутта
		real(kind = 8), allocatable, dimension(:,:) :: a
		real(kind = 8), allocatable, dimension(:) :: b
		real(kind = 8), allocatable, dimension(:) :: c
		real(kind = 8), allocatable, dimension(:,:) :: k
	! ------------------------------------
	! Задание коэффициентов в зависимости от метода
	! По умолчанию используется метод 4-ого порядка
	if (present(method_name)) then
		m_name = trim(method_name)
	else
		m_name = "RKp4n3"
	end if
	call RK_Tab_Init_D(m_name, order, s, a, b, c, k, EQN)
	k = 0.0
	! ======= Метод Рунге-Кутта =======
		do i = 1,s,1
			do 	alpha = 1,EQN,1
				g(alpha) = y(alpha) + h*dot_product(k(alpha,1:s),a(i,1:s))
			end do
			!call func(g,k(1:EQN,i))
			k(1:EQN,i) = func(g)
		end do
		do 	alpha = 1,EQN,1
			y(alpha) = y(alpha) + h*dot_product(b,k(alpha,:))
		end do
		t = t + h
	! ==== ------------------ =======
		deallocate(a,b,c,k)
	end subroutine RK_It_D
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!!								Метод одинарной точности (полный цикл)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	subroutine RK_S(func, EQN, y, t, t_start, t_stop, h, print_calc, method_name)
	implicit none
	! Аргументы процедуры	
		! Функция, задающая правую часть системы ОДУ
		interface
			function func(x)
				real, dimension(:), intent(in) :: x
				real, dimension(1:ubound(x,1)) :: func
			end function func
		end interface
		! Размерность системы
		integer, intent(in) :: EQN	
		! Функция Y (сеточная функция)
		real, dimension(1:EQN), intent(inout) :: y
		! Время
		real, intent(inout) :: t
		! Временной промежуток численного интегрирования
		real, intent(in) :: t_start, t_stop
		! Шаг метода
		real, intent(in) :: h
		! Распечатывать ли вычисления в консоль
		logical, intent(in) :: print_calc
		! Название метода (необязательный аргумент)
		character(len = *), optional, intent(in) :: method_name
	! Параметры метода
		! Стадийность метода
		integer :: s
		! Порядок метода
		integer :: order
		! Название метода (для внутреннего использования)
		character(len = 32) :: m_name
	! Переменные
		! Счетчики
		integer :: i, alpha
		! Вспомогательная функция
		real, dimension(1:EQN) :: g
	! Коэффициенты метода Рунге-Кутта
		real, allocatable, dimension(:,:) :: a
		real, allocatable, dimension(:) :: b
		real, allocatable, dimension(:) :: c
		real, allocatable, dimension(:,:) :: k
	! ------------------------------------
	! Задание коэффициентов в зависимости от метода
	! По умолчанию используется метод 4-ого порядка
	if (present(method_name)) then
		m_name = trim(method_name)
	else
		m_name = "RKp4n3"
	end if
	call RK_Tab_Init_S(m_name, order, s, a, b, c, k, EQN)
	! Проверяем условие печатать/не печатать результаты вне цикла, потому что так экономней
	! не надо проверять для каждого шага это условие
	! ======= Метод Рунге-Кутта (полный цикл c распечаткой) =======
		if(print_calc) then
			do while(t <= t_stop)
				k = 0.0
				write(*,*) t, y ! Распечатка
				
				do i = 1,s,1
					do 	alpha = 1,EQN,1
						g(alpha) = y(alpha) + h*dot_product(k(alpha,1:s),a(i,1:s))
					end do
					!call func(g,k(1:EQN,i))
					k(1:EQN,i) = func(g)
				end do
				do 	alpha = 1,EQN,1
					y(alpha) = y(alpha) + h*dot_product(b,k(alpha,:))
				end do
				t = t + h
				
			end do
		else
		! ======= Метод Рунге-Кутта (полный цикл без распечатки) =======
			do while(t <= t_stop)
				k = 0.0
				do i = 1,s,1
					do 	alpha = 1,EQN,1
						g(alpha) = y(alpha) + h*dot_product(k(alpha,1:s),a(i,1:s))
					end do
					!call func(g,k(1:EQN,i))
					k(1:EQN,i) = func(g)
				end do
				do 	alpha = 1,EQN,1
					y(alpha) = y(alpha) + h*dot_product(b,k(alpha,:))
				end do
				t = t + h
			end do
		end if
	! ==== ------------------ =======
		deallocate(a,b,c,k)
	end subroutine RK_S
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!!											Метод двойной точности (полный цикл)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	subroutine RK_D(func, EQN, y, t, t_start, t_stop, h, print_calc, method_name)
	implicit none
	! Аргументы процедуры	
		! Функция, задающая правую часть системы ОДУ
		interface
			function func(x)
				real(kind = 8), dimension(:), intent(in) :: x
				real(kind = 8), dimension(1:ubound(x,1)) :: func
			end function func
		end interface
		! Размерность системы
		integer, intent(in) :: EQN	
		! Функция Y (сеточная функция)
		real(kind = 8), dimension(1:EQN), intent(inout) :: y
		! Время
		real(kind = 8), intent(inout) :: t
		! Временной промежуток численного интегрирования
		real(kind = 8), intent(in) :: t_start, t_stop
		! Шаг метода
		real(kind = 8), intent(in) :: h
		! Распечатывать ли ход вычисления в консоль
		logical, intent(in) :: print_calc
		! Название метода (необязательный аргумент)
		character(len = *), optional, intent(in) :: method_name
	! Параметры метода
		! Стадийность метода
		integer :: s
		! Порядок метода
		integer :: order
		! Название метода (для внутреннего использования)
		character(len = 32) :: m_name
	! Переменные
		! Счетчики
		integer :: i, alpha
		! Вспомогательная функция
		real(kind = 8), dimension(1:EQN) :: g
	! Коэффициенты метода Рунге-Кутта
		real(kind = 8), allocatable, dimension(:,:) :: a
		real(kind = 8), allocatable, dimension(:) :: b
		real(kind = 8), allocatable, dimension(:) :: c
		real(kind = 8), allocatable, dimension(:,:) :: k
	! ------------------------------------
	! Задание коэффициентов в зависимости от метода
	! По умолчанию используется метод 4-ого порядка
	if (present(method_name)) then
		m_name = trim(method_name)
	else
		m_name = "RKp4n3"
	end if
	call RK_Tab_Init_D(m_name, order, s, a, b, c, k, EQN)
	k = 0.0
	! Проверяем условие печатать/не печатать результаты вне цикла, потому что так экономней
	! не надо проверять для каждого шага это условие
	! ======= Метод Рунге-Кутта (полный цикл c распечаткой) =======
		if(print_calc) then
			do while(t <= t_stop)
				k = 0.0
				write(*,*) t, y ! Распечатка
				
        do i = 1,s,1
					do 	alpha = 1,EQN,1
						g(alpha) = y(alpha) + h*dot_product(k(alpha,1:s),a(i,1:s))
					end do
					!call func(g,k(1:EQN,i))
					k(1:EQN,i) = func(g)
				end do
				do 	alpha = 1,EQN,1
					y(alpha) = y(alpha) + h*dot_product(b,k(alpha,:))
				end do
				t = t + h
			end do
			
		else
		! ======= Метод Рунге-Кутта (полный цикл без распечатки) =======
			do while(t <= t_stop)
				k = 0.0
				do i = 1,s,1
					do 	alpha = 1,EQN,1
						g(alpha) = y(alpha) + h*dot_product(k(alpha,1:s),a(i,1:s))
					end do
					!call func(g,k(1:EQN,i))
					k(1:EQN,i) = func(g)
				end do
				do 	alpha = 1,EQN,1
					y(alpha) = y(alpha) + h*dot_product(b,k(alpha,:))
				end do
				t = t + h
			end do
			
		end if
	! ==== ------------------ =======
		deallocate(a,b,c,k)
	end subroutine RK_D
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!		Методы двойной точности с выбором оптимального шага (полный цикл)
!		с опционально включаемой экстраполяцией по Ричардсону 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	subroutine RK_Rich_autostep_D(func, EQN, y, t, t_start, t_stop, &
																&	h, tolerance, print_calc, Richardson, debug, method_name)
	implicit none
	!-------------------------------------------------------------------------------------------------
	! 							Аргументы процедуры	
	!-------------------------------------------------------------------------------------------------
		! Функция, задающая правую часть системы ОДУ
		interface
			function func(x)
				real(kind = 8), dimension(:), intent(in) :: x
				real(kind = 8), dimension(1:ubound(x,1)) :: func
			end function func
		end interface
		! Размерность системы
		integer, intent(in) :: EQN	
		! Функция Y (сеточная функция)
		real(kind = 8), dimension(1:EQN), intent(inout) :: y
		! Время
		real(kind = 8), intent(inout) :: t
		! Временной промежуток численного интегрирования
		real(kind = 8), intent(in) :: t_start, t_stop
		! Шаг метода, который обновляется если это необходимо
		real(kind = 8), intent(inout) :: h
		! Допустимая погрешность вычислений
		real(kind = 8), intent(in) :: tolerance
		! Распечатывать ли ход вычисления в консоль
		logical, intent(in) :: print_calc
		! Включить ли внутреннюю экстраполяцию
		logical, intent(in) :: Richardson
		! Включить ли логирование
		logical, intent(in) :: debug
		! Название метода (необязательный аргумент)
		character(len = *), optional, intent(in) :: method_name
	!-------------------------------------------------------------------------------------------------
	! 							Параметры метода
	!-------------------------------------------------------------------------------------------------
		! Стадийность метода
		integer :: s
		! Порядок метода
		integer :: order
		! Название метода (для внутреннего использования)
		character(len = 32) :: m_name
	!-------------------------------------------------------------------------------------------------
	! 						Специфические параметры для управления шагом
	!-------------------------------------------------------------------------------------------------
		! Дополнительный массив, для хранения 2-х вычисленных шагов
		real(kind = 8), dimension(1:EQN) :: y_1h
		! Дополнительный массив для хранения значения функции, вычисленного с удвоенным шагом (w)
		real(kind = 8), dimension(1:EQN) :: y_2h
		! Дополнительный массив для хранения двух промежуточных шагов
		real(kind = 8), dimension(1:EQN,1:2) :: y_tmp
		! Погрешность (локальная)
		real(kind = 8) :: error
		! Масштабирующий коэффициент
		real(kind = 8), dimension(1:EQN) :: d
		! Гарантийный фактор (предотвращает слишком быстрое изменение шага программой)
		real(kind = 8) :: fac, fac_max, fac_min
		! Новый шаг
		real(kind = 8) :: h_new, h_old
		! Был ли отбракованы те два шага?
		logical :: step_rejected
	!-------------------------------------------------------------------------------------------------
	! 									Оставшиеся внутренние переменные
	!-------------------------------------------------------------------------------------------------
		! Счетчики
		integer :: i, alpha, j
		! Вспомогательная функция
		real(kind = 8), dimension(1:EQN) :: g
	!-------------------------------------------------------------------------------------------------
	! 						Коэффициенты метода Рунге-Кутта
	!-------------------------------------------------------------------------------------------------
		real(kind = 8), allocatable, dimension(:,:) :: a
		real(kind = 8), allocatable, dimension(:) :: b
		real(kind = 8), allocatable, dimension(:) :: c
		real(kind = 8), allocatable, dimension(:,:) :: k
	!-------------------------------------------------------------------------------------------------
	! Задание коэффициентов в зависимости от метода
		if (present(method_name)) then
			m_name = trim(method_name)
		else
			m_name = "RKp4n3" ! По умолчанию используется метод 4-ого порядка
		end if
		call RK_Tab_Init_D(m_name, order, s, a, b, c, k, EQN)
		
		d = 1.0d0 ! Пока используем простейший способ вычисления масштабирующего коэффициента
		fac_max = 1.5D0
		fac_min = 0.5D0
		fac = (0.25D0)**(1.0D0/(order+1.0D0)) ! Другие возможные значения 0.8, 0.9, (0.38)**(1/(order+1))
		step_rejected = .True.
		
		if(debug) then
			! Записываем отброшенные шаги
			open(23,file = "rejected_steps.log", action = 'write')
			! Записываем принятые шаги
			open(24,file = "accepted_steps.log", action = 'write')
		end if
		
		! ======= Метод Рунге-Кутта (полный цикл, с коррекцией шага) =======
		do while(t <= t_stop)
			!---------------------------------------------------------------
			! Печатаем вычисленные на предыдущем шаге значения  в консоль
			! Так как изначально step_rejected = ИСТИНА, то при первом про-
			! ходе ничего не  выводится.
			if( (.not. step_rejected) .and. print_calc) then
				write(*,*) t-h_old, y_tmp(:,1)
				write(*,*) t, y_tmp(:,2)
			end if
			if( (.not. step_rejected) .and. debug ) then
				! Записываем не принятые шаги в файл
				write(24,*) t - h_old, h_old
				write(24,*) t, h_old
			else if( step_rejected .and. debug ) then
				write(23,*) t, h_old ! Записываем отброшенные шаги
			end if
			!---------------------------------------------------------------
			! Делаем два шага с размером шага h
			k = 0.0D0
			y_1h(:) = y(:)
			do j = 1,2,1
				do i = 1,s,1
					do 	alpha = 1,EQN,1
						g(alpha) = y_1h(alpha) + h*dot_product(k(alpha,1:s),a(i,1:s))
					end do
					k(1:EQN,i) = func(g)
				end do
				do 	alpha = 1,EQN,1
					y_1h(alpha) = y_1h(alpha) + h*dot_product(b,k(alpha,:))
				end do
				! Сохраняем оба шага, чтобы в случае удовлетворительной погрешности их использовать
				y_tmp(1:EQN,j) = y_1h(1:EQN)
			end do
			
			! Теперь делаем один шаг с размером шага 2h
			k = 0.0D0
			y_2h(:) = y(:)
			do i = 1,s,1
				do 	alpha = 1,EQN,1
					g(alpha) = y_2h(alpha) + 2.0D0*h*dot_product(k(alpha,1:s),a(i,1:s))
				end do
				k(1:EQN,i) = func(g)
			end do
			do 	alpha = 1,EQN,1
				y_2h(alpha) = y_2h(alpha) + 2.0D0*h*dot_product(b,k(alpha,:))
			end do
			
			! После этого даем оценку погрешности
			error = abs((1.0D0/(2.0D0**order - 1.0D0))*maxval( (y_1h(1:EQN) - y_2h(1:EQN))/d(1:EQN) ))
			! В любом случае необходимо вычислить новый шаг
			h_new = h*min(fac_max, max( fac_min, fac*(tolerance/error)**(1.0D0/(order + 1.0D0)) ) )
			
			!!--------------- Оценка погрешности ----------------
			!!
			!! Погрешность удовлетворительная и шаги y_1h принимаются
			!!
			if(error <= tolerance) then
				step_rejected = .False.
				
				h_old = h ! Старый шаг сохранили
				h = h_new ! Переменную шага обновляем
				t = t + 2.0D0*h_old ! Смещаемся по времени, используя двойной старый шаг
				fac_max = min(fac_max + 0.5D0, 1.5D0) ! Увеличиваем фактор в разумных пределах
				
				! --------- Опционально делаем экстраполяция Ричардсона ---------------
				if(Richardson) then
					y = y_1h(1:EQN) + (y_1h(1:EQN) - y_2h(1:EQN))/(2.0D0**order - 1.0D0)
					y_tmp(1:EQN,2) = y
				! В противном случае
				else
					y = y_1h(1:EQN)
				end if
				!-----------------------------------------------------
			else if(error > tolerance) then
				! Погрешность не удовлетворительная, шаги y_1h отбрасываются
				step_rejected = .True.
				!!
				y = y ! Оставляем то же начальное значение
				t = t ! Не делаем шаг по времени
				h_old = h ! Старый шаг сохраняем
				h = h_new ! Но переменную шага обновляем
				fac_max = 1.0D0 ! Уменьшаем фактор до единицы
				!!
			end if
			!!--------------- ^^^^^^^^^^^^^^^^^^ ----------------
		end do ! Конец while цикла
	! ======= ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ =======
		deallocate(a,b,c,k)
		if(debug) then
			close(23, status = 'keep')
			close(24, status = 'keep')
		end if
	end subroutine RK_Rich_autostep_D
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!!											Вспомогательные функции
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!--------------------
!	Вычисление числа итераций	
!--------------------
	integer function ItNum(t_start, t_stop, h)
		implicit none
		real :: t_start, t_stop, h
		! Число итераций
		ItNum = int((t_stop - t_start)/h)
	end function ItNum
!--------------------
	integer function ItNum_D(t_start, t_stop, h)
		implicit none
		real(kind = 8) :: t_start, t_stop, h
		! Число итераций
		ItNum_D = int((t_stop - t_start)/h)
	end function ItNum_D
!--------------------
!	Распечатка текущей итерации N раз за весь цикл	
!--------------------
	subroutine ItInfo(t_start, t, it_num, h, N)
		implicit none
		real :: t, t_start, h
		integer :: it_num
		integer :: N
		integer :: i
		i = int((t - t_start)/h)
		if(mod(i,(it_num + 1)/N) .eq. 0) then
			print *, "Итерация №:",char(27),"[32m",i,char(27),"[39m"
		end if
	end subroutine ItInfo
!--------------------
	subroutine ItInfo_D(t_start, t, it_num, h, N)
		implicit none
		real(kind = 8) :: t, t_start, h
		integer :: it_num
		integer :: N
		integer :: i
		i = int((t - t_start)/h)
		if(mod(i,(it_num + 1)/N) .eq. 0) then
			print *, "Итерация №:",char(27),"[32m",i,char(27),"[39m"
		end if
	end subroutine ItInfo_D
end module RK