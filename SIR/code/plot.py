#! /usr/bin/env python3
# -*- coding: utf-8 -*-
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
import sys

if(len(sys.argv) <= 1):
	print("Ни одна модель для построения не выбрана. Выход")
	sys.exit(0)
elif(sys.argv[1] == 'Det_SIR'):
	fig_title = "Детерминированная модель SIR"
	file_prefix = "Det_"
elif(sys.argv[1] == 'Stoch_SIR'):
	fig_title = "Стохастическая модель SIR"
	file_prefix = "Stoch_"
else:
	fig_title = "Стохастическая модель SIR"
	file_prefix = "Stoch_"
	print("Построение чертежа для стохастической модели SIR")

t,S,I,R = np.loadtxt('data.dat', usecols = (0,1,2,3), unpack=True)
# Число картинок
fig_nums = [ 0, 1 ]

fig = [ plt.figure(i) for i in fig_nums ]
ax = [ fig[0].add_subplot(1,1,1), fig[1].gca(projection='3d') ]
# ax[0].set_aspect('equal')

# S(t) I(t) R(t)
ax[0].set_xlabel("$t$ — время", fontsize = 14)
ax[0].set_ylabel("Число людей", fontsize = 14)
ax[0].set_title(fig_title, fontsize = 14)

# Фазовый портрет
ax[1].set_xlabel("$S(t)$", fontsize = 14)
ax[1].set_ylabel("$I(t)$", fontsize = 14)
ax[1].set_zlabel("$R(t)$", fontsize = 14)
ax[1].set_title(fig_title+": фазовый портрет.", fontsize = 14)

file = [ file_prefix + "SIR(t).png", file_prefix + "Phase.png" ]

print("Строим графики...")
print("S(t), I(t), R(t) и фазовый портрет")

ax[0].plot(t,S, label = "$S(t)$ — здоровые")
#ax[0].plot(t,I, color = 'k', linestyle = '-', label = "$I(t)$ — инфицированные")
ax[0].plot(t,I, label = "$I(t)$ — инфицированные")
ax[0].plot(t,R, label = "$R(t)$ — иммунизированные")
ax[1].plot(S,I,R, label= "Фазовый портрет")

ax[0].legend(loc='right')


print("Сохраняем файлы")
# fig[0].savefig(file[0],dpi=300, format="png", pad_inches=0.1)

for i, fn in zip(fig_nums,file):
	fig[i].savefig(fn,dpi=300, format="png", pad_inches=0.1)

	
	
	
	