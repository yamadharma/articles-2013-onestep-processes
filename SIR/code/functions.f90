module functions
implicit none
contains
	! Детерминированная SIR модель
	function func(x)
		implicit none
		real(kind = 8), dimension(:), intent(in) :: x
		real(kind = 8), dimension(lbound(x,1):ubound(x,1)) :: func
		!==
		real(kind = 8) :: beta, nu
		namelist /sir_params/ beta, nu
		open (11, file = 'params.data', delim = 'apostrophe', action = 'read')
		read (11, nml = sir_params)
		close(11)
		func(1) = -beta*x(1)*x(2)
		func(2) =  beta*x(1)*x(2) - nu*x(2)
		func(3) =  nu*x(2)
	end function func
	
	! Стохастическая модель SIR
	function Afunc(x)
		implicit none
		real(kind = 8), dimension(:), intent(in) :: x
		real(kind = 8), dimension(lbound(x,1):ubound(x,1)) :: Afunc
		!==
		real(kind = 8) :: beta, nu
		namelist /sir_params/ beta, nu
		open (11, file = 'params.data', delim = 'apostrophe', action = 'read')
		read (11, nml = sir_params)
		close(11)
		Afunc(1) = -beta*x(1)*x(2)
		Afunc(2) =  beta*x(1)*x(2) - nu*x(2)
		Afunc(3) =  nu*x(2)
	end function Afunc
	
	function Bfunc(x)
		use matrix
		implicit none
		real(kind = 8), dimension(:), intent(in) :: x
		real(kind = 8), dimension(lbound(x,1):ubound(x,1),lbound(x,1):ubound(x,1)) :: Bfunc
		real(kind = 8), dimension(1:3,1:3) :: B
		!==
		real(kind = 8) :: beta, nu
		namelist /sir_params/ beta, nu
		open (11, file = 'params.data', delim = 'apostrophe', action = 'read')
		read (11, nml = sir_params)
		close(11)
		!==
		B(1,1:3) = [ beta*x(1)*x(2), -beta*x(1)*x(2), 0.0D0 ]
		B(2,1:3) = [ beta*x(1)*x(2), -beta*x(1)*x(2) + nu*x(2), -nu*x(2) ]
		B(3,1:3) = [ 0.0D0, -nu*x(2), nu*x(2) ]
		Bfunc = matrix_sqrt(B,3,3)
	end function Bfunc
end module functions