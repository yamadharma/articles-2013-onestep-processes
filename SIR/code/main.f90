program main
	use RK
	use functions
	use sde_num_methods
	implicit none
	integer, parameter :: EQN = 3
	!!
	real(kind = 8) :: h
	real(kind = 8) :: eps
	real(kind = 8) :: t_start, t_stop
	real(kind = 8) :: t
	!!
	real(kind = 8) :: beta, nu
	!!
	real(kind = 8), dimension(1:EQN) :: y, dy
	!!
	character(len = 32) :: arg
	namelist /calc_params/ h, eps, t_start, t_stop
	namelist /sir_params/ beta, nu
	
	open (11, file = 'params.data', delim = 'apostrophe', action = 'read')
	read (11, nml = calc_params)
	read (11, nml = sir_params)
	close(11)
	
	! Получаем один аргумент
	call get_command_argument(1, arg)
	
	! Вызов метода для решения
	!y = [ nu/beta, 0.0d0, 100d0 - nu/beta ]
	!dy = [ 1.0d0, 0.5d0, 0.5d0 ]
	y = [ 100.0d0, 5.0d0, 0.0d0 ]
	t = t_start
	
	if (trim(arg) == "Det_SIR") then
		call RK_D(func, EQN, y, t, t_start, t_stop, h, .true., 'RKp4n3')
	else if(trim(arg) == "Stoch_SIR") then
		call StRKp3_d(Afunc, Bfunc, EQN, y, t, t_start, t_stop, h)
	end if
!----------------------------------------------------------------------
end program main
