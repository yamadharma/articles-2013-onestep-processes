#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import subprocess
import matplotlib.pyplot as plt
import numpy as np

def drange(start, stop, step):
	r = start
	while r < stop:
		yield r
		r += step
#---------------------
plt.figure(1)
# Заголовок этого чертежа
plt.title('Детерминированная модель хищник-жертва', fontsize=14)
plt.xlabel('$x(t)$ — численность жертв', fontsize=14)
plt.ylabel('$y(t)$ — численность хищников', fontsize=14)
#plt.text(1, 3, u'Текст')

r1 = drange(0.0, 11.0, 1.0)
r2 = drange(0.0, 11.0, 1.0)

# r1 = [0.0, 1.0, 2.0, 3.0, 4.0, 5.0]
# r2 = [0.0, 1.0, 2.0, 3.0, 4.0, 5.0]
for x, y in zip(r1,r2): 
	sx = str(x)
	sy = str(y)
	print("x = ",sx,"y = ",sy)
	subprocess.call(['./main', 'deterministic', sx, sy])
	p, q = np.loadtxt('out.dat', usecols=(2,1), unpack=True)
	plt.plot(q, p, ".", markersize=1)
	del q
	del p
#xmin, xmax = plt.xlim()
#plt.xlim( -10, xmax)
plt.savefig('All_det.png',dpi=300, format="png", pad_inches=0.1)
plt.savefig('All_det.pdf',dpi=300, format="pdf", pad_inches=0.1)
