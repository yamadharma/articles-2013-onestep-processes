#!/usr/bin/gnuplot
# Выбор терминала и формата файла для вывода рисунка
if(exists("png")){
	set terminal pngcairo enhanced size 1024,768 font "DejaVu Serif"
}
else {	
	set terminal pdfcairo enhanced rounded font "DejaVu Serif"
}
#Установка кодировки
set encoding utf8
# Нарисовать сетку и засечки на осях
set grid xtics ytics
# Оставить только нижнюю и левую границы
set border 3
#Убрать ось x сверху и ось y снизу
set xtics nomirror
set ytics nomirror
# Число ломанных
set samples 50000
# Стиль линий
set termoption dash
# set style line 1 linecolor rgb '#0060ad' linetype 1 linewidth 1
# set style line 2 linecolor rgb '#dd181f' linetype 1 linewidth 1
set style line 1 linecolor rgb "black" linetype 1 linewidth 1 
set style line 2 linecolor rgb "black" linetype 2 linewidth 1 
##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 							Детерминированная модель
##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if(exists("png")){
	set output "phase.png"
}
else {
	set output "phase.pdf"
}
# Название картинки
set title "Детерминированная модель Лотки-Вольтерры (фазовый портрет)"
# Настройка осей
set ylabel "y(t) — численность хищников"
set xlabel "x(t) — численность жертв"

plot "out.dat" using 2:3 with lines linestyle 1 title "Хищник-Жертва"
if(exists("png")){
	set output "population.png"
}
else {
	set output "population.pdf"
}
# Название картинки
set title "Детерминированная модель Лотки-Вольтерры (графики решений)"
# Настройка осей
set ylabel "Численность"
set xlabel "t — время"

set key at 25,15

plot "out.dat" using 1:2 with lines linestyle 1 title "Жертвы x(t)",  "out.dat" using 1:3 with lines linestyle 2 title "Хищники y(t)"