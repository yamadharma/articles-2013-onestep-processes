#!/usr/bin/gnuplot -smooth
# Выбор терминала и формата файла для вывода рисунка
if(exists("png")){
	set terminal pngcairo enhanced size 1024,768 font "DejaVu Serif"
}
else {	
	set terminal pdfcairo enhanced rounded font "DejaVu Serif"
}
#Установка кодировки
set encoding utf8
# Нарисовать сетку и засечки на осях
set grid xtics ytics
# Оставить только нижнюю и левую границы
set border 3
#Убрать ось x сверху и ось y снизу
set xtics nomirror
set ytics nomirror
# Число ломанных
set samples 50000
# Стиль линий
set style line 1 linecolor rgb '#0060ad' linetype 1 linewidth 1
set style line 2 linecolor rgb '#dd181f' linetype 1 linewidth 1
##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 							Детерминированная модель
##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if(exists("png")){
	set output "phase.png"
}
else {
	set output "phase.pdf"
}
# Название картинки
set title "Детерминированная модель Лоттка-Вальтерра (фазовый портрет)"
# Настройка осей
set ylabel "y(t) — численность хищников"
set xlabel "x(t) — численность жертв"

plot "out.dat" using 2:3 with lines linestyle 1 title "Хищник-Жертва"
if(exists("png")){
	set output "population.png"
}
else {
	set output "population.pdf"
}
# Название картинки
set title "Детерминированная модель Лоттка-Вальтерра (графики решений)"
# Настройка осей
set ylabel "Численность"
set xlabel "t — время"

plot "out.dat" using 1:2 with lines linestyle 1 title "Жертвы x(t)",  "out.dat" using 1:3 with lines linestyle 2 title "Хищники y(t)"
##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 								Стохастическая модель
##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Название картинки
set title "Стохастическая модель Лоттка-Вальтерра"
if(exists("png")){
	set output "sphase.png"
}
else {
	set output "sphase.pdf"
}
# Настройка осей
set ylabel "y(t) — численность хищников"
set xlabel "x(t) — численность жертв"

#plot "sout.dat" using 2:3 smooth csplines linestyle 1 title "Хищник-Жертва"
plot "sout.dat" using 2:3 with lines linestyle 1 title "Хищник-Жертва"

if(exists("png")){
	set output "spopulation.png"
}
else {	
	set output "spopulation.pdf"
}
# Настройка осей
set ylabel "Численность"
set xlabel "t — время"

plot "sout.dat" using 1:2 with lines linestyle 1 title "Жертвы  x(t)",  "sout.dat" using 1:3 with lines linestyle 2 title "Хищники y(t)"
