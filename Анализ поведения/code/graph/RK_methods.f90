module RK_methods
implicit none
! Подпрограмма RK(func, EQN, y, t, h, order) реализует итерацию метода Рунге-Кутта
! 	func --- подпрограмма, задающая правую часть системы диф. уравнений (функцию F)
!	имеет вид func(y,yp)
		! real, dimension(*) :: y
		! real, dimension(*) :: yp
		! yp(i) = dy(i)/dt
! 	integer :: EQN --- порядок системы
! 	real :: y --- сеточная функция
! 	real :: t --- текущий момент времени
! 	real :: h --- шаг сетки
! 	real :: order --- порядок метода (необязательный аргумент)
contains
	subroutine RK(func, EQN, y, t, h, order)
	implicit none
		!integer :: info
	! Параметры метода
		! Стадийность метода
		integer :: s
		! Порядок метода (необязательный аргумент)
		integer, optional, intent(in) :: order
		! Порядок метода (для внутреннего использования)
		integer :: privat_order
		! Шаг метода
		real, intent(in) :: h
	! Параметры системы уравнений (задачи)
		! Размерность системы
		integer, intent(in) :: EQN
		! Подпрограмма, задающая правую часть системы (функцию F)
		external :: func
	! Переменные
		! Счетчики
		integer :: i
		integer :: alpha
		! Время
		real, intent(inout) :: t
		! Функция Y
		real, dimension(1:EQN), intent(inout) :: y
		! Вспомогательная функция
		real, dimension(1:EQN) :: g
	! Коэффициенты метода Рунге-Кутта
		! real, dimension(1:s,1:s) :: a
		! real, dimension(1:s) :: b
		! real, dimension(1:EQN,1:s) :: k
		real, allocatable :: a(:,:)
		real, allocatable :: b(:)
		real, allocatable :: k(:,:)
	! ------------------------------------
	! Задание коэффициентов в зависимости от порядка метода
	! По умолчанию используется метод 4-ого порядка
	if (present(order)) then
		privat_order = order
	else
		privat_order = 4
	end if
	select case (privat_order)
		case(1)
			s = 1
			allocate(a(1:s,1:s), b(1:s), k(1:EQN,1:s))
			a(1,1) = 0.0
			b(1) = 1
		case(2)
			s = 2
			allocate(a(1:s,1:s), b(1:s), k(1:EQN,1:s))
			a(1,1) = 0.0
			a(1,2) = 0.0
			a(2,1) = 1.0
			a(2,2) = 0.0
			b(1) = 0.5
			b(2) = 0.5
		case(3)
			s = 3
			allocate(a(1:s,1:s), b(1:s), k(1:EQN,1:s))
			a(1,1) = 0.0
			a(1,2) = 0.0
			a(1,3) = 0.0
			a(2,1) = 2.0/3.0
			a(2,2) = 0.0
			a(2,3) = 0.0
			a(3,1) = 1.0/3.0
			a(3,2) = 1.0/3.0
			a(3,3) = 0.0
			b(1) = 0.25
			b(2) = 0.0
			b(3) = 0.75
		case(4)
			s = 4
			allocate(a(1:s,1:s), b(1:s), k(1:EQN,1:s))
			a(1,1) = 0.0
			a(1,2) = 0.0
			a(1,3) = 0.0
			a(1,4) = 0.0
			a(2,1) = 1.0/3.0
			a(2,2) = 0.0
			a(2,3) = 0.0
			a(2,4) = 0.0
			a(3,1) = -1.0/3.0
			a(3,2) = 1.0
			a(3,3) = 0.0
			a(3,4) = 0.0
			a(4,1) = 1.0
			a(4,2) = -1.0
			a(4,3) = 1.0
			a(4,4) = 0.0
			b(1) = 1.0/8.0
			b(2) = 3.0/8.0
			b(3) = 3.0/8.0
			b(4) = 1.0/8.0
		case default
			s = 4
			allocate(a(1:s,1:s), b(1:s), k(1:EQN,1:s))
			a(1,1) = 0.0
			a(1,2) = 0.0
			a(1,3) = 0.0
			a(1,4) = 0.0
			a(2,1) = 1.0/3.0
			a(2,2) = 0.0
			a(2,3) = 0.0
			a(2,4) = 0.0
			a(3,1) = -1.0/3.0
			a(3,2) = 1.0
			a(3,3) = 0.0
			a(3,4) = 0.0
			a(4,1) = 1.0
			a(4,2) = -1.0
			a(4,3) = 1.0
			a(4,4) = 0.0
			b(1) = 1.0/8.0
			b(2) = 3.0/8.0
			b(3) = 3.0/8.0
			b(4) = 1.0/8.0
	end select
	k = 0.0
	! ------------------------------------
	! ======= Метод Рунге-Кутта =======
		do i = 1,s,1
			do 	alpha = 1,EQN,1
				g(alpha) = y(alpha) + h*dot_product(k(alpha,1:s),a(i,1:s))
			end do
			call func(g,k(1:EQN,i))
			!k(1:EQN,i) = f(g)
		end do
		do 	alpha = 1,EQN,1
			y(alpha) = y(alpha) + h*dot_product(b,k(alpha,:))
		end do
		t = t + h
	! ==== ------------------ =======
	end subroutine RK
!-----------
subroutine RK_D(func, EQN, y, t, h, order)
	implicit none
		!integer :: info
	! Параметры метода
		! Стадийность метода
		integer :: s
		! Порядок метода (необязательный аргумент)
		integer, optional, intent(in) :: order
		! Порядок метода (для внутреннего использования)
		integer :: privat_order
		! Шаг метода
		real(kind = 8), intent(in) :: h
	! Параметры системы уравнений (задачи)
		! Размерность системы
		integer, intent(in) :: EQN
		! Подпрограмма, задающая правую часть системы (функцию F)
		external :: func
	! Переменные
		! Счетчики
		integer :: i
		integer :: alpha
		! Время
		real(kind = 8), intent(inout) :: t
		! Функция Y
		real(kind = 8), dimension(1:EQN), intent(inout) :: y
		! Вспомогательная функция
		real(kind = 8), dimension(1:EQN) :: g
	! Коэффициенты метода Рунге-Кутта
		! real, dimension(1:s,1:s) :: a
		! real, dimension(1:s) :: b
		! real, dimension(1:EQN,1:s) :: k
		real(kind = 8), allocatable :: a(:,:)
		real(kind = 8), allocatable :: b(:)
		real(kind = 8), allocatable :: k(:,:)
	! ------------------------------------
	! Задание коэффициентов в зависимости от порядка метода
	! По умолчанию используется метод 4-ого порядка
	if (present(order)) then
		privat_order = order
	else
		privat_order = 4
	end if
	select case (privat_order)
		case(1)
			s = 1
			allocate(a(1:s,1:s), b(1:s), k(1:EQN,1:s))
			a(1,1) = 0.0
			b(1) = 1
		case(2)
			s = 2
			allocate(a(1:s,1:s), b(1:s), k(1:EQN,1:s))
			a(1,1) = 0.0
			a(1,2) = 0.0
			a(2,1) = 1.0
			a(2,2) = 0.0
			b(1) = 0.5
			b(2) = 0.5
		case(3)
			s = 3
			allocate(a(1:s,1:s), b(1:s), k(1:EQN,1:s))
			a(1,1) = 0.0
			a(1,2) = 0.0
			a(1,3) = 0.0
			a(2,1) = 2.0/3.0
			a(2,2) = 0.0
			a(2,3) = 0.0
			a(3,1) = 1.0/3.0
			a(3,2) = 1.0/3.0
			a(3,3) = 0.0
			b(1) = 0.25
			b(2) = 0.0
			b(3) = 0.75
		case(4)
			s = 4
			allocate(a(1:s,1:s), b(1:s), k(1:EQN,1:s))
			a(1,1) = 0.0
			a(1,2) = 0.0
			a(1,3) = 0.0
			a(1,4) = 0.0
			a(2,1) = 1.0/3.0
			a(2,2) = 0.0
			a(2,3) = 0.0
			a(2,4) = 0.0
			a(3,1) = -1.0/3.0
			a(3,2) = 1.0
			a(3,3) = 0.0
			a(3,4) = 0.0
			a(4,1) = 1.0
			a(4,2) = -1.0
			a(4,3) = 1.0
			a(4,4) = 0.0
			b(1) = 1.0/8.0
			b(2) = 3.0/8.0
			b(3) = 3.0/8.0
			b(4) = 1.0/8.0
		case default
			s = 4
			allocate(a(1:s,1:s), b(1:s), k(1:EQN,1:s))
			a(1,1) = 0.0
			a(1,2) = 0.0
			a(1,3) = 0.0
			a(1,4) = 0.0
			a(2,1) = 1.0/3.0
			a(2,2) = 0.0
			a(2,3) = 0.0
			a(2,4) = 0.0
			a(3,1) = -1.0/3.0
			a(3,2) = 1.0
			a(3,3) = 0.0
			a(3,4) = 0.0
			a(4,1) = 1.0
			a(4,2) = -1.0
			a(4,3) = 1.0
			a(4,4) = 0.0
			b(1) = 1.0/8.0
			b(2) = 3.0/8.0
			b(3) = 3.0/8.0
			b(4) = 1.0/8.0
	end select
	k = 0.0
	! ------------------------------------
	! ======= Метод Рунге-Кутта =======
		do i = 1,s,1
			do 	alpha = 1,EQN,1
				g(alpha) = y(alpha) + h*dot_product(k(alpha,1:s),a(i,1:s))
			end do
			call func(g,k(1:EQN,i))
			!k(1:EQN,i) = f(g)
		end do
		do 	alpha = 1,EQN,1
			y(alpha) = y(alpha) + h*dot_product(b,k(alpha,:))
		end do
		t = t + h
	! ==== ------------------ =======
	end subroutine RK_D
!================================================================================================================
!--------------------
!	Вычисление числа итераций	
!--------------------
	integer function ItNum(t_start, t_stop, h)
		implicit none
		real :: t_start, t_stop, h
		! Число итераций
		ItNum = int((t_stop - t_start)/h)
	end function ItNum
!--------------------
	integer function ItNum_D(t_start, t_stop, h)
		implicit none
		real(kind = 8) :: t_start, t_stop, h
		! Число итераций
		ItNum_D = int((t_stop - t_start)/h)
	end function ItNum_D
!--------------------
!	Распечатка текущей итерации N раз за весь цикл	
!--------------------
	subroutine ItInfo(t_start, t, it_num, h, N)
		implicit none
		real :: t, t_start, h
		integer :: it_num
		integer :: N
		integer :: i
		i = int((t - t_start)/h)
		if(mod(i,(it_num + 1)/N) .eq. 0) then
			print *, "Итерация №:",char(27),"[32m",i,char(27),"[39m"
		end if
	end subroutine ItInfo
!--------------------
	subroutine ItInfo_D(t_start, t, it_num, h, N)
		implicit none
		real(kind = 8) :: t, t_start, h
		integer :: it_num
		integer :: N
		integer :: i
		i = int((t - t_start)/h)
		if(mod(i,(it_num + 1)/N) .eq. 0) then
			print *, "Итерация №:",char(27),"[32m",i,char(27),"[39m"
		end if
	end subroutine ItInfo_D
end module RK_methods
