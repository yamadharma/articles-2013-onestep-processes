program main
	use RK_methods
	use functions
	use sde_num_methods
	use matrix
	implicit none
	integer, parameter :: neqn = 2
	real(kind = 8), parameter :: h = 0.001
	! Параметры модели
	real(kind = 8) :: alpha, beta, gamma, delta
	namelist /prepre_param/ alpha, beta, gamma, delta
	! Границы отрезка численного интегрирования
	real(kind = 8) :: t_start, t_stop
	namelist /time_param/ t_start, t_stop
	! Время
	real(kind = 8) :: t
	! Сетчатая функция
	real(kind = 8), dimension(neqn) :: y
	! Аргументы командной строки
	! Первый аргумент задает какую модель надо строить
	! следующие два передают начальные значения
	character(len=32), dimension(0:3) :: arg
	real(kind = 8), dimension(2:3) :: a 
	integer :: i
	! Открываем файл и считываем параметры
	open (11, file = 'params.dat', delim = 'apostrophe')
	read (11, nml = prepre_param)
	read (11, nml = time_param)
	close (11)
	
	do i = 1,3,1
		call getarg(i,arg(i))
	end do
	read(arg(2),*) a(2)
	read(arg(3),*) a(3)
	
	! Задаем начальные значения (близкие к положению равновесия)
	y(1) = gamma/delta + a(2)
	y(2) = alpha/beta + a(3)
	t = t_start
!----------------------------------------------------------------------
	if(adjustl(arg(1)) .eq. "deterministic") then
		! Открываем файлы для записи результатов вычисления
		open (12, file='out.dat')
		!! Детерминированная модель Хищник-Жертва
		!write(*,*) "Детерминированная модель"
		do while(t .lt. t_stop)
			write(12,*) t, y
			call RK_D(func, neqn, y, t, h, 2)
		end do
		close(12, status = "keep")
	end if
!----------------------------------------------------------------------

	y(1) = gamma/delta + a(2)
	y(2) = alpha/beta + a(3)
	t = t_start
!----------------------------------------------------------------------
	if(adjustl(arg(1)) .eq. "stochastic") then
		! Открываем файлы для записи результатов вычисления
		open (13, file='sout.dat')
		!! Стохастическая модель Хищник-Жертва
		!write(*,*) "Стохастическая модель"
		do while(t .lt. t_stop)
			write(13,*) t, y
			call StRK3D(Afunc, Bfunc, neqn, y, t, h)
		end do
		close(13, status = "keep")
	end if
!----------------------------------------------------------------------
end program main
