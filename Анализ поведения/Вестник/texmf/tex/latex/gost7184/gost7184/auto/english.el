(TeX-add-style-hook "english"
 (lambda ()
    (TeX-add-symbols
     "bbland"
     "bbletal"
     "bbledby"
     "bbledn"
     "bblvol"
     "bblissue"
     "bblissuefirst"
     "bblof"
     "bblno"
     "bblnofirst"
     "bblpp"
     "bblp"
     "bblnump"
     "bbltechrep"
     "bblmthesis"
     "bblphdthesis"
     "bblcompiler")))

