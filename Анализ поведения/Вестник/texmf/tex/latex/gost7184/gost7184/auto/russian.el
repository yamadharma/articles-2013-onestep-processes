(TeX-add-style-hook "russian"
 (lambda ()
    (TeX-add-symbols
     "bbland"
     "bbletal"
     "bbledby"
     "bbled"
     "bbleds"
     "bbledn"
     "bblvol"
     "bblissue"
     "bblissuefirst"
     "bblof"
     "bblno"
     "bblnofirst"
     "bblpp"
     "bblp"
     "bblnump"
     "bbltechrep"
     "bblmthesis"
     "bblphdthesis"
     "bblcompiler"
     "bblfeb")))

