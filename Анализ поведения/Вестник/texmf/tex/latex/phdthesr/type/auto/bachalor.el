(TeX-add-style-hook "bachalor"
 (lambda ()
    (TeX-add-symbols
     "PHDauthorDescr"
     "PHDstudygroupDescr"
     "PHDcountryDescr"
     "PHDchiefDescr"
     "PHDdisciplineDescr")
    (TeX-run-style-hooks
     "phdthesr/type/title-brs")))

