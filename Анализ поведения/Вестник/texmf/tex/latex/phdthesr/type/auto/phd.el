(TeX-add-style-hook "phd"
 (lambda ()
    (TeX-add-symbols
     "PHDright"
     "PHDchiefDescr"
     "PHDdisciplineDescr"
     "footnotesize"
     "footnoterule")))

