(TeX-add-style-hook "title-brs"
 (lambda ()
    (TeX-add-symbols
     "brsTable"
     "approvedStamp"
     "authorTable"
     "brsTableProblemType"
     "brsTableBalls"
     "brsTableBallsSum"
     "brsTableAttestInterim"
     "brsTableAttestFinal"
     "brsTableTotal"
     "brsTableText"
     "brsTablePresentation"
     "brsTableProtection"
     "brsTableCourse"
     "PHDauthorDescr"
     "PHDstudygroupDescr"
     "PHDcountryDescr"
     "PHDchiefDescr"
     "PHDyearShort"
     "PHDapprove"
     "footnotesize"
     "footnoterule")
    (TeX-run-style-hooks
     "mdwtab")))

