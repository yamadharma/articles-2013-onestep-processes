(TeX-add-style-hook "pfu-math-inf-phys"
 (lambda ()
    (TeX-run-style-hooks
     "truncate"
     "breakwords"
     "pfuproc/style/pfu-bulletin")))

