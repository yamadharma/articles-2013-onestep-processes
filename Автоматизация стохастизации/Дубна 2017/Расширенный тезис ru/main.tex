\input{preambule.tex} % Включаем преамбулу

\begin{document}

\section{Аннотация}
Библиотека компьютерной алгебры SymPy применяется для автоматизации алгоритма вывода систем обыкновенных дифференциальных уравнений и систем стохастических дифференциальных уравнений из кинетических схем взаимодействия. Кроме непосредственно описания моделей химических реакций, такие схемы могут описывать биологические, экологические и технические модели~\cite{lit30c,L_01}. В работе описан сам алгоритм, некоторые детали реализации и приведены результаты его работы для двух примеров.
\section{Введение}

Для описания моделей, где интенсивность взаимодействия составных компонентов зависит от концентрации того или иного компонента, можно использовать схемы взаимодействия из химической кинетики:
  \[
    \sum\limits^{n}_{i=1}N^{s}_{i}X_{i} \xleftrightarrow[\;\;k^{-}_{s}\;\;]{\;\;k^{+}_{s}\;\;} \sum\limits^{n}_{i=1}M^{s}_{i}X_{i},\; s = 1,\ldots,S.
  \]
Здесь $X_{i}$ обозначает количество $i$-го компонента, матрицы $\mathbf{M} = [M^{s}_{i}]$ и $\mathbf{N} = [N^{s}_{i}]$ называются матрицами состояния системы и задают количество взаимодействующих компонентов системы на каждом этапе реакции, $k^{+}_{s}$ и $k^{-}_{s}$ --- это коэффициенты взаимодействия в прямом и обратном направлении реакции.

Примером моделей, которые можно схематически описать такими схемами, могут служить модели химических реакций, биологических и экологических систем. Из данных схем можно вывести систему обыкновенных дифференциальных уравнений, описывающую изменение переменных $X_{i}(t)$ в течении времени.

В нашем коллективе был обобщен метод~\cite{lit28a}, позволяющий осуществлять стохастизацию детерминированной модели, исходя из имплицитных стохастических свойств самой системы. При этом исходных данных, имеющихся в схеме взаимодействия, достаточно, чтобы записать стохастическое дифференциальное уравнения. Для систем большой размерности метод стохастизации требует большого количества рутинной работы. Однако он поддается алгоритмизации и в данной работе описана автоматизации вывода вектора сноса и матрицы диффузии с помощью языка \texttt{Python} и библиотеки компьютерной алгебры \texttt{SymPy}.

\section{Алгоритм получения систем ОДУ и СДУ}
В качествен исходных данных выступают матрицы $\mathbf{M}$ и $\mathbf{N}$ а также коэффициенты взаимодействия $k^{+}_{s}$ и $k^{-}_{s}$. Находим матрицу $\mathbf{R} = \mathbf{M}^{T} - \mathbf{N}^{T}$, столбцы которой обозначим векторами $\mathbf{r}^{j}$. Далее следует вычислить вспомогательные величины
  \begin{gather*}
    s^{+}_{j}(\mathbf{x}) = k^{+}_{j}\prod\limits^{n}_{i = 1}\dfrac{x^{i}!}{(x^{i} - N^{j}_{i})!} = k^{+}_{j}\cdot \dfrac{x^{1}!}{(x^{1} - N^{j}_{1})!} \cdot\ \dfrac{x^{2}!}{(x^{2} - N^{j}_{2})!} \cdot \ldots \cdot \dfrac{x^{n}!}{(x^{n} - N^{j}_{n})!},\\
    s^{-}_{j}(\mathbf{x}) = k^{-}_{j}\prod\limits^{n}_{i = 1}\dfrac{x^{i}!}{(x^{i} - M^{j}_{i})!} = k^{+}_{j}\cdot \dfrac{x^{1}!}{(x^{1} - M^{j}_{1})!} \cdot\ \dfrac{x^{2}!}{(x^{2} - M^{j}_{2})!} \cdot \ldots \cdot \dfrac{x^{n}!}{(x^{n} - M^{j}_{n})!}.
  \end{gather*}
Используя эти величины можно рассчитать \emph{вектор сноса} $\mathbf{f}(\mathbf{x})$ и \emph{матрицу диффузии} $\mathbf{G}(\mathbf{x})$. Вектор сноса вычисляется по формуле
\[
  \mathbf{f}(\mathbf{x}) = \mathbf{r}^{1}(s^{+}_{1}(\mathbf{x}) - s^{-}_{1}(\mathbf{x})) + \mathbf{r}^{2}(s^{+}_{2}(\mathbf{x}) - s^{-}_{2}(\mathbf{x})) + \ldots + \mathbf{r}^{s}(s^{+}_{s}(\mathbf{x}) - s^{-}_{s}(\mathbf{x})),
\]
и позволяет записать систему ОДУ
\[
  \dfrac{\d \mathbf{x}}{\d t} = \mathbf{f}(\mathbf{x}).
\]
Для записи системы стохастических дифференциальных уравнений в форме Ито, необходимо рассчитать также и матрицу $\mathbf{G}(\mathbf{x})$
\[
  \mathbf{G}(\mathbf{x}) = \mathbf{r}^{1} (\mathbf{r}^{1})^{T}(s^{+}_{1}(\mathbf{x}) - s^{-}_{1}(\mathbf{x})) + \mathbf{r}^{2} (\mathbf{r}^{2})^{T}(s^{+}_{2}(\mathbf{x}) - s^{-}_{2}(\mathbf{x})) + \ldots + \mathbf{r}^{s} (\mathbf{r}^{s})^{T}(s^{+}_{s}(\mathbf{x}) - s^{-}_{s}(\mathbf{x})),
\]
после чего система СДУ записывается в виде
  \[
    \d\mathbf{x} = \mathbf{f}(\mathbf{x})\d t + \sqrt{\mathbf{G}(\mathbf{x})}\mathbf{W}(t),
  \]
  где $\mathbf{W}$ --- $n$--мерный винеровский процесс.
  
\section{Краткое описание программной реализации}

Для автоматизации получения вектора сноса и матрицы диффузии использован язык \texttt{Python}~\cite{L_Python} и библиотека компьютерной алгебры \texttt{SymPy}~\cite{L_Scipy}. В результате достаточно ввести символьные вектора $X$ и $K$ и числовые матрицы $N$ и $M$, которые могут задаваться в том числе и в формате числовых массивов \texttt{NumPy}~\cite{L_Scipy}.
Основные функции всего две: для получения вектора сноса и матрицы диффузии в \texttt{SymPy} формате
  \begin{verbatim}
    f = drift_vector(X, K, N, M),
    G = diffusion_matrix(X, K, N, M).
  \end{verbatim}
  Далее, воспользовавшись встроенными методами, можно преобразовать $\mathbf{f}$ и $\mathbf{G}$ в \LaTeX формулы или в выражения, составленные в соответствии с синтаксическими правилами большого круга языков программирования. Поддерживаются как распространенные (C/C++, Fortran) так и новые (Julia Lang).

\section{Примеры и верификация программы}
Для проверки программы рассмотрим два примера. Первый пример это модель Ферхюльста, описывающая рост популяции в условиях ограниченности ресурсов (логистическая кривая), а вторая модель это модель брюсселятора~\cite{L_gardiner}, предложенная И. Пригожином как простейшая модель реакции типа Белоусова--Жаботинского. Данные модели хорошо известны и представляют возможность удостоверится в корректной работе созданных функций.
\subsection{Модель Ферхюльста}
Модель логистического роста описывается следующей схемой:
\[ X \xrightarrow{\;\;k_{1}\;\;} 2X,\;\; X + X \xrightarrow{\;\;k_{2}\;\;} X.
\]
В этом случае матрицы $\mathbf{M}$ и $\mathbf{N}$ вырождаются в векторы: $N^1_1 = 1$, $N^2_1 = 2$, $M^1_1 = 2$, $M^2_1 = 1$, а векторы $\mathbf{r}$ --- в скаляры $r^1 = 1$, $r^2 = -1$. Так как реакция не обратимая, то достаточно вычислить только $s^{+}$:
\begin{align*}
  & s^{+}_{1} = k_1 \cdot \dfrac{x!}{(x-1)!} = k_1 x,\\
  & s^{+}_{2} = k_2 \cdot \dfrac{x!}{(x-2)!} = k_2 x (x-1) = k_{2}x^2 - k_2 x.
\end{align*}
\begin{align*}
    &f(x) = 1\cdot k_1 x - 1(k_2 x^2 - k_2 x) = k_1 x + k_2 x - k_2 x^2,\\
    &g(x) = 1\cdot k_1 x + (k_2 x^2 - k_2 x) = k_1 x - k_2 x + k_2 x^2.
  \end{align*}
  Обычно, в связи с тем, что $x^2$ -- велико, а $x$ -- мало, слагаемое $k_2 x$ отбрасывают.
  \[
   {\d x}/{\d t} = k_1x - k_2 x^2,
  \]
  и
  \[
  \d x(t) = (k_1 x - k_2 x^2)\d t + \sqrt{k_1 x + k_2 x^2}\d W
  \]
\subsection{Модель брюсселятора}
Модель брюсселятора полностью определяется следующей схемой:
  \begin{gather*}
     A \xrightarrow{\;\;\;\;k_1\;\;\;\;\;}X,\\
     2X + Y \xrightarrow{\;\;\;\;k_2\;\;\;\;\;}2X,\\
     B + X \xrightarrow{\;\;\;\;k_3\;\;\;\;\;}Y + D,\\
     X \xrightarrow{\;\;\;\;k_4\;\;\;\;\;} E.
  \end{gather*}
Кроме обычных реагентов $X$ и $Y$ в реакции также участвуют реагенты $A$, $B$, $D$ и $E$, которые являются неисчерпаемыми и поэтому их количество считается неизменяемым со временем. Их можно не учитывать при составлении матриц $M$ и $N$. Однако, даже при их учете алгоритм дает корректные результаты. Продемонстрируем это.
  \[
    \mathbf{N} = 
    \begin{bmatrix}
      0 & 0 & 1 & 0 & 0 & 0\\
      2 & 1 & 0 & 0 & 0 & 0\\
      1 & 0 & 0 & 1 & 0 & 0\\
      1 & 0 & 0 & 0 & 0 & 0
    \end{bmatrix}
    \mathbf{M} = 
    \begin{bmatrix}
    1 & 0 & 0 & 0 & 0 & 0\\
    3 & 0 & 0 & 0 & 0 & 0\\
    0 & 1 & 0 & 0 & 1 & 0\\
    0 & 0 & 0 & 0 & 0 & 1
    \end{bmatrix}
    \mathbf{x} = 
     \begin{bmatrix}x\\y\\a\\b\\d\\e\end{bmatrix}
    \mathbf{k} = 
    \begin{bmatrix}k_{1}\\k_{2}\\k_{3}\\k_{4}\end{bmatrix}
\]
Вектор сноса и матрица диффузии примут следующий вид:
\[
  \begin{pmatrix}
    a k_{1} - b k_{3} x + k_{2} x y \left(x - 1\right) - k_{4} x\\
    b k_{3} x - k_{2} x y \left(x - 1\right)\\
    -a k_{1}\\
    -b k_{3} x\\
    b k_{3} x\\
    k_{4} x
  \end{pmatrix}
\]
\[
  \begin{bmatrix}
    a k_{1} + b k_{3} x + k_{2} x y \left(x - 1\right) + k_{4} x & - b k_{3} x - k_{2} x y \left(x - 1\right) & - a k_{1} & b k_{3} x & - b k_{3} x & - k_{4} x\\
    -b k_{3} x - k_{2} x y \left(x - 1\right) & b k_{3} x + k_{2} x y \left(x - 1\right) & 0 & - b k_{3} x & b k_{3} x & 0\\
    -a k_{1} & 0 & a k_{1} & 0 & 0 & 0\\
    b k_{3} x & - b k_{3} x & 0 & b k_{3} x & - b k_{3} x & 0\\
    -b k_{3} x & b k_{3} x & 0 & - b k_{3} x & b k_{3} x & 0\\
    -k_{4} x & 0 & 0 & 0 & 0 & k_{4} x
  \end{bmatrix}
\]
\[
  \left\{
  \begin{aligned}
    &\dot{x} = a k_{1} - b k_{3} x + k_{2} x y \left(x - 1\right) - k_{4} x,\\
    &\dot{y} = b k_{3} x - k_{2} x y \left(x - 1\right).
  \end{aligned}
  \right.
\]
% To describe models where the intensity of the interaction between components depends on the concentration of a component, one can use the scheme of interaction from chemical kinetics:
% \[
% \sum\limits^{n}_{i=1}N^{s}_{i}X_{i} \xleftrightarrow[\;\;k^{-}_{s}\;\;]{\;\;k^{+}_{s}\;\;} \sum\limits^{n}_{i=1}M^{s}_{i}X_{i},\; s = 1,\ldots,S.
% \]
% Here $X_{i}$ represents the number $i$-th component, matrices $\mathbf{M} = [M^{s}_{i}]$ and $\mathbf{N} = [N^{s}_{i}]$ are called the system state matrices and specify the number of interacting components of the system at each stage of the reaction $k^{+}_{s}$ and $k^{-}_{s}$ --- are coefficients of interaction in direct and reverse reactions.
% One can use such schemes to describe models of chemical reactions, biological and ecological systems. From these diagrams we can derive a system of ordinary differential equations describing the change of variables $X_{i}(t)$ over time.

% Our research group has been generalized the stochastization method, which allows write out stochastic differential equation based on deterministic model . The original data reported in the interaction scheme, it is enough to get stochastic differential equations. For systems of large dimension, the stochastization method requires a large amount of routine work. However, it is possible to automate it. This paper describes this automation, by using Python language and SymPy library for symbolic camputations.

% Created program allows from the array of the coefficients of the interactions to obtain at the output the system of ODE and vector drift and the diffusion matrix for the system of SDEs in human-readable form and in other formats suitable for direct numerical solution.


%Подключаем литературу
\bibliographystyle{ugost2008}% 
\bibliography{lit}

\end{document}